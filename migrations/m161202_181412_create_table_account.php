<?php

use yii\db\Migration;

class m161202_181412_create_table_account extends Migration
{
    public function up()
    {
       $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%account}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'nome' => $this->string(250)->notNull(),
            'cognome' => $this->string(250)->notNull(),
            'indirizzo'=>$this->string(250)->notNull(),
            'cap'=>$this->string(10)->notNull(),
            'citta'=>$this->string(250)->notNull(),
            'provincia'=>$this->string(250)->notNull(),
            'nazione'=>$this->string(250)->notNull(),
            'email'=>$this->string(250)->notNull(),
            'telefono'=>$this->string(50)->notNull(),
            'ragione_sociale' => $this->string(250)->notNull(),
            'partita_iva' => $this->string(20)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%account}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
