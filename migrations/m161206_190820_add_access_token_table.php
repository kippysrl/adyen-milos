<?php

use yii\db\Migration;

class m161206_190820_add_access_token_table extends Migration
{
    public function up()
    {
       $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%token}}', [
            'id' => $this->primaryKey(),
            'token'=>$this->string(250)->notNull(),
            'status'=>  $this->smallInteger(1)->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            
        ], $tableOptions);
        
    }

    public function down()
    {
        $this->dropTable('{{%token}}');
    }

    
}
