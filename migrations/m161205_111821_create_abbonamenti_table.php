<?php

use yii\db\Migration;

/**
 * Handles the creation of table `packs`.
 */
class m161205_111821_create_abbonamenti_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%abbonamenti}}', [
            'id' => $this->primaryKey(),
            'id_account'=> $this->integer()->notNull(),
            'serial_number' => $this->string(50)->notNull(),
            'kippy_imei'=> $this->string(150)->notNull(),
            'id_prodotto'=> $this->integer()->notNull(),
            'durata_mesi'=> $this->integer()->notNull(),
            'scadenza' => $this->integer()->notNull(), 
            'pagamento' => $this->string(50)->notNull(),
            'sigla_valuta' => $this->string(3)->notNull(),
            'importo'=>$this->float(10,2),
            'token' => $this->string(250)->notNull(), 
            'token_scadenza_mese' => $this->string(50)->notNull(), 
            'token_scadenza_anno' => $this->string(50)->notNull(), 
            'status'=> $this->smallInteger(1)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->addForeignKey('fk_abbonamenti_account', 'abbonamenti', 'id_account', 'account', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_abbonamenti_prodotti', 'abbonamenti', 'id_prodotto', 'prodotti', 'id', 'RESTRICT', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
       $this->dropTable('{{%abbonamenti}}');
    }
}
