<?php

use yii\db\Migration;

class m161215_080546_add_column_listino_table extends Migration
{
    public function up()
    {
        $this->addColumn('listino', 'durata_mesi', $this->integer()->notNull()->after('opzione_24'));
        
    }

    public function down()
    {
        $this->dropColumn('listino', 'durata_mesi');
        
    }
}
