<?php

use yii\db\Migration;

class m170207_152113_add_column_account_table extends Migration
{
     public function up()
    {
        $this->addColumn('account', 'access_token', $this->string(255)->notNull()->after('id'));
        
    }

    public function down()
    {
        $this->dropColumn('abbonamenti', 'access_token');
        
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
