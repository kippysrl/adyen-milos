<?php

use yii\db\Migration;

class m170207_140346_add_column_abbonamenti_table extends Migration
{
    public function up()
    {
        $this->addColumn('abbonamenti', 'disdetta', $this->smallInteger(1)->notNull()->after('status'));
        
    }

    public function down()
    {
        $this->dropColumn('abbonamenti', 'disdetta');
        
    }
}
