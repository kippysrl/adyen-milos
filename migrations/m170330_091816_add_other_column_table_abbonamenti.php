<?php

use yii\db\Migration;

class m170330_091816_add_other_column_table_abbonamenti extends Migration
{
    public function up()
    {
        $this->addColumn('abbonamenti', 'sospeso', $this->smallInteger(1)->notNull()->after('disdetta'));
        $this->addColumn('abbonamenti', 'rinnovi_falliti', $this->smallInteger(1)->notNull()->after('sospeso'));
        
    }

    public function down()
    {
        $this->dropColumn('abbonamenti', 'sospeso');
        $this->dropColumn('abbonamenti', 'rinnovi_falliti');
        
    }
}
