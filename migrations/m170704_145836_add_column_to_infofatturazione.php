<?php

use yii\db\Migration;

class m170704_145836_add_column_to_infofatturazione extends Migration
{
    public function up()
    {
        $this->addColumn('infofatturazione', 'note', $this->text()->notNull()->after('end_period'));
        
        
    }

    public function down()
    {
        $this->dropColumn('infofatturazione', 'note');
       
        
    }
}
