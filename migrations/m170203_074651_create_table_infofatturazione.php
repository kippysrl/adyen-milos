<?php

use yii\db\Migration;

class m170203_074651_create_table_infofatturazione extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%infofatturazione}}', [
            'id' => $this->primaryKey(),
            'id_abbonamento' => $this->integer()->notNull(),
            'id_ordine' => $this->integer(),
            'start_period' => $this->integer()->notNull(), 
            'end_period' => $this->integer()->notNull(), 
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->addForeignKey('fk_infofatturazione_abbonamenti', 'infofatturazione', 'id_abbonamento', 'abbonamenti', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_infofatturazione_ordini', 'infofatturazione', 'id_ordine', 'ordini', 'id', 'RESTRICT', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
       $this->dropTable('{{%infofatturazione}}');
    }
}
