<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>


<?php
//echo "<pre>";
//print_r(Yii::$app->user->getIsGuest());
//echo "</pre>";
?>


<section id="elenco-fatture">
<div class="col-md-12 col-lg-12 nopadding webapp">

    <div class="row first-center">
      <div class="wrapper">
    <h3 class="cancel-title" style="font-size: 18px;"><?=Yii::t('app','VUOI VERAMENTE ANNULLARE IL TUO PACCHETTO DI SERVIZI?')?></h3>
    <p style="font-size: 16px;"><?=Yii::t('app','Sappiamo che tieni molto al tuo migliore amico.')?></p>
    <p style="font-size: 16px;"><?=Yii::t('app','Proprio per questo vogliamo che tu stia sempre connesso con il tuo animale, se disdici il tuo servizio mensile non potrai utilizzare il tuo Kippy.')?></p>
    <br>
          <?php $absoluteHomeUrl = Url::home(true); ?>


<!--    chanage before live windows open location-->
    <button type="button" id="big-green-button" onclick="window.open('http://localhost:8080/service-kippy-3/site/device', '_self')"><?=Yii::t('app','HO CAMBIATO IDEA, VOGLIO CONTINUARE A UTILIZZARE KIPPY')?></button>
      </div>

    </div>

    <div class="row second-center">
        <div class="col-md-6 col-lg-6 ">
            <?= Html::img('@web/images/sad-dog-face.png', ['class' => 'img-responsive']) ?>
        </div>

        <div class="col-md-6 col-lg-6" style="padding-top: 30px;">

            <h3 class="cancel-title"><?=Yii::t('app','Se annulli il pacchetto, perderai l’accesso a tutti i servizi:')?></h3>

            <p><b>x</b> <?=Yii::t('app','TRACCIAMENTO A QUALUNQUE DISTANZA IN TEMPO REALE CON I MIGLIORI OPERATORI EUROPEI')?></p>
            <p><b>x</b> <?=Yii::t('app','TRACCIAMENTO ATTIVITÀ MOTORIE E DELLO STATO DI BENESSERE DEL TUO ANIMALE')?></p>
            <p><b>x</b> <?=Yii::t('app','MESSAGGI VITA CON CONTENUTI BASATI SULLE PERFORMANCE DEL TUO ANIMALE')?></p>
            <p><b>x</b> <?=Yii::t('app','STORICO DI TUTTE LE LOCALIZZAZIONI NEGLI ULTIMI DUE MESI')?></p>
            <p><b>x</b> <?=Yii::t('app','TANTE ALTRE NOVITÀ IN COSTANTE AGGIORNAMENTO E SVILUPPO')?></p>

        </div>


    </div>

    <div class="row">

        <div class="col-md-12 col-lg-12">

            <div class="third-center" style="padding-bottom: 80px;">

            <h3 class="cancel-title" style="font-size: 18px;"> <?=Yii::t('app','Prendo atto che:')?></h3>
            <div class="col-md-6 col-md-offset-3" style="background: #71a924; text-align: justify;">
            <p><?=Yii::t('app','Annullando il pacchetto di servizi non potrò utilizzare il mio Kippy in nessun modo.')?></p>
            <p><?=Yii::t('app','Annullando il pacchetto di servizi non avrò la possibilità di accedere ai contenuti sopra elencati.')?></p>

            </div>

            </div>


        </div>


    </div>



    <div class="row">

        <div class="col-md-4 col-lg-4 box-small-4">
            <div class="wrapper">
            <p><strong><?=Yii::t('app','HO UN PROBLEMA')?></strong></p>

                <?php
                //test
                // $_SESSION['lang'] = 'it-it';
                //$_SESSION['lang'] = 'en-en';
                 // $_SESSION['lang'] = 'fr-fr';
                //$_SESSION['lang'] = 'es-es';
              //  $_SESSION['lang'] = 'de-de';
                ?>

                <?php if($_SESSION['lang']=='it-it'): ?>
                <button type="button" id="big-green-button" onClick="window.open('https://www.kippy.eu/ho-un-problema/','kippy.eu')"><?=Yii::t('app','CONTATTA IL CUSTOMER SERVICE')?></button>
                <?php elseif($_SESSION['lang']=='en-en'):?>
                <button type="button" id="big-green-button" onClick="window.open('https://www.kippy.eu/en/i-have-a-problem/','kippy.eu')"><?=Yii::t('app','CONTATTA IL CUSTOMER SERVICE')?></button>
                <?php elseif($_SESSION['lang']=='fr-fr'):?>
                <button type="button" id="big-green-button" onClick="window.open('https://www.kippy.eu/fr/jai-un-probleme/','kippy.eu')"><?=Yii::t('app','CONTATTA IL CUSTOMER SERVICE')?></button>
                <?php elseif($_SESSION['lang']=='es-es'):?>
                <button type="button" id="big-green-button" onClick="window.open('https://www.kippy.eu/es/tengo-un-problema/','kippy.eu')"><?=Yii::t('app','CONTATTA IL CUSTOMER SERVICE')?></button>
                <?php elseif($_SESSION['lang']=='de-de'):?>
                <button type="button" id="big-green-button" onClick="window.open('https://www.kippy.eu/de/ich-habe-ein-problem/','kippy.eu')"><?=Yii::t('app','CONTATTA IL CUSTOMER SERVICE')?></button>
                <?php endif;?>

            </div>

        </div>


        <div class="col-md-4 col-lg-4 box-small-4">
            <div class="wrapper">
            <p><strong><?=Yii::t('app','HO CAMBIATO IDEA')?></strong></p>

                <?php if($_SESSION['lang']=='it-it'): ?>
                    <button type="button" id="big-green-button" onClick="window.open('https://www.kippy.eu/ho-cambiato-idea/','kippy.eu')"><?=Yii::t('app','VOGLIO CONTINUARE AD USARE KIPPY')?></button>
                <?php elseif($_SESSION['lang']=='en-en'):?>
                    <button type="button" id="big-green-button" onClick="window.open('https://www.kippy.eu/en/i-want-to-keep-using-kippy/','kippy.eu')"><?=Yii::t('app','VOGLIO CONTINUARE AD USARE KIPPY')?></button>
                <?php elseif($_SESSION['lang']=='fr-fr'):?>
                    <button type="button" id="big-green-button" onClick="window.open('https://www.kippy.eu/fr/je-veux-continuer-a-utiliser-kippy/','kippy.eu')"><?=Yii::t('app','VOGLIO CONTINUARE AD USARE KIPPY')?></button>
                <?php elseif($_SESSION['lang']=='es-es'):?>
                    <button type="button" id="big-green-button" onClick="window.open('https://www.kippy.eu/es/quiero-seguir-usando-kippy/','kippy.eu')"><?=Yii::t('app','VOGLIO CONTINUARE AD USARE KIPPY')?></button>
                <?php elseif($_SESSION['lang']=='de-de'):?>
                    <button type="button" id="big-green-button" onClick="window.open('https://www.kippy.eu/de/ich-mochte-kippy-weiterhin-benutzen/','kippy.eu')"><?=Yii::t('app','VOGLIO CONTINUARE AD USARE KIPPY')?></button>
                <?php endif;?>

            </div>

        </div>

        <?php

      $id =  Yii::$app->session->get('id_recurring');

        ?>


        <div class="col-md-4 col-lg-4 box-small-4">
            <div class="wrapper">
            <p><strong><?=Yii::t('app','SONO SICURO')?></strong></p>
           <button type="button" id="big-gray-button" onclick="window.open('http://localhost:8080/service-kippy-3/site/disable-recurring?id=<?= $id ?>', '_self')">  <?=Yii::t('app','ANNULLA IL PACCHETTO SERVIZI')?></button>


            </div>


        </div>


    </div>





</div>
</section>