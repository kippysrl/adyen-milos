<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>

<section id="elenco-fatture">
    <div class="col-md-12 col-lg-12 nopadding webapp">
        <div class="wrapper">
            <h1 style="color: #74b027;"><?=Yii::t('app','PERCHÉ VUOI ANNULLARE IL PACCHETTO DI SERVIZI?')?></h1>

        </div>

        <form method="POST" action="<?=Url::to(['site/disable-recurring']);?>" >

        <div class="form-group col-md-6 col-md-offset-3">


            <div class="form-group">
                <select class="form-control" name="reason">


                    <option value="<?=Yii::t('app','Non uso Kippy in modo continuativo')?>"><?=Yii::t('app','Non uso Kippy in modo continuativo')?></option>
                    <option value="<?=Yii::t('app','Non funziona bene')?>"><?=Yii::t('app','Non funziona bene')?></option>
                    <option value="<?=Yii::t('app','Non ho più l’animale')?>"><?=Yii::t('app','Non ho più l’animale')?></option>
                    <option value="<?=Yii::t('app','Il servizio mensile è troppo caro')?>"><?=Yii::t('app','Il servizio mensile è troppo caro')?></option>
                    <option value="<?=Yii::t('app','Non è adatto al mio animale')?>"><?=Yii::t('app','Non è adatto al mio animale')?></option>
                    <option value="<?=Yii::t('app','Altro')?>"><?=Yii::t('app','Altro')?></option>

                </select>
            </div>


        <br>
        <div class="form-group">
            <label for="Textarea" style="font-size: 16px;"><?=Yii::t('app','Vuoi approfondire i motivi per cui hai annullato il pacchetto di servizi?')?></label>
            <textarea class="form-control" id="Textarea" rows="5" name="message"></textarea>
        </div>


        <div class="form-group" style="text-align: center;">
        <input type="submit" class="btn btn-info white-button" value="<?=Yii::t('app','Annulla il pacchetto di servizi')?>" name="cancel-the-service">
        </div>

            <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />

        </form>

        </div>




    </div>
</section>