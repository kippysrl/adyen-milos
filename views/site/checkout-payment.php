<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\bootstrap\Collapse;

$this->title = 'Kippy Vita';
?>
<?php // print_r($_SESSION)?>

<section id="checkout">

        <div class="row">
            <div class="col-sm-4 col-sm-push-8 purchase_summary">
                <p style="text-transform: uppercase"><?=Yii::t('app','ID kippy')?>: <?=$thisKippy['serial']?></p> 
                <h3 class="compli"><?=Yii::t('app','Complimenti per la tua scelta!')?></h3> 
                
               <div id="tagli-pacchetti" style="padding:0;">
                   <div class="taglio <?=  strtolower($cart['items']['pack']['name'])?>">
                       <h2 class=""><?=$cart['items']['pack']['name']?></h2>
                       <p><span class="currency"><?=$cart['items']['pack']['currency']?></span> <span class="pricelist"><?= number_format($cart['items']['pack']['price'],2,".","")?></span></p>
                       <?php if($cart['items']['pack']['code'] == 'BASIC'):?>
                        <p><?=Yii::t('app','Connettività gratuita per')?> <b class="durata">6 <?=Yii::t('app','mesi')?></b></p>
                       
                        <?php elseif ($cart['items']['pack']['code'] == 'PREMIUM'):?>
                        <p><?=Yii::t('app','Connettività gratuita per')?> <b class="durata">12 <?=Yii::t('app','mesi')?></b></p>
                     
                        <?php elseif ($cart['items']['pack']['code'] == 'ULTIMATE'):?>
                        <p><?=Yii::t('app','Connettività gratuita per')?> <b class="durata">24 <?=Yii::t('app','mesi')?></b></p>
                      
                        <?php elseif ($cart['items']['pack']['code'] == 'BASIC-NEW'):?>
                        <p><?=Yii::t('app','Connettività gratuita per')?> <b class="durata">1 <?=Yii::t('app','mese')?></b></p>
                       
                        <?php elseif ($cart['items']['pack']['code'] == 'PREMIUM-NEW'):?>
                        <p><?=Yii::t('app','Connettività gratuita per')?> <b class="durata">12 <?=Yii::t('app','mesi')?></b></p>
                        
                        <?php elseif ($cart['items']['pack']['code'] == 'ULTIMATE-NEW'):?>
                        <p><?=Yii::t('app','Connettività gratuita per')?> <b class="durata">24 <?=Yii::t('app','mesi')?></b></p>
                        
                        <?php endif;?>
                        <br>
                   </div>
               </div>
               
               <div class="riepilogo-dati clearfix">
                   <h2><?=Yii::t('app','Riepilogo Dati')?></h2>
                   
                   <label><?=Yii::t('shop','Nome')?></label><p><?=$model->nome?></p>
                   <label><?=Yii::t('shop','Cognome')?></label><p><?=$model->cognome?></p>
                   <label><?=Yii::t('shop','Indirizzo')?></label><p><?=$model->indirizzo?></p>
                  
                   <label><?=Yii::t('shop','CAP')?></label><p><?=$model->cap?></p>
                   <label><?=Yii::t('shop','Città')?></label><p><?=$model->citta?></p>
                   <label><?=Yii::t('shop','Stato/provincia')?></label><p><?=$model->provincia?></p>
                   <label><?=Yii::t('shop','Paese')?></label><p><?=$model->nazione?></p>
                   <label><?=Yii::t('shop','E-mail')?></label><p><?=$model->email?></p>
                   <label><?=Yii::t('shop','Telefono')?></label><p><?=$model->telefono?></p>
                   <label><?=Yii::t('shop','Ragione sociale')?></label><p><?=($model->ragione_sociale)?$model->ragione_sociale:'-'?></p>
                   <label><?=Yii::t('shop','Partita Iva')?></label><p><?=($model->partita_iva)?$model->partita_iva:'-'?></p>

                   <a id="open-update-dati" class="pull-right" href="<?=Yii::$app->homeUrl?>site/checkout-billing"><?=Yii::t('app','Modifica dati')?></a>
               </div>  

                
            </div>
            <div class="col-sm-8 col-sm-pull-4 paymethod">
                <?php echo Collapse::widget(['items' => []]); //trick per icludere il bootstrap/collapse nella vista ?>
                <h2 class=""><?=Yii::t('app','Metodo di pagamento')?></h2>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div id="creditcard" class="panel panel-default paybox">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                <h3><i class="fa fa-square-o"></i><i class="fa fa-square"></i><?= Yii::t('app','Carta di credito')?> </h3>
                                <div class="loghini">
                                    <?php foreach ($paymentMethods as $paybox):
                                            if(in_array($paybox['brandCode'],$credit_card_local_payment)):
                                    ?>
                                                <img alt="<?=$paybox['name']?>" style="" width="48" height="auto" src="<?=$paybox['logos']['normal']?>">
                                    <?php
                                            endif;
                                          endforeach;
                                    ?> 
                                </div>
                            </a>
                        </div>

                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                <div id="Main" class="padd"><br>
                                <form method="POST" action="<?=Yii::$app->homeUrl?>site/adyen-card" id="adyen-encrypted-form">
                                    <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                                    <div class="form-group">
                                        <label>
                                            <?=Yii::t('shop','Numero Carta')?>
                                        </label>
                                        <input class="form-control validateAdyen" type="text" size="20" data-encrypted-name="number"/>
                                        <div class="help-block"></div>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <?=Yii::t('shop','Nome')?>
                                        </label>
                                       <input class="form-control validateAdyen" type="text" size="20" data-encrypted-name="holderName"/>
                                        <div class="help-block"></div>
                                    </div>


                                    <div class="row">
                                        <div class="col-xs-6" >
                                            <div class="form-group">
                                                <label>
                                                    <?=Yii::t('shop','Mese Scadenza')?> (MM)
                                                </label>
                                                <input class="form-control validateAdyen" type="text" size="2" data-encrypted-name="expiryMonth"/>
                                                <div class="help-block"></div>
                                            </div>
                                        </div>   
                                        <div class="col-xs-6" >
                                            <div class="form-group">
                                                <label>
                                                    <?=Yii::t('shop','Anno Scadenza')?> (YYYY)
                                                </label>
                                                <input class="form-control validateAdyen" type="text" size="4" data-encrypted-name="expiryYear"/>
                                                <div class="help-block"></div>
                                            </div>
                                        </div>
                                    </div>    


                                    <div class="form-group">
                                        <label>
                                            <?=Yii::t('shop','Codice Sicurezza')?> (Cvv2/Cvc3/Cid)
                                        </label>
                                        <div class="row">
                                            <div class="col-xs-6" >
                                                <input class="form-control validateAdyen" type="text" size="4" data-encrypted-name="cvc"/>
                                                <div class="help-block"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <input type="hidden" value="<?= date('c',time())?>" data-encrypted-name="generationtime"/>
                                    <input type="submit" class="btn btn-azzurro" value="<?=Yii::t('shop','Procedi')?>"/>
                                </form><br>
                                </div>
                            </div>
                        </div>
                    </div>
                
                
                <?php 
                        $num_other = 2;
                        foreach ($paymentMethods as $paybox):
                        
                        if(in_array($paybox['brandCode'],$other_local_payment)):
                ?>
                        <div id="<?=$paybox['brandCode']?>" class="panel panel-default paybox">
                            <div class="panel-heading" role="tab" id="heading_<?=$num_other?>">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?=$num_other?>" aria-expanded="false" aria-controls="collapse_<?=$num_other?>">
                                    <h3><i class="fa fa-square-o"></i><i class="fa fa-square"></i><?=$paybox['name']?></h3>
                                    <div class="loghini">
                                        <img alt="<?=$paybox['name']?>" style="" width="48" height="auto" src="<?=$paybox['logos']['normal']?>">
                                    </div>
                                </a>
                            </div>
                            <div id="collapse_<?=$num_other?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?=$num_other?>">
                                <div class="panel-body">
                                    <a class="btn btn-azzurro pay_now_adyen" href="" data-brandCode="<?=$paybox['brandCode']?>" data-issuerId="<?= isset($paybox['issuers'])?$paybox['issuers'][0]['issuerId']:''?>"><?=Yii::t('shop','Procedi')?></a>
                                </div>
                            </div>    
                        </div>      
                <?php   endif;
                        $num_other++;
                        endforeach;
                ?>
                <form method="post" action="<?=Yii::$app->params['Adyen_local_form_action']?>" id="adyenForm" name="adyenForm" target="_parent">
                    <input type="hidden" name="merchantSig" value="" />
                    <?php foreach($params_form_locale as $name => $value):?>
                        <input type="hidden" name="<?=$name?>" value="<?=$value?>" />
                    <?php endforeach;?>
                    
                    <input type="hidden" name="brandCode" value="" />
                    <input type="hidden" name="issuerId" value="" />       
                </form>
            </div>
        </div>
        </div>        
</section>
<?php
$thisKippy = Yii::$app->session->get('thisKippy');
$globalvar = 'var homeUrl="'.Yii::$app->homeUrl.'"; var kippySerial ="'.$thisKippy['serial'].'"; var errorform = "'.Yii::t('shop','Il dato inserito non è valido!').'"; var errorformReq = "'.Yii::t('shop','Il campo è obbligatorio!').'";';
$this->registerJs($globalvar, \yii\web\View::POS_END);
$this->registerJsFile(Yii::$app->params['Adyen_library_url']);
$scriptFooter = <<< JS
//ADYEN Local payment form    
$('body').on('click','.pay_now_adyen',function(e){
    var btn =$(this);
        
    //inserisco brandCode e issuerId nel form in base alla selezione
    var brandCode = btn.attr('data-brandCode');   
    var issuerId = btn.attr('data-issuerId'); 
    
    $('input[name=brandCode]').val(brandCode);
    $('input[name=issuerId]').val(issuerId);
     
    var localForm = $('#adyenForm');
    var params = localForm.serialize();   
    
    //qui chiamata ajax per calcolare merchantSig    
    $.post(homeUrl + 'site/get-merchant-sig',{'params':params},function(response){
        //inserisco merchantSig    
        $('input[name=merchantSig]').val(response);
        localForm.submit();
    });
    
    e.preventDefault();
});
        
        
//ADYEN Credit Card form
        
// The form element to encrypt.
var form = document.getElementById('adyen-encrypted-form');
// See https://github.com/Adyen/CSE-JS/blob/master/Options.md for details on the options to use.
var options = {};
// Bind encryption options to the form.
adyen.createEncryptedForm(form, options);
$('.validateAdyen').on('change, blur',function(){
    var fthis = $(this);
    if(fthis.val()==""){
     fthis.parents('.form-group').addClass('has-error').removeClass('has-success');   
     fthis.siblings('.help-block').html(errorformReq);
     return false;    
    }  
        
    if(fthis.hasClass('invalid')){
            fthis.parents('.form-group').addClass('has-error').removeClass('has-success');
            fthis.siblings('.help-block').html(errorform);             
    }else{
        fthis.parents('.form-group').addClass('has-success').removeClass('has-error');
        fthis.siblings('.help-block').empty(); 
    }
});

        
//menu active su pet
    $('#m-'+kippySerial).addClass('active');
    $('#d-'+kippySerial).addClass('active');
        
//accordion

JS;
$this->registerJs($scriptFooter, \yii\web\View::POS_END);
?>        