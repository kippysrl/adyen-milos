<?php

/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = 'Kippy Vita';
?>
<?php // print_r($_SESSION)?>

<section id="checkout">

        <div class="row">
            <div class="col-sm-4 col-sm-push-8 purchase_summary">
                <p style="text-transform: uppercase"><?=Yii::t('app','ID kippy')?>: <?=$thisKippy['serial']?></p> 
                <h3 class="compli"><?=Yii::t('app','Complimenti per la tua scelta!')?></h3> 
                
               <div id="tagli-pacchetti" style="padding:0;">
                   <div class="taglio <?=  strtolower($cart['items']['pack']['name'])?>">
                       <h2 class=""><?=$cart['items']['pack']['name']?></h2>
                       <p><span class="currency"><?=$cart['items']['pack']['currency']?></span> <span class="pricelist"><?= number_format($cart['items']['pack']['price'],2,".","")?></span></p>
                       <?php if($cart['items']['pack']['code'] == 'BASIC'):?>
                        <p><?=Yii::t('app','Connettività gratuita per')?> <b class="durata">6 <?=Yii::t('app','mesi')?></b></p>
                       
                        <?php elseif ($cart['items']['pack']['code'] == 'PREMIUM'):?>
                        <p><?=Yii::t('app','Connettività gratuita per')?> <b class="durata">12 <?=Yii::t('app','mesi')?></b></p>
                     
                        <?php elseif ($cart['items']['pack']['code'] == 'ULTIMATE'):?>
                        <p><?=Yii::t('app','Connettività gratuita per')?> <b class="durata">24 <?=Yii::t('app','mesi')?></b></p>
                      
                        <?php elseif ($cart['items']['pack']['code'] == 'BASIC-NEW'):?>
                        <p><?=Yii::t('app','Connettività gratuita per')?> <b class="durata">1 <?=Yii::t('app','mese')?></b></p>
                       
                        <?php elseif ($cart['items']['pack']['code'] == 'PREMIUM-NEW'):?>
                        <p><?=Yii::t('app','Connettività gratuita per')?> <b class="durata">12 <?=Yii::t('app','mesi')?></b></p>
                        
                        <?php elseif ($cart['items']['pack']['code'] == 'ULTIMATE-NEW'):?>
                        <p><?=Yii::t('app','Connettività gratuita per')?> <b class="durata">24 <?=Yii::t('app','mesi')?></b></p>
                        
                        <?php endif;?>
                        <br>
                   </div>
               </div>
               
               <div class="riepilogo-dati clearfix">
                   <h2><?=Yii::t('app','Riepilogo Dati')?></h2>
                   
                   <label><?=Yii::t('shop','Nome')?></label><p><?=$model->nome?></p>
                   <label><?=Yii::t('shop','Cognome')?></label><p><?=$model->cognome?></p>
                   <label><?=Yii::t('shop','Indirizzo')?></label><p><?=$model->indirizzo?></p>
                  
                   <label><?=Yii::t('shop','CAP')?></label><p><?=$model->cap?></p>
                   <label><?=Yii::t('shop','Città')?></label><p><?=$model->citta?></p>
                   <label><?=Yii::t('shop','Stato/provincia')?></label><p><?=$model->provincia?></p>
                   <label><?=Yii::t('shop','Paese')?></label><p><?=$model->nazione?></p>
                   <label><?=Yii::t('shop','E-mail')?></label><p><?=$model->email?></p>
                   <label><?=Yii::t('shop','Telefono')?></label><p><?=$model->telefono?></p>
                   <label><?=Yii::t('shop','Ragione sociale')?></label><p><?=($model->ragione_sociale)?$model->ragione_sociale:'-'?></p>
                   <label><?=Yii::t('shop','Partita Iva')?></label><p><?=($model->partita_iva)?$model->partita_iva:'-'?></p>

                   <a id="open-update-dati" class="pull-right" href="<?=Yii::$app->homeUrl?>site/checkout-billing"><?=Yii::t('app','Modifica dati')?></a>
               </div>  

                
            </div>
            <div class="col-sm-8 col-sm-pull-4 paymethod">
                <h2 class=""><?=Yii::t('app','Metodo di pagamento')?></h2>
                <div id="creditcard" class="paybox">
                    <h3><?=Yii::t('app','Carta di credito')?></h3>
                    <div id="Main" class="padd"><br>
                    <form method="POST" action="<?=Yii::$app->homeUrl?>site/adyen-card" id="adyen-encrypted-form">
                        <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                        <div class="form-group">
                            <label>
                                <?=Yii::t('shop','Numero Carta')?>
                            </label>
                            <input class="form-control validateAdyen" type="text" size="20" data-encrypted-name="number"/>
                            <div class="help-block"></div>
                        </div>
                        <div class="form-group">
                            <label>
                                <?=Yii::t('shop','Nome')?>
                            </label>
                           <input class="form-control validateAdyen" type="text" size="20" data-encrypted-name="holderName"/>
                            <div class="help-block"></div>
                        </div>
                        
                            
                        <div class="row">
                            <div class="col-xs-6" >
                                <div class="form-group">
                                    <label>
                                        <?=Yii::t('shop','Mese Scadenza')?> (MM)
                                    </label>
                                    <input class="form-control validateAdyen" type="text" size="2" data-encrypted-name="expiryMonth"/>
                                    <div class="help-block"></div>
                                </div>
                            </div>   
                            <div class="col-xs-6" >
                                <div class="form-group">
                                    <label>
                                        <?=Yii::t('shop','Anno Scadenza')?> (YYYY)
                                    </label>
                                    <input class="form-control validateAdyen" type="text" size="4" data-encrypted-name="expiryYear"/>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>    
                        
                        
                        <div class="form-group">
                            <label>
                                <?=Yii::t('shop','Codice Sicurezza')?> (Cvv2/Cvc3/Cid)
                            </label>
                            <div class="row">
                                <div class="col-xs-6" >
                                    <input class="form-control validateAdyen" type="text" size="4" data-encrypted-name="cvc"/>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>
                        
                        <input type="hidden" value="<?= date('c',time())?>" data-encrypted-name="generationtime"/>
                        <input type="submit" class="btn btn-azzurro" value="<?=Yii::t('shop','Procedi')?>"/>
                    </form><br>
                    </div>
                </div>
                <?php if($cart['items']['pack']['name']!=='BASIC'): //SOLO SE IL PACCHETTO NON E' IL BASIC'?>
                <div id="paypal" class="paybox">
                    <h3><?=Yii::t('app','PayPal')?></h3>
                    <div class="padd paypp">
                    <p><?=Yii::t('shop', 'Cliccando su "check out with paypal" sarai rediretto sul sito di PayPal per completare il pagamento.')?></p>
                        <div class="text-center">

                        <!--PULSANTE PAPAL -->
                        <form action="<?=Yii::$app->homeUrl?>site/paypal" METHOD="POST">
                                <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                                <input style="margin:0 auto; background-color:transparent;padding:0; max-width: 200px;" type='image' name='submit' src='<?=Yii::$app->homeUrl?>images/express-checkout-hero.png' border='0' align='top' alt='Check out with PayPal'/>
                        </form>

                        </div>
                    </div>
                </div>
                <?php endif;?>
            </div>
        </div>

</section>
<?php
$thisKippy = Yii::$app->session->get('thisKippy');
$globalvar = 'var homeUrl="'.Yii::$app->homeUrl.'"; var kippySerial ="'.$thisKippy['serial'].'"; var errorform = "'.Yii::t('shop','Il dato inserito non è valido!').'"; var errorformReq = "'.Yii::t('shop','Il campo è obbligatorio!').'";';
$this->registerJs($globalvar, \yii\web\View::POS_END);
//$this->registerJsFile(Yii::$app->homeUrl.'js/require.js');
$this->registerJsFile(Yii::$app->params['Adyen_library_url']);
$scriptFooter = <<< JS
//ADYEN Credit Card form
        
// The form element to encrypt.
var form = document.getElementById('adyen-encrypted-form');
// See https://github.com/Adyen/CSE-JS/blob/master/Options.md for details on the options to use.
var options = {};
// Bind encryption options to the form.
adyen.createEncryptedForm(form, options);
$('.validateAdyen').on('change, blur',function(){
    var fthis = $(this);
    if(fthis.val()==""){
     fthis.parents('.form-group').addClass('has-error').removeClass('has-success');   
     fthis.siblings('.help-block').html(errorformReq);
     return false;    
    }  
        
    if(fthis.hasClass('invalid')){
            fthis.parents('.form-group').addClass('has-error').removeClass('has-success');
            fthis.siblings('.help-block').html(errorform);             
    }else{
        fthis.parents('.form-group').addClass('has-success').removeClass('has-error');
        fthis.siblings('.help-block').empty(); 
    }
});

        
//menu active su pet
    $('#m-'+kippySerial).addClass('active');
    $('#d-'+kippySerial).addClass('active');
JS;
$this->registerJs($scriptFooter, \yii\web\View::POS_END);
?>        