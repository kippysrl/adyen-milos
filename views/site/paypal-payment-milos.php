<?php

use yii\helpers\Url;
use yii\helpers\Html;

?>


<div class="container">
    <div class="col-md-6">

        <br>

        <form method="POST" action="<?=Url::to(['site/paypal-pay']);?>" id="adyen-encrypted-form">

            <div class="form-group">
                <label>PayPay payment: <?php echo Html::img('@web/images/paypal_ico copia.png') ?></label>
            </div>


            <input type="hidden" value="<?php echo date('Y-m-d').'T'.date('H:i:s', time()).'000Z'; ?>" data-encrypted-name="generationtime"/>

            <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />


            <div style="clear: both;"></div>

            <button type="submit" class="btn btn-info pull-right">Pay Now</button>


        </form>

    </div>
</div>



