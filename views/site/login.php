<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;

$linguabrowser = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        switch ($linguabrowser) {
            case 'it':
                $signup = 'Non sei ancora registrato?';
                $pswRequest = 'Password dimenticata?';
                break;
            case 'en':
                $signup = 'Not signed up yet?';
                $pswRequest = 'Forgot password ?';
                break;
            case 'fr':
               $signup = 'Vous pas encore inscrit?';
                $pswRequest = 'J’ai oublié mon mot de passe';
                break;
            case 'de':
                $signup = 'Noch nicht registriert?';
                $pswRequest = 'Passwort vergessen';
                break;
            case 'es':
                $signup = 'Aún no se ha registrado?';
                $pswRequest = '¿Has olvidado tu contraseña?';
                break;
            default://se non c'è la lingua preferita usa questa
                $signup = 'Not signed up yet?';
                $pswRequest = 'Forgot password ?';
                break;
        }
?>
<div id="site-login">

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-horizontal'],
        
    ]); ?>
       
            <p><a href="http://vita.kippy.eu"><?=$signup?></a></p>
    
        <div class="col-sm-12 col-md-5">
            
            <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'password')->passwordInput() ?>
        </div>    
        <div class="form-group">
            <div class="col-sm-12">
                <?= Html::submitButton('Login', ['class' => 'btn btn-azzurro', 'name' => 'login-button']) ?>
            </div>
        </div>
            <p><a href="<?=Url::to(['/site/password-recovery'])?>"><?=$pswRequest?></a></p>   
    <?php ActiveForm::end(); ?>

    
</div>
