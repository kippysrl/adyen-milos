<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Country;
use app\models\Province;
use yii\web\Session;
/* @var $this yii\web\View */
/* @var $model backend\models\Ordini */
/* @var $form yii\widgets\ActiveForm */
$lang_postfix=substr(Yii::$app->language, 3,2);
$lang_suffix=substr(Yii::$app->language, 0,2);
?>

<div class="ordini-form row">

    <?php $form = ActiveForm::begin();?>
    
    <div class="col-sm-6">
    <?= $form->field($model, 'nome')->textInput(['maxlength' => true])->label(Yii::t('shop', 'Nome').'*') ?>
    </div>    
    <div class="col-sm-6">
    <?= $form->field($model, 'cognome')->textInput(['maxlength' => true])->label(Yii::t('shop', 'Cognome').'*') ?>
    </div>    
    <div class="col-sm-6">
    <?= $form->field($model, 'indirizzo')->textInput(['maxlength' => true])->label(Yii::t('shop', 'Indirizzo').'*') ?>
    </div>       
    <div class="col-sm-6">
    <?= $form->field($model, 'cap')->textInput(['maxlength' => true])->label(Yii::t('shop', 'CAP').'*') ?>
    </div>    
    <div class="col-sm-6">    
    <?= $form->field($model, 'citta')->textInput(['maxlength' => true])->label(Yii::t('shop', 'Città').'*') ?>
    </div>    
    <div class="col-sm-6">
   <?php 
         
         //con ip detect ////
         $paeseDefault = NULL;
         //catturo il paese dalla session
         $paeseInSession = Yii::$app->session->get('paese');
         
         if($paeseInSession):
            //verifico se il paese è abilitato alle consegne 
            if(Country::find()->where(['sigla'=>$paeseInSession , 'status'=>'1'])->exists()):
               //imposto paese come selezionato e sulla voce del form l'attributo DISABLED (vedi campo form)
               
               $selected = $paeseInSession;
            else:
               //restituisco a video il messaggio  di errore
               Yii::$app->session->setFlash('error', 'Sorry, shipment is not available in your country; see terms and conditions or contact us for further details.');
              
                //faccio comunque vedere di defualt 
               $selected = $paeseDefault;
               
            endif;
         else:
             //se è null restituisco un errore
               Yii::$app->session->setFlash('error', 'Now we are not able to geolocate your IP address for a technical failure; please try later'); 
                
             //faccio comunque vedere di defualt 
             $selected = $paeseDefault;
             
         endif;
   ?>
  
    <?= $form->field($model, 'provincia')->dropDownlist(
    		ArrayHelper::map(Province::find()->where(['sigla_country'=>$selected])->orderBy('provincia ASC')->all(),'provincia','provincia'),
                [
    		'prompt'=>'']
	)->label(Yii::t('shop', 'Stato/provincia').'*') ?>
    </div>    
    <div class="col-sm-6">    
    <?= $form->field($model, 'nazione')->dropDownlist(
    		ArrayHelper::map(Country::find()->where(['status'=>'1'])->andWhere(['!=', 'paese','Tutti i Paesi' ])->orderBy('paese ASC')->all(),'sigla','paese'),
                ['options' => [$selected => ['Selected'=>true]],
    		'prompt'=>'',
                //aggiunto con ip detection per inibire la tendina    
                 'disabled'=>true,
                    
                ]
	)->label(Yii::t('shop', 'Paese').'*') ?>
    </div>    
    <div class="col-sm-6">    
    <?= $form->field($model, 'email')->textInput(['maxlength' => true])->label(Yii::t('shop', 'Inserisci la tua e-mail').'*') ?>
     </div>    
    <div class="col-sm-6">   
    <?= $form->field($model, 'telefono')->textInput(['maxlength' => true])->label(Yii::t('shop', 'Numero di telefono').'*') ?>
    </div>    
    <div class="col-sm-6">
    <?= $form->field($model, 'ragione_sociale')->textInput(['maxlength' => true])->label(Yii::t('shop', 'Ragione sociale')) ?>
    </div>    
    <div class="col-sm-6">
    <?= $form->field($model, 'partita_iva')->textInput(['maxlength' => true])->label(Yii::t('shop', 'Partita Iva')) ?>
    </div>    
    <div class="col-sm-12">
    <p><b>* <?=Yii::t('shop', 'Obbligatorio')?></b></p>
    
    
    <?php if($model->isNewRecord):?>

    <div class="form-group" style="font-size: 12px">
       
        <?= $form->field($model, 'privacy')->checkbox(['style'=>'zoom:1.5','label' => Yii::t('app', 'Letta') . ' <a target="_blank" href="https://www.kippy.eu/common/web/pdf/Privacy_e_sicurezza_' . $lang_suffix . '.pdf">' . Yii::t('app', 'l’informativa sulla privacy') . '</a>, ' .Yii::t('app', 'acconsento al trattamento dei miei dati personali al fine dell’invio di comunicazioni commerciali da parte di Kippy S.r.l., incluse, offerte, promozioni, sconti, agevolazioni, informazioni su iniziative speciali, riguardanti prodotti e/o servizi propri o di terzi e per lo svolgimento di ricerche di mercato via e-mail o tramite posta.')]) ?>
        <?= $form->field($model, 'condizioni')->checkbox(['style'=>'zoom:1.5','label' => Yii::t('shop', 'Ho letto ed accetto le clausole').' <a target="_blank" href="https://www.kippy.eu/'.Yii::$app->language.'/'. Yii::t('url', 'termini-e-condizioni').'">'.Yii::t('app', '"Condizioni generali di vendita"').'</a>.']) ?>
        <p><?=Yii::t('app', 'Cliccando su “Acquista ora, richiedo che l’attivazione del pacchetto abbia inizio prima della scadenza del periodo di recesso che potrò esercitare comunque nei termini e alla condizioni descritte')?> <a target="_blank" href="https://www.kippy.eu/<?=Yii::$app->language?>/<?=Yii::t('url', 'resi')?>"><?=Yii::t('app', 'qui')?></a>.</p>
        
    </div>
    <div class="clearfix">
        <center>
    <?= Html::submitButton(Yii::t('shop','Acquista Ora') ,['class' => 'btn btn-verde']) ?>
       </center>
    </div>    
<p></p>
    <?php else:?>
        <center>
    
        <?= Html::submitButton(Yii::t('shop','Aggiorna') ,['class' => 'btn  btn-verde']) ?>
        </center>    
    <?php endif;?>
    <?php ActiveForm::end(); ?>
  
    </div>
</div>
