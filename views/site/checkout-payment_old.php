<?php

/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = 'Kippy Vita';
?>
<?php // print_r($_SESSION)?>

<section id="checkout">

        <div class="row">
            <div class="col-sm-4 col-sm-push-8 purchase_summary">
                <p style="text-transform: uppercase"><?=Yii::t('app','ID kippy')?>: <?=$thisKippy['serial']?></p> 
                <h3 class="compli"><?=Yii::t('app','Complimenti per la tua scelta!')?></h3> 
                
               <div id="tagli-pacchetti" style="padding:0;">
                   <div class="taglio <?=  strtolower($cart['items']['pack']['name'])?>">
                       <h2 class=""><?=$cart['items']['pack']['name']?></h2>
                       <p><span class="currency"><?=$cart['items']['pack']['currency']?></span> <span class="pricelist"><?= number_format($cart['items']['pack']['price'],2,".","")?></span></p>
                       <?php if($cart['items']['pack']['code'] == 'BASIC'):?>
                        <p><?=Yii::t('app','Connettività gratuita per')?> <b class="durata">6 <?=Yii::t('app','mesi')?></b></p>
                       
                        <?php elseif ($cart['items']['pack']['code'] == 'PREMIUM'):?>
                        <p><?=Yii::t('app','Connettività gratuita per')?> <b class="durata">12 <?=Yii::t('app','mesi')?></b></p>
                     
                        <?php elseif ($cart['items']['pack']['code'] == 'ULTIMATE'):?>
                        <p><?=Yii::t('app','Connettività gratuita per')?> <b class="durata">24 <?=Yii::t('app','mesi')?></b></p>
                      
                        <?php elseif ($cart['items']['pack']['code'] == 'BASIC-NEW'):?>
                        <p><?=Yii::t('app','Connettività gratuita per')?> <b class="durata">1 <?=Yii::t('app','mese')?></b></p>
                       
                        <?php elseif ($cart['items']['pack']['code'] == 'PREMIUM-NEW'):?>
                        <p><?=Yii::t('app','Connettività gratuita per')?> <b class="durata">12 <?=Yii::t('app','mesi')?></b></p>
                        
                        <?php elseif ($cart['items']['pack']['code'] == 'ULTIMATE-NEW'):?>
                        <p><?=Yii::t('app','Connettività gratuita per')?> <b class="durata">24 <?=Yii::t('app','mesi')?></b></p>
                        
                        <?php endif;?>
                        <br>
                   </div>
               </div>
               
               <div class="riepilogo-dati clearfix">
                   <h2><?=Yii::t('app','Riepilogo Dati')?></h2>
                   
                   <label><?=Yii::t('shop','Nome')?></label><p><?=$model->nome?></p>
                   <label><?=Yii::t('shop','Cognome')?></label><p><?=$model->cognome?></p>
                   <label><?=Yii::t('shop','Indirizzo')?></label><p><?=$model->indirizzo?></p>
                  
                   <label><?=Yii::t('shop','CAP')?></label><p><?=$model->cap?></p>
                   <label><?=Yii::t('shop','Città')?></label><p><?=$model->citta?></p>
                   <label><?=Yii::t('shop','Stato/provincia')?></label><p><?=$model->provincia?></p>
                   <label><?=Yii::t('shop','Paese')?></label><p><?=$model->nazione?></p>
                   <label><?=Yii::t('shop','E-mail')?></label><p><?=$model->email?></p>
                   <label><?=Yii::t('shop','Telefono')?></label><p><?=$model->telefono?></p>
                   <label><?=Yii::t('shop','Ragione sociale')?></label><p><?=($model->ragione_sociale)?$model->ragione_sociale:'-'?></p>
                   <label><?=Yii::t('shop','Partita Iva')?></label><p><?=($model->partita_iva)?$model->partita_iva:'-'?></p>

                   <a id="open-update-dati" class="pull-right" href="<?=Yii::$app->homeUrl?>site/checkout-billing"><?=Yii::t('app','Modifica dati')?></a>
               </div>  

                
            </div>
            <div class="col-sm-8 col-sm-pull-4 paymethod">
                <h2 class=""><?=Yii::t('app','Metodo di pagamento')?></h2>
                <div id="bancasella" class="paybox">
                    <h3><?=Yii::t('app','Carta di credito')?></h3>
                    <!-- Overlap Layer -->
                    <div id="FreezePane" class="Off"></div>
                    <div id="InnerFreezePane" class="Off">
                            <div id="text"></div>
                    </div>
                    <!-- Overlap Layer END -->
                    <div id="ErrorBox" class="Off">Error</div>
                    <!-- main Box, hidden before the browser check and the iFrame is properly loaded -->
                    <div id="Main" class="padd">
                            <!-- Credit card form -->
                            <form name="CCForm" method="post" id="CCForm" OnSubmit="return CheckCC();" class="Off">
                                    <div id="Fields">
                                    <fieldset id="CCFieldset">
                                    <legend>Credit card data</legend>
                                            <div id="CCcontainer">
                                            <div id="CCField" class="m-100"><label for="CC"><?=Yii::t('shop','Numero Carta')?></label><br class="visible-xxs"><div class="fieldcontainerL"><input type="text" name="CC" id="CC" autocomplete="off" maxlength="19" placeholder="4444444444444444"/></div></div>
                                            <div id="ExpDate"  class="m-100"><label><?=Yii::t('shop','Data Scadenza')?> (MM/YY)</label><br class="visible-xxs"><div class="fieldcontainerS"><input type="text" name="EXPMM" id="EXPMM" autocomplete="off" maxlength="2" placeholder="01" /></div> / <div class="fieldcontainerS"><input type="text" name="EXPYY" id="EXPYY" autocomplete="off" maxlength="2"  placeholder="18"/></div>
                                            </div>
                                            <hr/>
                                            <div id="CCVField" class="m-100"><label><?=Yii::t('shop','Codice Sicurezza')?> (Cvv2/4DBC)</label><br class="visible-xxs"><div class="fieldcontainerS"><input type="password" name="CVV2" id="CVV2" maxlength="4" /></div></div>
                                            <hr />
                                            <div id="NameField"><label for="Name"><?=Yii::t('shop','Nome')?></label><br class="visible-xxs"><div class="fieldcontainerL"><input type="text" name="Name" id="Name" value=""></div></div>
                                            <hr />
                                            <div id="EmailField"><label for="Email"><?=Yii::t('shop','Email')?></label><br class="visible-xxs"><div class="fieldcontainerL"><input type="email" name="Email" id="Email" value=""></div></div>
                                            </div>
                                    </fieldset>
                                    <fieldset id="SubmitFieldset" class="text-center">	
                                            <input type="submit" value="<?=Yii::t('shop','Procedi')?>" id="submit" />
                                    </fieldset>
                                    </div>
                            </form>
                    </div>
                </div>
                <?php if($cart['items']['pack']['name']!=='BASIC'): //SOLO SE IL PACCHETTO NON E' IL BASIC'?>
                <div id="paypal" class="paybox">
                    <h3><?=Yii::t('app','PayPal')?></h3>
                    <div class="padd paypp">
                    <p><?=Yii::t('shop', 'Cliccando su "check out with paypal" sarai rediretto sul sito di PayPal per completare il pagamento.')?></p>
                        <div class="text-center">

                        <!--PULSANTE PAPAL -->
                        <form action="<?=Yii::$app->homeUrl?>site/paypal" METHOD="POST">
                                <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                                <input style="margin:0 auto; background-color:transparent;padding:0; max-width: 200px;" type='image' name='submit' src='<?=Yii::$app->homeUrl?>images/express-checkout-hero.png' border='0' align='top' alt='Check out with PayPal'/>
                        </form>

                        </div>
                    </div>
                </div>
                <?php endif;?>
            </div>
        </div>

</section>
<?php
$thisKippy = Yii::$app->session->get('thisKippy');
$this->registerCssFile(Yii::$app->homeUrl.'css/gestpay.css');
$globalvar = 'var homeUrl="'.Yii::$app->homeUrl.'"; var kippySerial ="'.$thisKippy['serial'].'"; var gestpayUrl ="'.Yii::$app->params['gestpay'].'"; var PARes ="'.$PARes.'"; var TransKey ="'.$TransKey.'"; var shopLogin ="'.$shopLogin.'"; var encString ="'.$encString.'"; ';
$this->registerJs($globalvar, \yii\web\View::POS_END);
$this->registerJsFile(Yii::$app->params['gestpay'] . "Pagam/JavaScript/js_GestPay.js");
$scriptFooter = <<< JS
//bancasella gestpay        
//declaring local object to handle asynchronous responses from the payment page 
var LocalObj = {}
//setting up a function to handle asynchronous security check result after creating the iFrame and loading the payment page
LocalObj.PaymentPageLoad = function(Result){
	//check for errors, if the Result.ErroCode is 10 the iFrame is created correctly and the security check are passed 
	if(Result.ErrorCode == 10 ){
		//iFrame created and and the security check passed 
		//now we can show the form with the credit card fields
		//Handle 3D authentication 2nd call
		
		if (PARes.length > 0){
			//The card holder land for the 2nd call after 3d authentication so we can proceed to process the transaction without showing the form 
			document.getElementById('text').innerHTML = 'Payment in progress...';			
			GestPay.SendPayment({PARes:PARes,TransKey:TransKey},LocalObj.PaymentCallBack);			
		}else{
			document.getElementById('InnerFreezePane').className='Off';
			document.getElementById('FreezePane').className='Off';
			document.getElementById('CCForm').className='On';
		}
	}else{
		//An error has occurred, check the Result.ErrorCode and Result.ErrorDescription 
		//place error handle code HERE
		document.getElementById('ErrorBox').innerHTML='Error:' + Result.ErrorCode + ' ' + Result.ErrorDescription;
		document.getElementById('ErrorBox').className='On'
		document.getElementById('InnerFreezePane').className = 'Off'
		document.getElementById('FreezePane').className = 'Off'
	}							
}
//setting up a function to handle payment result
LocalObj.PaymentCallBack = function (Result){
	if(Result.ErrorCode == 0 ){
		//Transaction correctly processed
		//Decrypt the string to read the Transaction Result
		document.location.replace(homeUrl + 'site/responsella?a=' + shopLogin + '&b='+ Result.EncryptedString);
	}else{
		//An error has occurred
		//check for 3D authentication required
		if(Result.ErrorCode == 8006){
			//The credit card is enrolled we must send the card holder to the authentication page on the issuer website
			//Get the TransKey, IMPORTANT! this value must be stored for further use
			var TransKey = Result.TransKey
			var date = new Date();
			date.setTime(date.getTime()+(1200000));
 			document.cookie = 'TransKey='+TransKey.toString()+'; expires='+ date.toGMTString() +' ; path=/';
			//put the EcryptedString generated by the WSCryptDecrypt webservice in a cookies for later use, the cookies will expire in 20 minutes
			var date = new Date();
			date.setTime(date.getTime()+(1200000));
			document.cookie = 'encString=' + encString +'; expires='+ date.toGMTString() +' ; path=/';		//Get the VBVRisp encrypted string required to access the issuer authentication page
			document.cookie ='shopLogin=' + shopLogin + '; expires='+ date.toGMTString() +' ; path=/'; 	//Get the shopLogin required to access the issuer authentication page
			//Get the VBVRisp encrypted string required to access the issuer authentication page
			var VBVRisp = Result.VBVRisp
			//redirect the user to the issuer authentication page
			var a = shopLogin; 
			var b = VBVRisp;
			var c= document.location.href; //this is the landing page where the user will be redirected after the issuer authentication must be ABSOLUTE
 			
 			var AuthUrl = gestpayUrl + 'pagam/pagam3d.aspx'; 
 			document.location.replace(AuthUrl+'?a='+a+'&b='+b+'&c='+c);
		}else{
			//Hide overlapping layer
			document.getElementById('InnerFreezePane').className='Off';
			document.getElementById('FreezePane').className='Off';	
			document.getElementById('submit').disabled=false;
			//Check the ErrorCode and ErrorDescription
			if(Result.ErrorCode == 1119 || Result.ErrorCode == 1120){
				document.getElementById('ErrorBox').innerHTML='Error:' + Result.ErrorCode +' - ' + Result.ErrorDescription;
				document.getElementById('ErrorBox').className='On'
				//alert(Result.ErrorDescription);
				document.getElementById('CC').focus();
			}
			if(Result.ErrorCode == 1124 || Result.ErrorCode == 1126){
				document.getElementById('ErrorBox').innerHTML='Error:' + Result.ErrorCode +' - ' + Result.ErrorDescription;
				document.getElementById('ErrorBox').className='On'				
				//alert(Result.ErrorDescription);
				document.getElementById('EXPMM').focus();
			}
			if(Result.ErrorCode == 1125){
				document.getElementById('ErrorBox').innerHTML='Error:' + Result.ErrorCode +' - ' + Result.ErrorDescription;
				document.getElementById('ErrorBox').className='On'
				//alert(Result.ErrorDescription);
				document.getElementById('EXPYY').focus();
			}
			if(Result.ErrorCode == 1149){
				document.getElementById('ErrorBox').innerHTML='Error:' + Result.ErrorCode +' - ' + Result.ErrorDescription;
				document.getElementById('ErrorBox').className='On'
				//alert(Result.ErrorDescription);
				document.getElementById('CVV2').focus();
			}
			if(Result.ErrorCode != 1149 || Result.ErrorCode != 1119 || Result.ErrorCode != 1120 || Result.ErrorCode != 1124 || Result.ErrorCode != 1126 || Result.ErrorCode != 1125){
				document.getElementById('ErrorBox').innerHTML='Error:' + Result.ErrorCode +' - ' + Result.ErrorDescription;
				document.getElementById('ErrorBox').className='On'
			
			}		
		}
	}
}
//Send data to GestPay and process transaction
function CheckCC(){
        
	document.getElementById('submit').disabled=true; //disable submit button
	//raise the Overlap layer
	document.getElementById('FreezePane').className='FreezePaneOn';
	//raise the loading message
	document.getElementById('text').innerHTML = 'Payment in progress...';
	document.getElementById('InnerFreezePane').className='On';
	GestPay.SendPayment ({
					 CC : document.getElementById('CC').value,
					 EXPMM : document.getElementById('EXPMM').value,
					 EXPYY : document.getElementById('EXPYY').value,
					 CVV2: document.getElementById('CVV2').value
			},LocalObj.PaymentCallBack);
	return false;
	
}        

//check if the browser support HTML5 postmessage
    if(BrowserEnabled){
            //Browser enabled
            //Creating the iFrame
            GestPay.CreatePaymentPage(shopLogin, encString, LocalObj.PaymentPageLoad);
            //raise the Overlap layer
            document.getElementById('FreezePane').className='FreezePaneOn';
            //raise the loading message
            document.getElementById('text').innerHTML = 'Loading...';
            document.getElementById('InnerFreezePane').className='On';
            //Handle after 3D authentication second call
    }else{
            //Browser not supported
            //Place error handle code here
            document.getElementById('ErrorBox').innerHTML='Error: Browser not supported';
            document.getElementById('ErrorBox').className='On'
    }
        
//menu active su pet
    $('#m-'+kippySerial).addClass('active');
    $('#d-'+kippySerial).addClass('active');
JS;
$this->registerJs($scriptFooter, \yii\web\View::POS_END);
?>        