<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\popover\PopoverX;
$this->title = 'Kippy Vita';
?>
<?php //print_r($_SESSION);
?>

<section id="intro">
    <?php if($status=='ACTIVE'):?>
            <h1 class="text-center"><?=Yii::t('app','Pacchetti servizi Kippy')?><?= ($isVita)?' Vita':' Finder'?></h1>
    <?php else:?>
            <h1 class="text-center"><?=Yii::t('app','Scegli il pacchetto e attiva il tuo Kippy')?><?= ($isVita)?' Vita':' Finder'?></h1>
    <?php endif;?>
            
    <?php if($kippy->listino=='FINDER_OLD'):?>
   
        <p><?=Yii::t('app','Grazie a Kippy potrai accedere ad una serie di servizi e contenuti esclusivi in base al pacchetto scelto. Per garantirti dati sempre aggiornati e senza limiti di distanza, Kippy utilizza le reti telefoniche dei migliori operatori disponibili.')?></p>
    
    <?php else:?>

        <p><?=Yii::t('app','Grazie a Kippy potrai accedere ad una serie di servizi e contenuti esclusivi in base al pacchetto scelto. Per garantirti dati (posizione e attività) sempre aggiornati e senza limiti di distanza, Kippy utilizza le reti telefoniche dei migliori operatori disponibili.')?></p>
        
    <?php endif;?>
</section>    
<?php if($kippy):?>
<section id="elenco-kippy">    
    <?php 
        //ribalto la data
        $data = explode('-', $kippy->subscription_end);
        $scadenza = $data[2] . '/' . $data[1] . '/' . $data [0];
    ?>
    <div class="kippy-abbinato <?=($status=='attivo')?'attivo':''?>">
        <div class="row table">
            <div class="col-md-4 cell">
                <h2 class="animal"><?= $petName?></h2>
                <p><b>ID </b>KIPPY: <span><?= $kippy->serial_number?></span></p>
            </div>
            <div class="col-md-4 cell">
                <?php 
                    if($status=='ACTIVE' || $status=='EXPIRING'){
                        $bollo = 'verde';
                        $stato = Yii::t('app','ATTIVO');
                    }
                    if($status=='SUSPENDEND' || $status=='EXPIRED') {
                        $bollo = 'rosso';
                        $stato = Yii::t('app','SCADUTO');
                    }
                    if($status=='INACTIVE') {
                        $bollo = '';
                        $stato = Yii::t('app','NON ATTIVO');
                    }    
                ?>
                <p><?=Yii::t('app','STATO')?>: 
                    <i class="fa fa-circle <?=$bollo?>"></i> 
                    <b><?=$stato?></b>
                </p>
            </div>
            <div class="col-md-4 cell text-right">
                <?php if($status=='ACTIVE' || $status=='EXPIRING'):?>
                <a class="update-plan" href="#"><?=Yii::t('app','modifica')?></a>
                <p><?=Yii::t('app','Prossimo addebito')?>: <b><?= $scadenza?></b></p>
                <?php endif;?>
                <?php if($status=='INACTIVE'):?>
                <a href="#" class="choise-plan btn btn-azzurro"><?=Yii::t('app','ATTIVA PACCHETTO')?></a>
                <?php endif;?>
                <?php if($status=='SUSPENDEND' || $status=='EXPIRED'):?>
                <a href="#" class="choise-plan btn btn-azzurro"><?=Yii::t('app','RIATTIVA')?></a>
                <p class="rosso"><?=Yii::t('app','Scadenza servizio')?>: <b><?=$scadenza?></b></p>
                <?php endif;?>
            </div>
        </div>
        <div class="collapsible">
            <div class="row table frequenza">

                <div class="col-md-4 cell">
                    <p><b><?=Yii::t('app','Scegli il tuo pacchetto')?></b></p>
                </div>
                <div class="col-md-4 cell">
                    <?php 
                       $items = [];
                       foreach ($packs as $pack):
                           $items[$pack['id']]= $pack['name'] . ' ' . $pack['currency'] . ' '. number_format($pack['price'],2,".","");
                       endforeach;
                       
                    ?>
                    <?php 
                        if($abbonamento):    
                           $options= [
                               $abbonamento->id_prodotto => ['selected' => true]
                            ];
                        else:
                           $options= [
                               17 => ['selected' => true],//default vita 
                               20 => ['selected' => true],//default finder
                            ]; 
                        endif;
                        echo Html::dropDownList('frequenza-fatturazione', null,$items,[
                        'options' => $options,  
                        'class'=>'btn-block',
                        'id'=>'frequenza-fatturazione',
                        'onchange'=>'$(this).parents(".frequenza").find(".change-frequenza").prop("disabled", false)'
                    ])?>


                </div>
                <div id="append-loader" class="col-md-4 cell text-right">
                    <a id="add-to-cart" href="<?=Yii::$app->homeUrl?>site/checkout-billing" class="btn btn-azzurro change-frequenza" ><?=Yii::t('app','conferma scelta')?></a>
                </div>   
            </div>
            
        </div>    
    </div>
    <?php if($status=='ACTIVE' || $status=='EXPIRING'):?>
        
        <p>
            <?=Yii::t('app', 'Il tuo Kippy è attivo con')?>&nbsp;
            <?php if($abbonamento):
                $cod_abb = $abbonamento->idProdotto->codice;
                $nome_abb = explode('-', $cod_abb)[0];
            ?>
             <?=Yii::t('app', 'il pacchetto')?> <?=$nome_abb?> <?=Yii::t('app', 'fino a')?> <?=$scadenza?>.&nbsp;
            <?=Yii::t('app', 'Puoi scegliere un pacchetto diverso cliccando su “modifica” qui sopra.')?>
            <?php else:?>
            <?=Yii::t('app', 'SERVIZIO DI TRACCIAMENTO')?> <?=Yii::t('app', 'fino a')?> <?=$scadenza?>.&nbsp;
            <?=Yii::t('app', 'Potrai acquistare fin da ora uno dei nuovi pacchetti di servizi Kippy semplicemente cliccando sul tasto modifica qui sopra. Ti ricordiamo che al termine del tuo servizio di tracciamento attivo, se non effettuerai un acquisto di un nuovo pacchetto, il tuo Kippy sarà disattivato.')?>
            <?php endif;?>
        </p>
    <?php else:?>
        <p>
            <?php if(!$abbonamento && ($kippy->listino=='FINDER_OLD' || $kippy->listino=='VITA_OLD' || ($kippy->listino=='LICENZA_ZERO' && $status!=='INACTIVE'))):?>
            
              <?=Yii::t('app', 'Il servizio di tracciamento del tuo Kippy è scaduto e il tuo dispositivo è stato disattivato')?>.&nbsp; 
              
            <?php endif;?>
              
              <?=Yii::t('app', 'Scegli il pacchetto di servizi più adatto alle tue esigenze. Non avrai bisogno di sottoscrivere nessun contratto di abbonamento con alcun operatore telefonico.')?>  
        </p>    
    <?php endif;?>

</section>
<?php endif;?>
<section id="tagli-pacchetti">
    <div class="row">
        
        <div class="col-sm-4">
            <div class="taglio premium">
               <h2 class="<?=(isset($nome_abb)&& $nome_abb==$packs[1]['name'])?'active':'';?>"><?=$packs[1]['name']?></h2>
               <p><span class="currency"><?=$packs[1]['currency']?></span> <span class="pricelist"><?=number_format($packs[1]['price'],2,".","")?></span></p>
               <p><?=Yii::t('app','Connettività gratuita per')?> <b class="durata"><?=$packs[1]['durata_mesi'] . ' ' . Yii::t('app','mesi')?></b>
                </p>
                <ul>
                    <li><?=Yii::t('app','Garanzia della migliore connessione GPRS disponibile grazie con i migliori operatori europei')?></li>
                    <li><?=Yii::t('app','Nessun costo aggiuntivo di roaming per Europa, Turchia, India e Sud Africa')?></li>
                    <li><?=Yii::t('app','Localizzazioni incluse')?> <?=Yii::t('app','Illimitate')?>
                        <?= // info
                            PopoverX::widget([
                                'header' => Yii::t('app','INFO'),
                                'type' => PopoverX::TYPE_DEFAULT,
                                'placement' => PopoverX::ALIGN_TOP,
                                'content' => Yii::t('app', 'Tutte le volte che viene aggiornata la posizione del tuo Kippy sull’app, viene conteggiata una localizzazione.'),
                                'toggleButton' => ['tag'=> 'i','label'=>false, 'class'=>'fa fa-info-circle', 'style'=>'padding-left:10px;color:#444;font-size:18px; cursor:pointer'],
                            ]);
                        ?>
                    </li>
                    <li><?=Yii::t('app','Primo servizio di Petsitting con PetMe gratuito e 10% di sconto sui successivi')?>
                        <?= // info
                            PopoverX::widget([
                                'header' => Yii::t('app','INFO'),
                                'type' => PopoverX::TYPE_DEFAULT,
                                'placement' => PopoverX::ALIGN_TOP,
                                'content' => Yii::t('app', 'Offerta soggetta a limitazioni di disponibilità. Contattaci per maggiori informazioni.'),
                                'toggleButton' => ['tag'=> 'i','label'=>false, 'class'=>'fa fa-info-circle', 'style'=>'padding-left:10px;color:#444;font-size:18px; cursor:pointer'],
                            ]);
                        ?>
                    </li>
                    <div class="scopridipiu">
                    <a class="apriscopri text-right"><?=Yii::t('app','Scopri tutti i dettagli')?> <i class="fa fa-chevron-right"></i></a>
                    <div class="collapsed">
                        <li><?=Yii::t('app','Attivazione e aggiornamento all’ultima versione del firmware Kippy©')?></li>
                        <li><?=Yii::t('app','Aggiornamento effemeridi giornaliero')?>
                            <?= // info
                                 PopoverX::widget([
                                     'header' => Yii::t('app','INFO'),
                                     'type' => PopoverX::TYPE_DEFAULT,
                                     'placement' => PopoverX::ALIGN_TOP,
                                     'content' => Yii::t('app', 'Quotidianamente, quando il tuo Kippy è acceso e in un luogo  con connessione GPRS, vengono aggiornate le posizioni dei satelliti per offrire una localizzazione più veloce quando richiesta (Assisted GPS).'),
                                     'toggleButton' => ['tag'=> 'i','label'=>false, 'class'=>'fa fa-info-circle', 'style'=>'padding-left:10px;color:#444;font-size:18px; cursor:pointer'],
                                 ]);
                             ?>                        
                        </li>
                        <?php if($isVita): //se è vita?>
                        <li><?=Yii::t('app','Activity tracking: tracciamento delle attività motorie e del consumo calorico del proprio animale')?></li>
                        <li><?=Yii::t('app','Messaggi Vita: contenuti basati sulle performance del tuo animale')?></li>
                        <?php endif;?>
                        <li><?=Yii::t('app','Customer Care tramite chat, telefono o email in 5 lingue')?>
                            <?= // info
                                PopoverX::widget([
                                    'header' => Yii::t('app','INFO'),
                                    'type' => PopoverX::TYPE_DEFAULT,
                                    'placement' => PopoverX::ALIGN_TOP,
                                    'content' => Yii::t('app', 'Italiano, francese, inglese, tedesco e spagnolo.'),
                                    'toggleButton' => ['tag'=> 'i','label'=>false, 'class'=>'fa fa-info-circle', 'style'=>'padding-left:10px;color:#444;font-size:18px; cursor:pointer'],
                                ]);
                            ?>
                        </li>
                        <li><?=Yii::t('app','History con tutte le posizioni e i percorsi registrati')?>
                            <?= // info
                                PopoverX::widget([
                                    'header' => Yii::t('app','INFO'),
                                    'type' => PopoverX::TYPE_DEFAULT,
                                    'placement' => PopoverX::ALIGN_TOP,
                                    'content' => Yii::t('app', 'I dati "history" sono i dati storici di tutti i posizionamenti GPS memorizzati.'),
                                    'toggleButton' => ['tag'=> 'i','label'=>false, 'class'=>'fa fa-info-circle', 'style'=>'padding-left:10px;color:#444;font-size:18px; cursor:pointer'],
                                ]);
                            ?>
                        </li>
                    </div>
                    </div>
                </ul>
                <a class="btn btn-block"> 
                <?php if($status=='ACTIVE' || $status=='EXPIRING'):?>
                    <?=Yii::t('app','modifica')?> 
                <?php endif;?>
                <?php if($status=='INACTIVE'):?>
                    <?=Yii::t('app','ATTIVA PACCHETTO')?>
                <?php endif;?>
                <?php if($status=='SUSPENDEND' || $status=='EXPIRED'):?>
                    <?=Yii::t('app','RIATTIVA')?>
                <?php endif;?>
                    <?=$packs[1]['name']?>
                </a>
            </div>    
        </div>
        <div class="col-sm-4">
            <div class="taglio ultimate">
                <h2 class="<?=(isset($nome_abb)&& $nome_abb==$packs[2]['name'])?'active':'';?>"><?=$packs[2]['name']?></h2>
                <p><span class="currency"><?=$packs[2]['currency']?></span> <span class="pricelist"><?=number_format($packs[2]['price'],2,".","")?></span></p>
                <p>    <?=Yii::t('app','Connettività gratuita per')?> <b class="durata"><?=$packs[2]['durata_mesi'] . ' ' . Yii::t('app','mesi')?></b>
                </p>
                <ul>
                    <li><?=Yii::t('app','Garanzia della migliore connessione GPRS disponibile grazie con i migliori operatori europei')?></li>
                    <li><?=Yii::t('app','Nessun costo aggiuntivo di roaming per Europa, Turchia, India e Sud Africa')?></li>
                    <li><?=Yii::t('app','Localizzazioni incluse')?> <?=Yii::t('app','Illimitate')?>
                        <?= // info
                            PopoverX::widget([
                                'header' => Yii::t('app','INFO'),
                                'type' => PopoverX::TYPE_DEFAULT,
                                'placement' => PopoverX::ALIGN_TOP,
                                'content' => Yii::t('app', 'Tutte le volte che viene aggiornata la posizione del tuo Kippy sull’app, viene conteggiata una localizzazione.'),
                                'toggleButton' => ['tag'=> 'i','label'=>false, 'class'=>'fa fa-info-circle', 'style'=>'padding-left:10px;color:#444;font-size:18px; cursor:pointer'],
                            ]);
                        ?>
                    </li>
                    <li><?=Yii::t('app','Primo servizio di Petsitting con PetMe gratuito e 10% di sconto sui successivi')?>
                        <?= // info
                            PopoverX::widget([
                                'header' => Yii::t('app','INFO'),
                                'type' => PopoverX::TYPE_DEFAULT,
                                'placement' => PopoverX::ALIGN_TOP,
                                'content' => Yii::t('app', 'Offerta soggetta a limitazioni di disponibilità. Contattaci per maggiori informazioni.'),
                                'toggleButton' => ['tag'=> 'i','label'=>false, 'class'=>'fa fa-info-circle', 'style'=>'padding-left:10px;color:#444;font-size:18px; cursor:pointer'],
                            ]);
                        ?>
                    </li>
                    <li><?=Yii::t('app','Accesso diretto al  customer care  II livello con operatore dedicato')?>
                        <?= // info
                            PopoverX::widget([
                                'header' => Yii::t('app','INFO'),
                                'type' => PopoverX::TYPE_DEFAULT,
                                'placement' => PopoverX::ALIGN_TOP,
                                'content' => Yii::t('app', 'Italiano, francese, inglese, tedesco e spagnolo.'),
                                'toggleButton' => ['tag'=> 'i','label'=>false, 'class'=>'fa fa-info-circle', 'style'=>'padding-left:10px;color:#444;font-size:18px; cursor:pointer'],
                            ]);
                        ?>
                    </li>
                    <div class="scopridipiu">
                    <a class="apriscopri text-right"><?=Yii::t('app','Scopri tutti i dettagli')?> <i class="fa fa-chevron-right"></i></a>
                    <div class="collapsed">
                        <li><?=Yii::t('app','Attivazione e aggiornamento all’ultima versione del firmware Kippy©')?></li>
                        <li><?=Yii::t('app','Aggiornamento effemeridi giornaliero')?>
                            <?= // info
                                 PopoverX::widget([
                                     'header' => Yii::t('app','INFO'),
                                     'type' => PopoverX::TYPE_DEFAULT,
                                     'placement' => PopoverX::ALIGN_TOP,
                                     'content' => Yii::t('app', 'Quotidianamente, quando il tuo Kippy è acceso e in un luogo  con connessione GPRS, vengono aggiornate le posizioni dei satelliti per offrire una localizzazione più veloce quando richiesta (Assisted GPS).'),
                                     'toggleButton' => ['tag'=> 'i','label'=>false, 'class'=>'fa fa-info-circle', 'style'=>'padding-left:10px;color:#444;font-size:18px; cursor:pointer'],
                                 ]);
                             ?>                        
                        </li>
                        <?php if($isVita): //se è vita?>
                        <li><?=Yii::t('app','Activity tracking: tracciamento delle attività motorie e del consumo calorico del proprio animale')?></li>
                        <li><?=Yii::t('app','Messaggi Vita: contenuti basati sulle performance del tuo animale')?></li>
                        <?php endif;?>
                        <li><?=Yii::t('app','Customer Care tramite chat, telefono o email in 5 lingue')?>
                            <?= // info
                                PopoverX::widget([
                                    'header' => Yii::t('app','INFO'),
                                    'type' => PopoverX::TYPE_DEFAULT,
                                    'placement' => PopoverX::ALIGN_TOP,
                                    'content' => Yii::t('app', 'Italiano, francese, inglese, tedesco e spagnolo.'),
                                    'toggleButton' => ['tag'=> 'i','label'=>false, 'class'=>'fa fa-info-circle', 'style'=>'padding-left:10px;color:#444;font-size:18px; cursor:pointer'],
                                ]);
                            ?>
                        </li>
                        <li><?=Yii::t('app','History con tutte le posizioni e i percorsi registrati')?>
                            <?= // info
                                PopoverX::widget([
                                    'header' => Yii::t('app','INFO'),
                                    'type' => PopoverX::TYPE_DEFAULT,
                                    'placement' => PopoverX::ALIGN_TOP,
                                    'content' => Yii::t('app', 'I dati "history" sono i dati storici di tutti i posizionamenti GPS memorizzati.'),
                                    'toggleButton' => ['tag'=> 'i','label'=>false, 'class'=>'fa fa-info-circle', 'style'=>'padding-left:10px;color:#444;font-size:18px; cursor:pointer'],
                                ]);
                            ?>
                        </li>
                    </div>
                    </div>
                </ul>
                <a class="btn btn-block"> 
                <?php if($status=='ACTIVE' || $status=='EXPIRING'):?>
                    <?=Yii::t('app','modifica')?> 
                <?php endif;?>
                <?php if($status=='INACTIVE'):?>
                    <?=Yii::t('app','ATTIVA PACCHETTO')?>
                <?php endif;?>
                <?php if($status=='SUSPENDEND' || $status=='EXPIRED'):?>
                    <?=Yii::t('app','RIATTIVA')?>
                <?php endif;?>
                    <?=$packs[2]['name']?>
                </a>
            </div>    
        </div>
        
        <div class="col-sm-4">
            <div class="taglio basic">
                <h2 class="<?=(isset($nome_abb)&& $nome_abb==$packs[0]['name'])?'active':'';?>"><?=$packs[0]['name']?></h2>
                <p><span class="currency"><?=$packs[0]['currency']?></span> <span class="pricelist"><?=number_format($packs[0]['price'],2,".","")?></span></p>
                <p>    <?=Yii::t('app','Connettività gratuita per')?> <b class="durata">
                    <?php if($packs[0]['durata_mesi']==1):?>
                    <?=Yii::t('app','1 mese')?>
                    <?php else:?>
                        <?=$packs[0]['durata_mesi'] . ' ' . Yii::t('app','mesi')?>
                    <?php endif;?>
                    </b>
                </p>
                <ul>
                    <li><?=Yii::t('app','Garanzia della migliore connessione GPRS disponibile grazie con i migliori operatori europei')?></li>
                    <li><?=Yii::t('app','Nessun costo aggiuntivo di roaming per Europa, Turchia, India e Sud Africa')?></li>
                    <li><?=Yii::t('app','Localizzazioni incluse')?> 10000 
                        <?= // info
                            PopoverX::widget([
                                'header' => Yii::t('app','INFO'),
                                'type' => PopoverX::TYPE_DEFAULT,
                                'placement' => PopoverX::ALIGN_TOP,
                                'content' => Yii::t('app', 'Tutte le volte che viene aggiornata la posizione del tuo Kippy sull’app, viene conteggiata una localizzazione.'),
                                'toggleButton' => ['tag'=> 'i','label'=>false, 'class'=>'fa fa-info-circle', 'style'=>'padding-left:10px;color:#444;font-size:18px; cursor:pointer'],
                            ]);
                        ?>
                    </li>
                    <div class="scopridipiu">
                    <a class="apriscopri text-right"><?=Yii::t('app','Scopri tutti i dettagli')?> <i class="fa fa-chevron-right"></i></a>
                    <div class="collapsed">
                        <li><?=Yii::t('app','Attivazione e aggiornamento all’ultima versione del firmware Kippy©')?></li>
                        <li><?=Yii::t('app','Aggiornamento effemeridi giornaliero')?>
                            <?= // info
                                 PopoverX::widget([
                                     'header' => Yii::t('app','INFO'),
                                     'type' => PopoverX::TYPE_DEFAULT,
                                     'placement' => PopoverX::ALIGN_TOP,
                                     'content' => Yii::t('app', 'Quotidianamente, quando il tuo Kippy è acceso e in un luogo  con connessione GPRS, vengono aggiornate le posizioni dei satelliti per offrire una localizzazione più veloce quando richiesta (Assisted GPS).'),
                                     'toggleButton' => ['tag'=> 'i','label'=>false, 'class'=>'fa fa-info-circle', 'style'=>'padding-left:10px;color:#444;font-size:18px; cursor:pointer'],
                                 ]);
                             ?>                        
                        </li>
                        <?php if($isVita): //se è vita?>
                        <li><?=Yii::t('app','Activity tracking: tracciamento delle attività motorie e del consumo calorico del proprio animale')?></li>
                        <li><?=Yii::t('app','Messaggi Vita: contenuti basati sulle performance del tuo animale')?></li>
                        <?php endif;?>
                        <li><?=Yii::t('app','Customer Care tramite chat, telefono o email in 5 lingue')?>
                            <?= // info
                                PopoverX::widget([
                                    'header' => Yii::t('app','INFO'),
                                    'type' => PopoverX::TYPE_DEFAULT,
                                    'placement' => PopoverX::ALIGN_TOP,
                                    'content' => Yii::t('app', 'Italiano, francese, inglese, tedesco e spagnolo.'),
                                    'toggleButton' => ['tag'=> 'i','label'=>false, 'class'=>'fa fa-info-circle', 'style'=>'padding-left:10px;color:#444;font-size:18px; cursor:pointer'],
                                ]);
                            ?>
                        </li>
                        <li><?=Yii::t('app','History con tutte le posizioni e i percorsi registrati')?>
                            <?= // info
                                PopoverX::widget([
                                    'header' => Yii::t('app','INFO'),
                                    'type' => PopoverX::TYPE_DEFAULT,
                                    'placement' => PopoverX::ALIGN_TOP,
                                    'content' => Yii::t('app', 'I dati "history" sono i dati storici di tutti i posizionamenti GPS memorizzati.'),
                                    'toggleButton' => ['tag'=> 'i','label'=>false, 'class'=>'fa fa-info-circle', 'style'=>'padding-left:10px;color:#444;font-size:18px; cursor:pointer'],
                                ]);
                            ?>
                        </li>
                    </div>
                    </div>
                </ul>
                <a class="btn btn-block"> 
                <?php if($status=='ACTIVE' || $status=='EXPIRING'):?>
                    <?=Yii::t('app','modifica')?> 
                <?php endif;?>
                <?php if($status=='INACTIVE'):?>
                    <?=Yii::t('app','ATTIVA')?>
                <?php endif;?>
                <?php if($status=='SUSPENDEND' || $status=='EXPIRED'):?>
                    <?=Yii::t('app','RIATTIVA')?>
                <?php endif;?>
                    <?=$packs[0]['name']?>
                </a>
            </div>
        </div>
    </div> 
    <br>
    <?php if(!$abbonamento && ($kippy->listino=='FINDER_OLD' || $kippy->listino=='VITA_OLD' || ($kippy->listino=='LICENZA_ZERO' && $status!=='INACTIVE'))):?>
    
            <?php if($status=='ACTIVE' || $status=='EXPIRING'):?> 
                <p><?=Yii::t('app', 'Alla scadenza del servizio di tracciamento il tuo Kippy sarà disattivato. Per continuare ad utilizzare i servizi di Kippy dovrai effettuare l’acquisto del pacchetto più in linea con le tue esigenze semplicemente cliccando sul tasto “Attiva pacchetto” nel box sopra i pacchetti di servizi.')?></p>
            <?php else:?>
                <p><?=Yii::t('app', 'Il servizio di tracciamento del tuo Kippy è scaduto e il tuo dispositivo è stato disattivato. Puoi riattivare il tuo Kippy acquistando uno dei pacchetti di servizi qui sopra.')?></p>
            <?php endif;?>  
                
    <?php endif;?>
    <p><?=Yii::t('app', 'Al termine dei pacchetti di servizi e contenuti Kippy, verrà applicato un upgrade al pacchetto superiore con stesso prezzo del pacchetto precedente. Potrai così avere accesso a più servizi e contenuti ad un prezzo vantaggioso. Il prezzo sarà addebitato sulla carta di credito da te usata per l’acquisto del pacchetto precedente, salvo disdetta ricevuta entro 2 giorni dalla scadenza del pacchetto in corso, effettuata tramite l’area apposita.')?></p>
    <p><?=Yii::t('app', 'N.B: in caso di un pacchetto BASIC verrà effettuato un rinnovo dello stesso pacchetto di servizi allo stesso prezzo.')?></p>

    <?php if(Yii::$app->session->get('LoginByApp')):?>
    <div class="text-center">
        <br><br>
        <a class="btn btn-azzurro" href="<?=Yii::$app->homeUrl?>site/user-account"><?=Yii::t('app','info fatturazione')?></a>
    </div>         
    <?php endif;?>
</section>


<?php
$globalvar = 'var homeUrl="'.Yii::$app->homeUrl.'"; var kippySerial ="'.$kippy->serial_number.'";';
$this->registerJs($globalvar, \yii\web\View::POS_END);
$scriptFooter = <<< JS
    //scopri i dettagli
    $('.apriscopri').click(function(e){
        e.preventDefault();
        $(this).hide().siblings('.collapsed').slideToggle();
    });
    //add to cart
    var loader = '<div id="loader"><div class="sk-fading-circle"><div class="sk-circle1 sk-circle"></div><div class="sk-circle2 sk-circle"></div><div class="sk-circle3 sk-circle"></div><div class="sk-circle4 sk-circle"></div><div class="sk-circle5 sk-circle"></div><div class="sk-circle6 sk-circle"></div><div class="sk-circle7 sk-circle"></div><div class="sk-circle8 sk-circle"></div><div class="sk-circle9 sk-circle"></div><div class="sk-circle10 sk-circle"></div><div class="sk-circle11 sk-circle"></div><div class="sk-circle12 sk-circle"></div></div></div>';
    $('#add-to-cart').click(function(e){
        e.preventDefault();
        $(this).hide();
        $('#append-loader').append(loader);
        //ajax call
        var url = homeUrl + 'site/add-to-cart';
        var idPacchetto = $('#frequenza-fatturazione').val();
        
        $.ajax({
            type: "POST",
            url: url,
            data: {'idPacchetto': idPacchetto},
            success: function(response){
               window.location.href = homeUrl + 'site/checkout-billing'; 
            }
        }); 
    });        
    //menu active su pet
    $('#m-'+kippySerial).addClass('active');
    $('#d-'+kippySerial).addClass('active');
        
    //sistemo altezza della col premium per dare continuità all'ombra
    
    $(window).bind('load resize' , function(){
        if($(window).width() > 767){
            var righe = $('.shadow');
            $.each(righe , function(k,riga){
                var that = $(riga);
                $(that).height($(that).siblings('.text-left').height());
            });
            $('.sfuma').height($('#pacchetti').height()-100);
        }
   });    
        
    $('#elenco-kippy').on('click','.update-plan, .choise-plan',function(e){
        e.preventDefault();
        $(this).parents('.kippy-abbinato').find('.collapsible').slideToggle();
    });
   
JS;
$this->registerJs($scriptFooter, \yii\web\View::POS_END);
?>        