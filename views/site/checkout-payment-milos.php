<?php

use yii\helpers\Url;
use yii\helpers\Html;

use app\models\Abbonamenti;

$this->title = 'Kippy Vita';

?>

<?php
//$id_account = Yii::$app->user->id;
//$thisKippy = Yii::$app->session->get('thisKippy');

//echo "<pre>";
//print_r($_SESSION);
//echo "</pre>";


//echo "<pre>";
//print_r(Abbonamenti::find()->where(['id_account'=>$id_account,'kippy_imei'=>$thisKippy['imei'], 'recurring'=>'1', 'serial_number'=>$thisKippy['serial']])->one());
//echo "</pre>";
?>
<script type="text/javascript" src="https://test.adyen.com/hpp/cse/js/8215033921503625.shtml"></script>

<section id="checkout">

    <div class="row">

        <div class="col-md-4 col-md-offset-4">
            
                <p style="text-transform: uppercase"><?=Yii::t('app','ID kippy')?>: <?=$thisKippy['serial']?></p>

                <div id="tagli-pacchetti" style="padding:0;">
                    <div class="taglio <?=  strtolower($cart['items']['pack']['name'])?>">
                        <h2 class=""><?=$cart['items']['pack']['name']?></h2>
                        <p><span class="currency"><?=$cart['items']['pack']['currency']?></span> <span class="pricelist"><?= number_format($cart['items']['pack']['price'],2,".","")?></span></p>
                        <?php if($cart['items']['pack']['code'] == 'BASIC'):?>
                            <p><b class="durata">6 <?=Yii::t('app','mesi')?></b></p>

                        <?php elseif ($cart['items']['pack']['code'] == 'PREMIUM'):?>
                            <p> <b class="durata">12 <?=Yii::t('app','mesi')?></b></p>

                        <?php elseif ($cart['items']['pack']['code'] == 'ULTIMATE'):?>
                            <p> <b class="durata">24 <?=Yii::t('app','mesi')?></b></p>

                        <?php elseif ($cart['items']['pack']['code'] == 'BASIC-NEW'):?>
                            <p> <b class="durata">1 <?=Yii::t('app','mese')?></b></p>

                        <?php elseif ($cart['items']['pack']['code'] == 'PREMIUM-NEW'):?>
                            <p> <b class="durata">12 <?=Yii::t('app','mesi')?></b></p>

                        <?php elseif ($cart['items']['pack']['code'] == 'ULTIMATE-NEW'):?>
                            <p> <b class="durata">24 <?=Yii::t('app','mesi')?></b></p>

                        <?php endif;?>
                        <br>
                    </div>
                </div>


            <h3><?=Yii::t('app','Scegli metodo di pagamento')?></h3>
            <br>

            <button class="accordion">
                <?=Yii::t('shop','Carta di Credito')?>
                <?php echo Html::img('@web/images/card-button.png'); ?>

            </button>

            <div class="panel">

                <br>

                <?php if (isset(Yii::$app->session['failed'])):?>

                    <div class="alert alert-danger">
                        <strong>Failed! </strong><?php echo Yii::$app->session['failed']; ?>
                    </div>

                <?php endif;?>

                <?php if ((Yii::$app->session['success'])):?>

                    <div class="alert alert-success">
                        <strong>Success! </strong><?php echo Yii::$app->session['success']; ?>
                    </div>

                <?php endif;?>

                <?php unset(Yii::$app->session['failed']); ?>
                <?php unset(Yii::$app->session['success']); ?>


                <form method="POST" action="<?=Url::to(['site/checkout-payment']);?>" id="adyen-encrypted-form">

                    <div class="form-group">
                        <label> <?=Yii::t('shop','Numero Carta')?></label>
                        <input class="form-control" type="text" size="20" data-encrypted-name="number" placeholder="XXXX XXXX XXXX XXXX"/>

                    </div>

                    <div class="form-group">
                        <label><?=Yii::t('shop','Nome')?></label>
                        <input class="form-control" type="text" size="20" data-encrypted-name="holderName" placeholder="Your name"/>
                    </div>

                    <div class="form-group col-md-2" style="padding-left:0px;">
                        <label><?=Yii::t('shop','Mese Scadenza')?></label>
                        <input class="form-control" type="text" size="2" data-encrypted-name="expiryMonth" placeholder="MM"/>
                    </div>



                    <div class="form-group col-md-3">
                        <label><?=Yii::t('shop','Anno Scadenza')?></label>
                        <input class="form-control" type="text" size="4" data-encrypted-name="expiryYear" placeholder="YYYY"/>
                    </div>


                    <div class="form-group col-md-3">
                        <label><?=Yii::t('shop','Codice Sicurezza')?></label>
                        <input class="form-control" type="text" size="4" data-encrypted-name="cvc" placeholder="Cvv2/Cvc3/Cid"/>
                    </div>



                    <?php



                    $id_account = Yii::$app->user->id;
                    $thisKippy = Yii::$app->session->get('thisKippy');

                 //   if(Abbonamenti::find()->where(['id_account'=>$id_account,'kippy_imei'=>$thisKippy['imei'], 'recurring'=>'1'])->one()):
                    if(!Abbonamenti::find()->where(['id_account'=>$id_account,'kippy_imei'=>$thisKippy['imei'],'recurring'=>1, 'serial_number'=>$thisKippy['serial']])->exists()):

                    ?>

                    <div class="form-group col-md-2">
                        <label for="countryCode">Recurring?</label>
                        <select class="form-control" name="recurring">

                            <option value="1">YES</option>
                            <option value="0">NO</option>

                        </select>
                    </div>

                    <?php endif;?>


                    <input type="hidden" value="<?php echo date('Y-m-d').'T'.date('H:i:s', time()).'000Z'; ?>" data-encrypted-name="generationtime"/>

                    <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />

<!--                    <input type="hidden" value="--><?php //echo $_SESSION['thisKippy']['serial']; ?><!--">-->

                    <div style="clear: both;"></div>




                    <button type="submit" class="btn btn-azzurro pull-right"><?=Yii::t('shop','Procedi')?></button>

                </form>


            </div>


            <button class="accordion">
                SEPA DIRECT DEBIT
                <?php echo Html::img('@web/images/sepa_logo.jpg'); ?>
            </button>
            <div class="panel">
                <br>

                <form method="POST" action="<?=Url::to(['site/checkout-payment']);?>" id="adyen-encrypted-form">

<!--                    <div class="form-group">-->
<!--                        <label>--><?php //echo Html::img('@web/images/sepa_logo.jpg') ?><!--</label>-->
<!--                        <p style="background-color: dodgerblue; color: white;">S<span style="color: yellow">€</span>PA Direct Debit</p>-->
<!--                    </div>-->


                    <div class="form-group">
                        <label><?=Yii::t('shop','Nome')?></label>
                        <input class="form-control" type="text" size="20"   name="ownerName" placeholder="<?=Yii::t('shop','Nome')?>" value="<?php  echo    Yii::$app->session->get('ownerName'); ?>"/>


                    </div>

                    <div class="form-group">
                        <label>IBAN</label>
                        <input class="form-control" type="text" size="20" name="iban"  placeholder="Iban" value="<?php  echo    Yii::$app->session->get('iban'); ?>"/>
                    </div>

                    <div class="form-group" style="padding-left: 0px;">
                        <label for="countryCode">Select Country</label>
                        <select class="form-control" name="countryCode">

                            <?php foreach ($sepaCountries as $sepaCountry): ?>

                                <?php $countryCode = isset($_SESSION['countryCode']) ? $_SESSION['countryCode'] : '';  ?>

                                <option <?php if($sepaCountry->iban== $countryCode ) echo 'selected'?> value="<?php echo $sepaCountry->iban;?>"><?php echo $sepaCountry->country_name; ?></option>

                            <?php endforeach; ?>


                        </select>
                    </div>

                    <?php

                    $id_account = Yii::$app->user->id;
                    $thisKippy = Yii::$app->session->get('thisKippy');

                    if(!Abbonamenti::find()->where(['id_account'=>$id_account,'kippy_imei'=>$thisKippy['imei'],'recurring'=>1, 'serial_number'=>$thisKippy['serial']])->exists()):

                    ?>



                    <div class="form-group col-md-6" style="padding-right: 0px;">
                        <label for="countryCode">Recurring?</label>
                        <select class="form-control" name="recurring">

                            <option  value="1">YES</option>
                            <option  value="0">NO</option>

                        </select>
                    </div>

                    <?php endif;?>


                    <input type="hidden" value="<?php echo date('Y-m-d').'T'.date('H:i:s', time()).'000Z'; ?>" data-encrypted-name="generationtime"/>

                    <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />


                    <div style="clear: both;"></div>

                    <button type="submit" class="btn btn-azzurro pull-right" id="sepa"><?=Yii::t('shop','Procedi')?></button>


                </form>

            </div>

            <button class="paypal" onclick="window.open('<?=Url::to(['site/paypal-pay']);?>')">
                                 PAYPAL
                               <?php echo Html::img('@web/images/paypal_logo.png'); ?>
            </button>





            <div class="user-info">
                <h2><?=Yii::t('app','Riepilogo Dati')?></h2>

                <label><?=Yii::t('shop','Nome')?></label><p><?=$model->nome?></p>
                <label><?=Yii::t('shop','Cognome')?></label><p><?=$model->cognome?></p>
                <label><?=Yii::t('shop','Indirizzo')?></label><p><?=$model->indirizzo?></p>

                <label><?=Yii::t('shop','CAP')?></label><p><?=$model->cap?></p>
                <label><?=Yii::t('shop','Città')?></label><p><?=$model->citta?></p>
                <label><?=Yii::t('shop','Stato/provincia')?></label><p><?=$model->provincia?></p>
                <label><?=Yii::t('shop','Paese')?></label><p><?=$model->nazione?></p>
                <label><?=Yii::t('shop','E-mail')?></label><p><?=$model->email?></p>
                <label><?=Yii::t('shop','Telefono')?></label><p><?=$model->telefono?></p>
                <label><?=Yii::t('shop','Ragione sociale')?></label><p><?=($model->ragione_sociale)?$model->ragione_sociale:'-'?></p>
                <label><?=Yii::t('shop','Partita Iva')?></label><p><?=($model->partita_iva)?$model->partita_iva:'-'?></p>

                <a id="open-update-dati" class="pull-right" href="<?=Yii::$app->homeUrl?>site/checkout-billing"><?=Yii::t('app','Modifica dati')?></a>
            </div>





        </div>




    </div>


    <script type="text/javascript">
        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function() {
                /* Toggle between adding and removing the "active" class,
                to highlight the button that controls the panel */
                this.classList.toggle("active");

                /* Toggle between hiding and showing the active panel */
                var panel = this.nextElementSibling;
                if (panel.style.display === "block") {
                    panel.style.display = "none";
                } else {
                    panel.style.display = "block";
                }
            });
        }

    </script>





</section>

<?php
$thisKippy = Yii::$app->session->get('thisKippy');
$globalvar = 'var homeUrl="'.Yii::$app->homeUrl.'"; var kippySerial ="'.$thisKippy['serial'].'";';
$this->registerJs($globalvar, \yii\web\View::POS_END);
$scriptFooter = <<< JS
//scopri i dettagli
    $('.apriscopri').click(function(e){
        e.preventDefault();
        $(this).siblings('.collapsed').slideToggle();
    });        
//menu active su pet
    $('#m-'+kippySerial).addClass('active');
    $('#d-'+kippySerial).addClass('active');
JS;
$this->registerJs($scriptFooter, \yii\web\View::POS_END);
?>

<script>
    // The form element to encrypt.
    var form = document.getElementById('adyen-encrypted-form');
    // See https://github.com/Adyen/CSE-JS/blob/master/Options.md for details on the options to use.
    var options = {};
    // Bind encryption options to the form.
    adyen.createEncryptedForm(form, options);
</script>

