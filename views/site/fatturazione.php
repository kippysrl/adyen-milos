<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;

//$_SESSION['lang'] = 'it-it';
//$_SESSION['lang'] = 'en-en';
//$_SESSION['lang'] = 'fr-fr';
//$_SESSION['lang'] = 'es-es';
//$_SESSION['lang'] = 'de-de';


$this->title = 'Kippy Vita';

?>
<section id='account'>

        <div class="kippy-abbinato">
            <div class="row table">
                <div class="col-md-3 cell">
                    <p><b><?=Yii::$app->user->identity->email?></b></p>
                </div>
                <div class="col-md-3 cell">
                    
                </div>
                <div class="col-md-3 cell">
                  
                </div>
                <div class="col-md-3 cell text-right">
                    <a class="update-email" href="#"><?=Yii::t('app','modifica dati fatturazione')?></a>
                   
                </div>
            </div>
            <div class="collapsible">
                <div class="row table email">
                    
                    <?= $this->render('_form-ordini', [
                        'model' => $model,
                        ]) ?>
                     
                </div>
            </div>    
        </div>

</section>
<section id="elenco-kippy">
        <?php if(!count($pacchetti)):?>
        <p><b><?=Yii::t('app','Non ci sono pacchetti attivi al momento.')?></b></p>
        <br>
        <?php endif;?>
        <?php foreach($pacchetti as $pacchetto):?>
        <div class="kippy-abbinato">
            <div class="row table">
                <div class="col-md-4 cell">
                    <h2 class="animal"><?=$pacchetto['petName']?></h2>
                    <p><b>ID </b>KIPPY: <span><?=$pacchetto['serial']?></span></p>
                </div>
                
                <div class="col-md-4 cell">
                    <p><?=Yii::t('app','STATO')?>: <i class="fa fa-circle verde"></i> <b><?=Yii::t('app','ATTIVO')?></b></p>
                </div>
                <div class="col-md-4 cell text-right">
                    <a class="remove-kippy" href="#" data-serial="<?=$pacchetto['serial']?>"><?=Yii::t('app','disdici il pacchetto')?></a>
                    
                    <p><?=Yii::t('app','TIPO PACCHETTO')?>: <b><?=$pacchetto['name']?></b></p>
                </div>
            </div>
            <div class="row table">
                <div class="col-md-12 cell">
                    <p><?=Yii::t('app','Data della prossima fattura')?>: <b><?=$pacchetto['scadenza']?></b><br>
                    <?=Yii::t('app','Pagamento')?>: <b>
                        <?php if($pacchetto['pagamento']=='bancasella'):
                            $l= strlen($pacchetto['token']);    
                            $card = str_repeat('*',$l-4) . substr($pacchetto['token'], -4);
                            echo Yii::t('app','Carta di Credito '). $card;       
                        else:
                            echo Yii::t('app','PayPal');
                        endif;
                        ?>
                    </b></p>
                </div>
            </div>
        </div>
        <?php endforeach;?>

</section>
<?php if(count($fatture)):?>

<section id="elenco-fatture">

        <div class="row">
            <div class="col-sm-12">
                <p><?=Yii::t('app','Le quote di servizio vengono addebitate all’inizio di ciascun periodo. Potrebbe trascorrere qualche giorno dalla data di fatturazione prima che l’addebito sia visualizzato sul tuo account.')?></p>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead class="">
                  <tr>
                    <td><?=Yii::t('app','Data')?></td>
                    <td><?=Yii::t('app','Kippy ID')?></td>
                    <td><?=Yii::t('app','Descrizione')?></td>
                    <td><?=Yii::t('app','Periodo')?></td>
                    <td class="text-center"><?=Yii::t('app','Metodo di pagamento')?></td>
                    <td class="text-center"><?=Yii::t('app','Totale')?></td>
                  </tr>
                </thead>
                <tbody>
                    <?php  foreach ($fatture as $fattura):?>

                        <?php
//                                    echo '<pre>'.print_r($fattura->idAbbonamento->pagamento, true).'</pre>';
//
//                                     die();

                        ?>

                    <tr>
                        <td class="azzurro"><?=Html::encode(date('d/m/y', $fattura->created_at))?></td>
                        <td><?=Html::encode($fattura->idAbbonamento->serial_number)?></td>
                        <?php if($fattura->id_ordine):?>
                            <td><?=Html::encode((Yii::$app->language=='it-it')?$fattura->idAbbonamento->idProdotto->descrizione_it:$fattura->idAbbonamento->idProdotto->descrizione_en)?></td>
                        <?php else:?>
                            <td><?=Html::encode($fattura->note)?></td>
                        <?php endif;?>
                        
                        <td><?=Html::encode(date('d/m/y', $fattura->start_period))?> - <?=Html::encode(date('d/m/y', $fattura->end_period))?></td>
                        <?php if($fattura->id_ordine):?>

                            <td class="text-center"><?=  strtoupper(Html::encode(($fattura->idAbbonamento->pagamento)));?></td>

                            <td class="text-center"><?=Html::encode($fattura->idAbbonamento->importo)?> <?=Html::encode($fattura->idAbbonamento->sigla_valuta)?></td>
                        <?php else:?>
                            <td class="text-center">-</td>
                            <td class="text-center">-</td>
                        <?php endif;?>
                    </tr>
                    <?php endforeach;?>
                </tbody>
                
            </table>
        </div>
        <div class="row fatture-footer">
            <div class="col-sm-12 clearfix">
               <?=Yii::t('app','N.B. La cronologia delle fatture è limitata a 10')?></p>
            </div>
        </div>


<!--    test-->

    <br>
    <br>

    <?php if(isset($result['details'])) : ?>


    <div class="table-responsive col-md-6">
        <p><b>Recurring</b></p>

        <table class="table table-bordered">
            <thead class="table-responsive">
            <tr>

                <td class="text-center"><b><?=Yii::t('shop','Nome')?></b></td>
                <td class="text-center"><b><?=Yii::t('app','Metodo di pagamento')?></b></td>
                <td class="text-center"><b>Disable</b></td>

            </tr>
            </thead>
            <tbody>


            <?php  foreach ($result['details'] as $one_result):?>

                   <?php

                        $cardHolderName =  isset($one_result['RecurringDetail']['card']['holderName']) ? $one_result['RecurringDetail']['card']['holderName'] : '';
                        $sepaHolderName =  isset($one_result['RecurringDetail']['bank']['ownerName']) ? $one_result['RecurringDetail']['bank']['ownerName'] : '';

                   ?>

             <?php $id = $one_result['RecurringDetail']['recurringDetailReference']; ?>

                <tr>
                    <td style="text-align: center"><?php echo $cardHolderName; echo $sepaHolderName; ?></td>
                    <td style="text-align: center"><?php echo $one_result['RecurringDetail']['variant']; ?></td>
                    <td><a href="<?=Url::to(['cancel-service-package', 'id'=>$id])?>" class="btn btn-danger btn-xs center-block" name="disable">Disable</a></td>
                </tr>
            <?php endforeach;?>

            </tbody>

        </table>
    </div>



    <?php endif;?>



<!--    test-->





        
</section>
<?php endif;?>
<?php
$globalvar = 'var homeUrl="'.Yii::$app->homeUrl.'"; var textAlert="'.Yii::t('app', 'Sei sicuro di voler disdire il tuo pacchetto di servizi?').'";';
$this->registerJs($globalvar, \yii\web\View::POS_END);
$scriptFooter = <<< JS
    $('#account').on('click','.update-email',function(e){
        e.preventDefault();
        $(this).parents('.kippy-abbinato').find('.collapsible').slideToggle();
    });
    $('#elenco-kippy').on('click','.remove-kippy',function(e){
        e.preventDefault();
        var serialNumber = $(this).attr('data-serial');
        swal({  
                title: "",   
                text: textAlert,   
                type: "warning",   
                showCancelButton: true,
                confirmButtonColor: "#15adcc;",
                //confirmButtonText: "GO TO CART!",
                //cancelButtonText: "Si, delete it!",
                closeOnConfirm: false,
                closeOnCancel: true
             },
            function(isConfirm){
              if (isConfirm) 
                window.location.href = homeUrl+ 'site/unsubscribe?idKippy='+ serialNumber;  
            }
        );
   });   
JS;
$this->registerJs($scriptFooter, \yii\web\View::POS_END);
?>        