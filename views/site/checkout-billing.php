<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
$this->title = 'Kippy Vita';
?>
<?php // print_r($_SESSION)?>
<section id="checkout">
        <div class="row">
            
            <div class="col-sm-4 col-sm-push-8 purchase_summary">
                <p style="text-transform: uppercase"><?=Yii::t('app','ID kippy')?>: <?=$thisKippy['serial']?></p> 
                <h3 class="compli"><?=Yii::t('app','Complimenti per la tua scelta!')?></h3> 
                <div id="tagli-pacchetti" style="padding:0;">
                    <div class="taglio <?=  strtolower($cart['items']['pack']['name'])?>">
                        <h2 class=""><?=$cart['items']['pack']['name']?></h2>
                        <p><span class="currency"><?=$cart['items']['pack']['currency']?></span> <span class="pricelist"><?= number_format($cart['items']['pack']['price'],2,".","")?></span></p>
                        <?php if($cart['items']['pack']['code'] == 'BASIC'):?>
                        <p><?=Yii::t('app','Connettività gratuita per')?> <b class="durata">6 <?=Yii::t('app','mesi')?></b></p>
                        <ul>
                            <div class="scopridipiu">
                                <a class="apriscopri text-right"><?=Yii::t('app','Scopri tutti i dettagli')?> <i class="fa fa-chevron-right"></i></a>
                                <div class="collapsed">
                                   <br>
                                    <li><?=Yii::t('app','Attivazione e aggiornamento all’ultima versione del firmware Kippy©')?></li>
                                    <li><?=Yii::t('app','Aggiornamento effemeridi giornaliero')?></li>
                                    <li><?=Yii::t('app','Garanzia della migliore connessione GPRS disponibile grazie con i migliori operatori europei')?></li>
                                    <li><?=Yii::t('app','Nessun costo aggiuntivo di roaming per Europa, Turchia, India e Sud Africa')?></li>
                                    <li><?=Yii::t('app','Customer Care tramite chat, telefono o email in 5 lingue')?></li>
                                    <li><?=Yii::t('app','History con tutte le posizioni e i percorsi registrati')?></li>
                                    <li><?=Yii::t('app','Localizzazioni incluse')?> 10000</li>
                                </div> 
                            </div>
                        </ul>
                        <?php elseif ($cart['items']['pack']['code'] == 'PREMIUM'):?>
                        <p><?=Yii::t('app','Connettività gratuita per')?> <b class="durata">12 <?=Yii::t('app','mesi')?></b></p>
                        <ul>
                            <div class="scopridipiu">
                                <a class="apriscopri text-right"><?=Yii::t('app','Scopri tutti i dettagli')?> <i class="fa fa-chevron-right"></i></a>
                                <div class="collapsed">
                                    <br>
                                    <li><?=Yii::t('app','Attivazione e aggiornamento all’ultima versione del firmware Kippy©')?></li>
                                    <li><?=Yii::t('app','Aggiornamento effemeridi giornaliero')?></li>
                                    <li><?=Yii::t('app','Garanzia della migliore connessione GPRS disponibile grazie con i migliori operatori europei')?></li>
                                    <li><?=Yii::t('app','Nessun costo aggiuntivo di roaming per Europa, Turchia, India e Sud Africa')?></li>
                                    <li><?=Yii::t('app','Customer Care tramite chat, telefono o email in 5 lingue')?></li>
                                    <li><?=Yii::t('app','History con tutte le posizioni e i percorsi registrati')?></li>
                                    <li><?=Yii::t('app','Localizzazioni incluse')?> <?=Yii::t('app','illimitate')?></li>
                                    <li><?=Yii::t('app','Primo servizio di Petsitting con PetMe gratuito')?></li>   
                                </div> 
                            </div>
                        </ul>
                        <?php elseif ($cart['items']['pack']['code'] == 'ULTIMATE'):?>
                        <p><?=Yii::t('app','Connettività gratuita per')?> <b class="durata">24 <?=Yii::t('app','mesi')?></b></p>
                        <ul>
                            <div class="scopridipiu">
                                <a class="apriscopri text-right"><?=Yii::t('app','Scopri tutti i dettagli')?> <i class="fa fa-chevron-right"></i></a>
                                <div class="collapsed">
                                   <br>
                                    <li><?=Yii::t('app','Attivazione e aggiornamento all’ultima versione del firmware Kippy©')?></li>
                                    <li><?=Yii::t('app','Aggiornamento effemeridi giornaliero')?></li>
                                    <li><?=Yii::t('app','Garanzia della migliore connessione GPRS disponibile grazie con i migliori operatori europei')?></li>
                                    <li><?=Yii::t('app','Nessun costo aggiuntivo di roaming per Europa, Turchia, India e Sud Africa')?></li>
                                    <li><?=Yii::t('app','Customer Care tramite chat, telefono o email in 5 lingue')?></li>
                                    <li><?=Yii::t('app','History con tutte le posizioni e i percorsi registrati')?></li>
                                    <li><?=Yii::t('app','Localizzazioni incluse')?> <?=Yii::t('app','illimitate')?></li>
                                    <li><?=Yii::t('app','Primo servizio di Petsitting con PetMe gratuito')?></li>
                                    <li><?=Yii::t('app','Accesso diretto al customer care II livello con operatore dedicato')?></li>
                                </div> 
                            </div>
                        </ul>
                        <?php elseif ($cart['items']['pack']['code'] == 'BASIC-NEW'):?>
                        <p><?=Yii::t('app','Connettività gratuita per')?> <b class="durata">1 <?=Yii::t('app','mese')?></b></p>
                        <ul>
                            <div class="scopridipiu">
                                <a class="apriscopri text-right"><?=Yii::t('app','Scopri tutti i dettagli')?> <i class="fa fa-chevron-right"></i></a>
                                <div class="collapsed">
                                    <br>
                                    <li><?=Yii::t('app','Attivazione e aggiornamento all’ultima versione del firmware Kippy©')?></li>
                                    <li><?=Yii::t('app','Aggiornamento effemeridi giornaliero')?></li>
                                    <li><?=Yii::t('app','Activity tracking: tracciamento delle attività motorie e del consumo calorico del proprio animale')?></li>
                                    <li><?=Yii::t('app','Garanzia della migliore connessione GPRS disponibile grazie con i migliori operatori europei')?></li>
                                    <li><?=Yii::t('app','Nessun costo aggiuntivo di roaming per Europa, Turchia, India e Sud Africa')?></li>
                                    <li><?=Yii::t('app','Messaggi Vita: contenuti basati sulle performance del tuo animale')?></li>
                                    <li><?=Yii::t('app','Customer Care tramite chat, telefono o email in 5 lingue')?></li>
                                    <li><?=Yii::t('app','History con tutte le posizioni e i percorsi registrati')?></li>
                                    <li><?=Yii::t('app','Localizzazioni incluse')?> 10000</li>
                                </div> 
                            </div>
                        </ul>
                        <?php elseif ($cart['items']['pack']['code'] == 'PREMIUM-NEW'):?>
                        <p><?=Yii::t('app','Connettività gratuita per')?> <b class="durata">12 <?=Yii::t('app','mesi')?></b></p>
                        <ul>
                            <div class="scopridipiu">
                                <a class="apriscopri text-right"><?=Yii::t('app','Scopri tutti i dettagli')?> <i class="fa fa-chevron-right"></i></a>
                                <div class="collapsed">
                                    <br>
                                    <li><?=Yii::t('app','Attivazione e aggiornamento all’ultima versione del firmware Kippy©')?></li>
                                    <li><?=Yii::t('app','Aggiornamento effemeridi giornaliero')?></li>
                                    <li><?=Yii::t('app','Activity tracking: tracciamento delle attività motorie e del consumo calorico del proprio animale')?></li>
                                    <li><?=Yii::t('app','Garanzia della migliore connessione GPRS disponibile grazie con i migliori operatori europei')?></li>
                                    <li><?=Yii::t('app','Nessun costo aggiuntivo di roaming per Europa, Turchia, India e Sud Africa')?></li>
                                    <li><?=Yii::t('app','Messaggi Vita: contenuti basati sulle performance del tuo animale')?></li>
                                    <li><?=Yii::t('app','Customer Care tramite chat, telefono o email in 5 lingue')?></li>
                                    <li><?=Yii::t('app','History con tutte le posizioni e i percorsi registrati')?></li>
                                    <li><?=Yii::t('app','Localizzazioni incluse')?> <?=Yii::t('app','illimitate')?></li>
                                    <?php if(Yii::$app->session->get('paese') =='IT'):?>
                                    <li><?=Yii::t('app','Primo servizio di Petsitting con PetMe gratuito')?></li>
                                    <?php endif;?>
                                </div> 
                            </div>
                        </ul>
                        <?php elseif ($cart['items']['pack']['code'] == 'ULTIMATE-NEW'):?>
                        <p><?=Yii::t('app','Connettività gratuita per')?> <b class="durata">24 <?=Yii::t('app','mesi')?></b></p>
                        <ul>
                            <div class="scopridipiu">
                                <a class="apriscopri text-right"><?=Yii::t('app','Scopri tutti i dettagli')?> <i class="fa fa-chevron-right"></i></a>
                                <div class="collapsed">
                                    <br>
                                    <li><?=Yii::t('app','Attivazione e aggiornamento all’ultima versione del firmware Kippy©')?></li>
                                    <li><?=Yii::t('app','Aggiornamento effemeridi giornaliero')?></li>
                                    <li><?=Yii::t('app','Activity tracking: tracciamento delle attività motorie e del consumo calorico del proprio animale')?></li>
                                    <li><?=Yii::t('app','Garanzia della migliore connessione GPRS disponibile grazie con i migliori operatori europei')?></li>
                                    <li><?=Yii::t('app','Nessun costo aggiuntivo di roaming per Europa, Turchia, India e Sud Africa')?></li>
                                    <li><?=Yii::t('app','Messaggi Vita: contenuti basati sulle performance del tuo animale')?></li>
                                    <li><?=Yii::t('app','Customer Care tramite chat, telefono o email in 5 lingue')?></li>
                                    <li><?=Yii::t('app','History con tutte le posizioni e i percorsi registrati')?></li>
                                    <li><?=Yii::t('app','Localizzazioni incluse')?> <?=Yii::t('app','illimitate')?></li>
                                    <?php if(Yii::$app->session->get('paese') =='IT'):?>
                                    <li><?=Yii::t('app','Primo servizio di Petsitting con PetMe gratuito')?></li>
                                    <?php endif;?>
                                    <li><?=Yii::t('app','Accesso diretto al customer care II livello con operatore dedicato')?></li>
                                </div> 
                            </div>
                        </ul>
                        <?php endif;?>
                    </div>
                </div>
                
            </div>
            <div class="col-sm-8 col-sm-pull-4">
                
                <h2 class=""><?=Yii::t('app','Dati fatturazione')?></h2>
                <?= $this->render('_form-ordini', [
                        'model' => $model,
                        ]) ?>
            </div>
        </div>
</section>
<?php
$thisKippy = Yii::$app->session->get('thisKippy');
$globalvar = 'var homeUrl="'.Yii::$app->homeUrl.'"; var kippySerial ="'.$thisKippy['serial'].'";';
$this->registerJs($globalvar, \yii\web\View::POS_END);
$scriptFooter = <<< JS
//scopri i dettagli
    $('.apriscopri').click(function(e){
        e.preventDefault();
        $(this).siblings('.collapsed').slideToggle();
    });        
//menu active su pet
    $('#m-'+kippySerial).addClass('active');
    $('#d-'+kippySerial).addClass('active');
JS;
$this->registerJs($scriptFooter, \yii\web\View::POS_END);
?>        