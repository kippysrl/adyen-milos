<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;

$this->title = '';
$this->params['breadcrumbs'][] = $this->title;

?>
<section id="thanks">
    <div class="container text-center">
	<img class="img-responsive center-block" src="<?=Yii::$app->homeUrl?>images/thankyou.jpg" alt="sistema gps per animale" width="500" height="auto" alt="">		
        <h3><?=Yii::t('shop', 'IL TUO PACCHETTO DI SERVIZI<br>E’ STATO ATTIVATO CORRETTAMENTE')?></h3>
        <p><?=Yii::t('shop', 'Riceverai a breve una mail con il riepilogo del tuo ordine.')?></p>
        <p><?=Yii::t('shop', 'Numero ordine:')?> <b id="orderNumber">
                <?php if  (isset($_GET['id_ordine']))

                { echo $_GET['id_ordine']; }   ?>
            </b></p>
        <p></p>
    </div>
</section>
