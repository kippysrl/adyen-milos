<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Password Recovery';
$this->params['breadcrumbs'][] = $this->title;

$linguabrowser = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        switch ($linguabrowser) {
            case 'it':
                $labelEmail = 'Inserisci qui la tua email, riceverai una email con la tua password';
                $button = 'Recupera password';
                $noAccount = 'Indirizzo email non esistente';
                $successMsg = 'Operazione completata con successo riceverai a breve una email.';
                $errorMsg = 'C’è stato un problema nel processare l’operazione, riprova più tardi';
                break;
            case 'en':
                $labelEmail = 'Enter your email address here, you will receive an email with your password';
                $button = 'Password recovery';
                $noAccount = 'Email address not correct';
                $successMsg = 'Operation completed successfully, you will receive an email shortly.';
                $errorMsg = 'There was a problem in processing the operation, please try again later.';
                break;
            case 'fr':
               $labelEmail = 'Entrez votre adresse email ici, vous recevrez un email avec votre mot de passe';
                $button = 'Récupérer le mot de passe';
                $noAccount = 'Adresse e-mail inexistante';
                $successMsg = 'Opération terminée avec succès, vous recevrez un courriel sous peu.';
                $errorMsg = 'Un problème est survenu lors du traitement de l’opération. Veuillez réessayer plus tard.';
                break;
            case 'de':
                $labelEmail = 'Geben Sie hier Ihre E-Mail-Adresse ein und Sie erhalten eine E-Mail mit Ihrem Passwort';
                $button = 'Passwort wiederherstellen';
                $noAccount = 'Emailadresse ist nicht korrekt';
                $successMsg = 'Vorgang erfolgreich abgeschlossen, erhalten Sie in Kürze eine E-Mail.';
                $errorMsg = 'Bei der Verarbeitung der Operation ist ein Problem aufgetreten. Bitte versuchen Sie es später erneut.';
                break;
            case 'es':
                $labelEmail = 'Ingrese su dirección de correo electrónico aquí, recibirá un correo electrónico con su contraseña';
                $button = 'Recuperar contraseña';
                $noAccount = 'Dirección del e-mail no existe';
                $successMsg = 'La operación se completó con éxito, recibirá un correo electrónico en breve.';
                $errorMsg = 'Hubo un problema al procesar la operación, intente de nuevo más tarde.';
                break;
            default://se non c'è la lingua preferita usa questa
                $labelEmail = 'Enter your email address here, you will receive an email with your password';
                $button = 'Password recovery';
                $noAccount = 'Email address not correct';
                $successMsg = 'Operation completed successfully, you will receive an email shortly.';
                $errorMsg = 'There was a problem in processing the operation, please try again later.';
                break;
        }
?>
<div id="site-login" >

        <?php $form = ActiveForm::begin([
        'id' => 'psw-form',
        'action'=>'site/get-password',
        
    ]); ?>
         
             <div class="col-sm-12">
                 <p><?=$labelEmail?></p>
             </div>
            <div class="col-sm-12 col-md-5">
                 <?= $form->field($model, 'email')->textInput(['autofocus' => true])->label(false) ?>

            </div>    
       
            <div class="col-sm-12">
             
                <?= Html::submitButton($button, ['class' => 'btn btn-azzurro', 'name' => 'psw-button']) ?>
            </div>
        

         <?php ActiveForm::end(); ?>
        <div class="ajax-response col-sm-12"></div>
        
</div>
<?php
$homeUrl =Yii::$app->homeUrl;

$scriptFooter = <<< JS
/* Form psw */
        
$(document).on('beforeSubmit','#psw-form',function(e){
        var form = $(this);  
        var btn = form.find('button');    
        if(form.find('.has-error').length) {
            return false;
        }          
        btn.attr("disabled", true);    
        $.ajax({
            url: "$homeUrl" + form.attr('action'),
            type: 'post',
            data: form.serialize(),
            success: function(res) {                                       
                if(res=="0"){
                  form.trigger("reset");
                  btn.attr("disabled", false);
                  form.siblings('.ajax-response').html('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>$successMsg</div>');
                }else{
                    btn.attr("disabled", false);
                    if(res=="115"){
                        form.siblings('.ajax-response').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>$noAccount</div>');
                    }else{
                        form.siblings('.ajax-response').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>$errorMsg</div>');
                    }
                }
            },
            error  : function () 
            {
                btn.attr("disabled", false);
                
                form.siblings('.ajax-response').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>$errorMsg</div>');
               
            }
       });                              
       return false;
   });
    
   //chiudo l'alert
   $('.ajax-response').on('click','.alert .close', function(){
      $('.ajax-response').empty();
   });
JS;
$this->registerJs($scriptFooter, \yii\web\View::POS_END);
?>         