<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = 'Kippy Vita';
?>
<div class="site-error container text-center">

    <h1><?=Yii::t('app','COLLEGA KIPPY AL TUO ACCOUNT')?></h1>
    <p><?=Yii::t('app','Accedi all’App Kippy Vita, inserisci i dati del tuo animale e l’ID Kippy che trovi sul retro del dispositivo.')?></p>
    <br><br><img class="img-responsive center-block" src="<?=Yii::$app->homeUrl?>images/1-smartphone-account.png" alt="kippy">
    
</div>
