<?php

/* @var $this yii\web\View */



use yii\helpers\Html;
//print_r($_SESSION);
$this->title = 'Kippy Vita';

?>
<div id="device">
    
    <div class="row">
        <?php foreach ($pets as $pet):?>
        <div class="col-xs-6 col-md-4">
            <div class="thumbnail">
                <a href="<?=Yii::$app->homeUrl?>site/index?kippyId=<?= $pet->serial_number?>">
                    <?php if($pet->kippy_image):?>
                        <img src="data:image/jpeg;base64,<?= base64_encode(file_get_contents('https://prod.kippyapi.eu/vita/UDDownloadFile.php?file_id=' . $pet->kippy_image . '&file_format=small&app_code='.Yii::$app->session->get('loggedIn_params')['app_code'].'&app_verification_code='.Yii::$app->session->get('loggedIn_params')['app_verification_code']))?>" alt="">
                    <?php else:?>
                        <img src="<?=Yii::$app->homeUrl?>images/placeHolderPet.png" alt="">
                    <?php endif;?>
                    <div class="caption">
                      <h3><?=$pet->kippy_name?></h3>
                      <p><b>ID </b>KIPPY: <span><?= $pet->serial_number?></span></p>
                      <?php 
                        switch ($pet->subscription_status){
                            case 0:
                                $status = 'ACTIVE';
                                break;
                            case 1: 
                                $status = 'EXPIRING';
                                break;
                            case 2:
                               $status = 'SUSPENDEND';
                                break;
                            case 3: 
                                $now = strtotime(date('Y-m-d'));
                                $end = strtotime($pet->subscription_end);
                                $status = (($now - $end) >= (60*60*24*365*20))? 'INACTIVE' : 'EXPIRED';//AGGIUNTO STATO INATTIVO PER I - 20 ANNI
                                break;

                        }
                        if($status=='ACTIVE' || $status=='EXPIRING'){
                            $bollo = 'verde';
                            $stato = Yii::t('app','ATTIVO');
                        }
                        if($status=='SUSPENDEND' || $status=='EXPIRED') {
                            $bollo = 'rosso';
                            $stato = Yii::t('app','SCADUTO');
                        }
                        if($status=='INACTIVE') {
                            $bollo = '';
                            $stato = Yii::t('app','NON ATTIVO');
                        }    
                    ?>
                    <p><?=Yii::t('app','STATO')?>: 
                        <i class="fa fa-circle <?=$bollo?>"></i> 
                        <b><?=$stato?></b>
<!--                        <i class="fa fa-gear pull-right"></i>-->
                    </p>
                    </div>
                </a>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
    <?php if(Yii::$app->session->get('LoginByApp')):?>
    <div class="text-center">
        <br>
        <a class="btn btn-azzurro" href="<?=Yii::$app->homeUrl?>site/user-account"><?=Yii::t('app','info fatturazione')?></a>
    </div>         
    <?php endif;?>
    
</div>
