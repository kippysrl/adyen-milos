<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\widgets\Alert;

$lang_suffix=substr(Yii::$app->language, 0,2);
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      
<html lang="<?= $lang_suffix ?>" class="no-js lt-ie9 lt-ie8 lt-ie7">
<![endif]-->
<!--[if IE 7]>         
<html lang="<?= $lang_suffix ?>" class="no-js lt-ie9 lt-ie8">
<![endif]-->
<!--[if IE 8]>         
<html lang="<?= $lang_suffix ?>" class="no-js lt-ie9">
<![endif]-->
<!--[if gt IE 8]><!-->
<html lang="<?= $lang_suffix ?>" class="no-js">
<!--<![endif]-->
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-T33QLCQ');</script>
<!-- End Google Tag Manager -->
<!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T33QLCQ"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <meta charset="<?= Yii::$app->charset ?>">
     <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <!-- Pushy CSS -->
    <link rel="stylesheet" href="<?=Yii::$app->homeUrl?>css/pushy.css">
    <!-- Modernizr -->
    <script type="text/javascript" src="<?=Yii::$app->homeUrl?>js/modernizr-2.6.2-respond-1.1.0.min.js" ></script>
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?=Yii::$app->homeUrl?>images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?=Yii::$app->homeUrl?>images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?=Yii::$app->homeUrl?>images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?=Yii::$app->homeUrl?>images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?=Yii::$app->homeUrl?>images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?=Yii::$app->homeUrl?>images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?=Yii::$app->homeUrl?>images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?=Yii::$app->homeUrl?>images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?=Yii::$app->homeUrl?>images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?=Yii::$app->homeUrl?>images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?=Yii::$app->homeUrl?>images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?=Yii::$app->homeUrl?>images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=Yii::$app->homeUrl?>images/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?=Yii::$app->homeUrl?>manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?=Yii::$app->homeUrl?>images/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

</head>
<body>
    
<?php $this->beginBody() ?>

    <header id="topbar" class="push hidden-md hidden-lg" style="<?=(Yii::$app->session->get('LoginByApp'))?'display:none;':''?>">
    <div class="container-fluid clearfix">
        <a href="#" class="menu-btn pull-left"><i class="fa fa-bars"></i></a>
        <?php if(!Yii::$app->user->isGuest && Yii::$app->session->get('backToWebApp')):?>
        <a href="<?=Yii::$app->session->get('backToWebApp')?>" class="pull-right back-to-app"><i class="fa fa-chevron-left"></i> <?=Yii::t('app','torna alla webapp')?></a> 
        <?php endif;?>
    </div>
</header>
<!-- Pushy Menu -->
<nav class="pushy pushy-left">
    <div id="menu_header">
        <a href="<?=Yii::$app->homeUrl?>">
            <img id="logo" class="img-responsive center-block" width="97" height="74" src="<?=Yii::$app->homeUrl?>images/logo_kippy_vita.png">   
        </a>
    </div>
    <?php if(!Yii::$app->user->isGuest):?>
    <ul>
        
        <li class="pushy-submenu">
            <a class="blu" href="#"><?=Yii::t('app','dispositivi associati')?></a>
            <ul>
                <?php if(count($pets = \app\controllers\SiteController::$menuPets)):?>
                    <?php foreach ($pets as $pet):?>
                        <li id="m-<?=$pet->serial_number?>" class="pushy-link"><a href="<?=Yii::$app->homeUrl?>site/index?kippyId=<?=$pet->serial_number?>"><?=$pet->kippy_name?></a></li>
                    <?php endforeach;?>
                <?php endif;?>
              
            </ul>
        </li>
        <li class="pushy-link"><a href="<?=Yii::$app->homeUrl?>site/user-account"><?=Yii::t('app','info fatturazione')?></a></li>
        <li class="pushy-link"><a href="https://www.kippy.eu/<?=Yii::$app->language?>/<?=Yii::t('url','termini-e-condizioni')?>" target="_blank"><?=Yii::t('app','Termini e Condizioni')?></a></li>
        <li class="pushy-link"><a href="https://www.kippy.eu/<?=Yii::$app->language?>/customer-care" target="_blank"><?=Yii::t('app','Customer care')?></a></li>
        <?php if(!Yii::$app->user->isGuest && !Yii::$app->session->get('LoginByApp') ): //&& !Yii::$app->session->get('backToWebApp')?>
        <li class="pushy-link"><a href="<?=Yii::$app->homeUrl?>site/logout"><?=Yii::t('app','Logout')?></a></li>
        <?php endif;?>
    </ul>
    <?php endif;?>
    <p class="footer text-center">&copy; KIPPY Srl<br>P.IVA 08263160965</p>
</nav>    
<!-- Site Overlay -->
<div class="site-overlay"></div>
<div id="container" class="container-fluid" style="<?=(Yii::$app->session->get('LoginByApp'))?'padding-top:0;':''?>">
    <div class="row">
        <div class="hidden-xs hidden-sm col-md-3 col-lg-2 nopadding">
            <nav class="pushy desk">
                <div id="menu_header">
                    <a href="<?=Yii::$app->homeUrl?>">
                    <img id="logo" class="img-responsive center-block" width="97" height="74" src="<?=Yii::$app->homeUrl?>images/logo_kippy_vita.png">
                    </a>
                </div>
                <?php if(!Yii::$app->user->isGuest):?>
                <ul <?=(Yii::$app->language == 'de-de')?'style="font-size:12px;"':''?>>
                    
                    <li class="pushy-submenu">
                        <a class="blu" href="#"><?=Yii::t('app','dispositivi associati')?></a>
                        <ul>
                           <?php if(count($pets = \app\controllers\SiteController::$menuPets)):?>
                                <?php foreach ($pets as $pet):?>
                                    <li id="d-<?=$pet->serial_number?>" class=""><a href="<?=Yii::$app->homeUrl?>site/index?kippyId=<?=$pet->serial_number?>"><?=$pet->kippy_name?></a></li>
                                <?php endforeach;?>
                            <?php endif;?>
                        </ul>
                    </li>
                    <li class=""><a href="<?=Yii::$app->homeUrl?>site/user-account"><?=Yii::t('app','info fatturazione')?></a></li>
                    <li class=""><a href="https://www.kippy.eu/<?=Yii::$app->language?>/<?=Yii::t('url','termini-e-condizioni')?>" target="_blank"><?=Yii::t('app','Termini e Condizioni')?></a></li>
                    <li class=""><a href="https://www.kippy.eu/<?=Yii::$app->language?>/customer-care" target="_blank"><?=Yii::t('app','Customer care')?></a></li>
                     <?php if(!Yii::$app->user->isGuest && !Yii::$app->session->get('LoginByApp') ): //&& !Yii::$app->session->get('backToWebApp')?>
                        <li class="pushy-link"><a href="<?=Yii::$app->homeUrl?>site/logout"><?=Yii::t('app','Logout')?></a></li>
                    <?php endif;?>
                </ul> 
                    <?php if(!Yii::$app->session->get('LoginByApp') && Yii::$app->session->get('backToWebApp')):?>
                    <a  href="<?=Yii::$app->session->get('backToWebApp')?>" class="back-to-app"><i class="fa fa-chevron-left"></i> <?=Yii::t('app','torna alla webapp')?></a> 
                    <?php endif;?>
                <?php endif;?>
                <p class="footer text-center">&copy; KIPPY Srl<br>P.IVA 08263160965</p>
            </nav>    
        </div>
        <div class="col-md-9 col-lg-10 nopadding webapp">
            <section class="banner">
            </section>
            <section class="promo-message">
                
                <?=Alert::widget(); ?>

            </section>

                <?= $content ?>
            
        </div>
    </div>
    
</div>
                 
<?php $this->endBody() ?>
<!-- Pushy JS -->
<script type="text/javascript" src="<?=Yii::$app->homeUrl?>js/pushy.min.js"></script>
<!-- add csrf to all ajax call -->    
<script type="text/javascript">    
    $('.pushy-submenu').removeClass('pushy-submenu-closed').addClass('pushy-submenu-open');
    $.ajaxSetup({
            data: <?= \yii\helpers\Json::encode([
                \yii::$app->request->csrfParam => \yii::$app->request->csrfToken,
            ]) ?>
    }); 
</script> 
<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59984894-3', 'auto');
  ga('set', 'anonymizeIP', true);
  ga('send', 'pageview');

</script>
</body>
</html>
<?php $this->endPage() ?>
