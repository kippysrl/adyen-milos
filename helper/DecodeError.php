<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace app\helper;

use Yii;

class DecodeError {
    
    public static function getMessage($code){
        switch ($code){
            case 0 : 
                return Yii::t('app','OK');
                break;
            case 1 : 
                return Yii::t('app','Codice Errore non in uso');
                break;
            case 2 : 
                return Yii::t('app','Non si connette al database');
                break;
            case 3 : 
                return Yii::t('app','Errore in una query/Server');
                break;
            case 4 : 
                return Yii::t('app','Errore algoritmo App');
                break;
            case 5 : 
                return Yii::t('app','Errore credenziali App');
                break;
            case 100 : 
                return Yii::t('app','Non riesce a ricevere i dati');
                break;
            case 101 : 
                return Yii::t('app','Non riesce a ricevere i dati');
                break;
            case 102 : 
                return Yii::t('app','Non riesce a ricevere i dati');
                break;
            case 103 : 
                return Yii::t('app','ID non esistente');
                break;
            case 104 : 
                return Yii::t('app','ID già registrato');
                break;
            case 105 : 
                return Yii::t('app','Manca l’area Geofence');
                break;
            case 106 : 
                return Yii::t('app','Non ci sono notifiche da visualizzare');
                break;
            case 107 : 
                return Yii::t('app','Non ci sono notifiche da visualizzare');
                break;
            case 108 : 
                return Yii::t('app','Login errato');
                break;
            case 109 : 
                return Yii::t('app','Account non confermato');
                break;
            case 110 : 
                return Yii::t('app','Login facebook offline');
                break;
            case 111 : 
                return Yii::t('app','Errore nella registrazione');
                break;
            case 112 : 
                return Yii::t('app','Mancato caricamento dati lingua');
                break;
            case 113 : 
                return Yii::t('app','Mancano dati da compilare');
                break;
            case 114 : 
                return Yii::t('app','Già registrato');
                break;
            case 115 : 
                return Yii::t('app','Email non registrata');
                break;
            case 116 : 
                return Yii::t('app','Login facebook offline');
                break;
            case 117 : 
                return Yii::t('app','Errore caricamento immagine/dati');
                break;
            case 118 : 
                return Yii::t('app','Errore associazione ID/utente');
                break;
            case 119 : 
                return Yii::t('app','Codice rinnovo già usato');
                break;
            case 120 : 
                return Yii::t('app','Codice rinnovo non esiste');
                break;
            
        }
    }
    
}