<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace app\helper;

use Yii;

class AdyenFunction {
    
    public static function calculateSignature($HMAC_KEY,$pairs){
        //1. Sort the key-value pairs by key.
        ksort($pairs, SORT_STRING);
        //2. Replace null values with an empty string ("") and escape the following characters (\,:) in the value of each pair:
        foreach ($pairs as $key => $value) {
            $escapedPairs[$key] = str_replace(':','\\:', str_replace('\\', '\\\\', $value));
        }
        //3. Concatenate the key names, first, followed by the values. Use a colon (“:”) to delimit the key names and values to obtain the signing string.
        $signingString = implode(":", array_merge(array_keys($escapedPairs), array_values($escapedPairs)));
        //4. Convert the HMAC key to the binary representation. Note that the HMAC key is considered as hexadecimal value.
        $binaryHmacKey = pack("H*" , $HMAC_KEY); 
        //5. Calculate the HMAC with the signing string, in binary representation given the UTF-8 charset, using the cryptographic hash function SHA-256.
        $binaryHmac = hash_hmac('sha256', $signingString, $binaryHmacKey, true);
        //6. Encode the result using the Base64 encoding scheme to obtain the signature.
        $signature = base64_encode($binaryHmac);
        
        return $signature;
    }   
    public static function getDecimalPoints($currency){

        switch ($currency) {
                case 'BHD':
                        $decimalPoints='3';
                        break;
                case 'CVE':
                        $decimalPoints='0';
                        break;
                case 'DJF':
                        $decimalPoints='0';
                        break;
                case 'GNF':
                        $decimalPoints='0';
                        break;
                case 'IDR':
                        $decimalPoints='0';
                        break;
                case 'JOD':
                        $decimalPoints='3';
                        break;							
                case 'JPY':
                        $decimalPoints='0';
                        break;							
                case 'KMF':
                        $decimalPoints='0';
                        break;							
                case 'KRW':
                        $decimalPoints='0';
                        break;							
                case 'KWD':
                        $decimalPoints='3';
                        break;							
                case 'LYD':
                        $decimalPoints='3';
                        break;							
                case 'MRO':
                        $decimalPoints='1';
                        break;							
                case 'OMR':
                        $decimalPoints='3';
                        break;							
                case 'PYG':
                        $decimalPoints='0';
                        break;							
                case 'RWF':
                        $decimalPoints='0';
                        break;							
                case 'TND':
                        $decimalPoints='3';
                        break;							
                case 'UGX':
                        $decimalPoints='0';
                        break;							
                case 'VND':
                        $decimalPoints='0';
                        break;							
                case 'VUV':
                        $decimalPoints='0';
                        break;							
                case 'XAF':
                        $decimalPoints='0';
                        break;							
                case 'XOF':
                        $decimalPoints='0';
                        break;							
                case 'XPF':
                        $decimalPoints='0';
                        break;							
                default:
                        $decimalPoints='2';
                        break;
        }
        return $decimalPoints;
    }
    public static function getLocale($language){

        switch ($language) {
                case 'it-it':
                        $locale='it';
                        break;
                						
                case 'es-es':
                        $locale='es';
                        break;							
                case 'de-de':
                        $locale='de';
                        break;							
               							
                case 'fr-fr':
                        $locale='fr';
                        break;							
                default:
                        $locale='en_GB';
                        break;
        }
        return $locale;
    }
}