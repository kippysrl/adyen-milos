<?php

/* 
 * Classe per riordinare array multidimensionali passando chiave e tipo ordinamento (asc - desc )
 * 
 */
namespace app\helper;

class SortMultiArray {
    public $sort_order = 'asc'; // default
    public $sort_key = 'position'; // default
    
    public function sortalo($array)
    {
        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $this->sort_key) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }

            switch ($this->sort_order) {
                case 'asc':
                    asort($sortable_array);
                break;
                case 'desc':
                    arsort($sortable_array);
                break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        }

        return $new_array;

    }
}