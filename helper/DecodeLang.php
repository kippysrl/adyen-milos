<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace app\helper;

use Yii;

class DecodeLang {
    protected static $languages=[
        1=>[
            'name'=>'English',
            'code'=>'en-gb',
        ],
        2=>[
            'name'=>'Italiano',
            'code'=>'it-it',
        ],
        3=>[
            'name'=>'Espanol',
            'code'=>'es-es',
        ],
        4=>[
            'name'=>'Deutsch',
            'code'=>'de-de',
        ],
        5=>[
            'name'=>'Francais',
            'code'=>'fr-fr',
        ],
        6=>[
            'name'=>'English',
            'code'=>'en-gb',
            //'name'=>'Slovencina',
            //'code'=>'SK',
        ],
        7=>[
            'name'=>'English',
            'code'=>'en-gb',
            //'name'=>'Hrvatski',
            //'code'=>'HR',
        ],
    ];
    public static function getName($id){
        return self::$languages[$id]['name'];
    }
    public static function getCode($id){
        return strtolower(self::$languages[$id]['code']);
    }
    
}