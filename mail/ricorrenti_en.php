<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$this->title = htmlentities('SERVICE PACKAGE');
?>
<style type="text/css" id="media-query">
      body {
  margin: 0;
  padding: 0; }

table {
  border-collapse: collapse;
  table-layout: fixed; }

* {
  line-height: inherit; }

a[x-apple-data-detectors=true] {
  color: inherit !important;
  text-decoration: none !important; }

.ie-browser .col, [owa] .block-grid .col {
  display: table-cell;
  float: none !important;
  vertical-align: top; }

.ie-browser .corner__x {
  display: none; }

.mso-container .corner__x {
  font-size: 0; }

.ie-browser .col.num12, .ie-browser .block-grid, [owa] .col.num12, [owa] .block-grid {
  width: 505px !important; }

.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
  line-height: 100%; }

.ie-browser .block-grid.two-up .col, [owa] .block-grid.two-up .col {
  width: 252px !important; }

.ie-browser .block-grid.three-up .col, [owa] .block-grid.three-up .col {
  width: 168px !important; }

.ie-browser .block-grid.four-up .col, [owa] .block-grid.four-up .col {
  width: 126px !important; }

.ie-browser .block-grid.five-up .col, [owa] .block-grid.five-up .col {
  width: 101px !important; }

.ie-browser .block-grid.six-up .col, [owa] .block-grid.six-up .col {
  width: 84px !important; }

.ie-browser .block-grid.seven-up .col, [owa] .block-grid.seven-up .col {
  width: 72px !important; }

.ie-browser .block-grid.eight-up .col, [owa] .block-grid.eight-up .col {
  width: 63px !important; }

.ie-browser .block-grid.nine-up .col, [owa] .block-grid.nine-up .col {
  width: 56px !important; }

.ie-browser .block-grid.ten-up .col, [owa] .block-grid.ten-up .col {
  width: 50px !important; }

.ie-browser .block-grid.eleven-up .col, [owa] .block-grid.eleven-up .col {
  width: 45px !important; }

.ie-browser .block-grid.twelve-up .col, [owa] .block-grid.twelve-up .col {
  width: 42px !important; }

@media only screen and (min-width: 525px) {
  .block-grid {
    width: 505px !important; }
  .block-grid .col {
    display: table-cell;
    Float: none !important;
    vertical-align: top; }
    .block-grid .col.num12 {
      width: 505px !important; }
  .block-grid.two-up .col {
    width: 252px !important; }
  .block-grid.three-up .col {
    width: 168px !important; }
  .block-grid.four-up .col {
    width: 126px !important; }
  .block-grid.five-up .col {
    width: 101px !important; }
  .block-grid.six-up .col {
    width: 84px !important; }
  .block-grid.seven-up .col {
    width: 72px !important; }
  .block-grid.eight-up .col {
    width: 63px !important; }
  .block-grid.nine-up .col {
    width: 56px !important; }
  .block-grid.ten-up .col {
    width: 50px !important; }
  .block-grid.eleven-up .col {
    width: 45px !important; }
  .block-grid.twelve-up .col {
    width: 42px !important; } }

@media (max-width: 525px) {
  .block-grid, .col {
    min-width: 320px !important;
    max-width: 100% !important; }
  .block-grid {
    width: calc(100% - 40px) !important; }
  .col {
    width: 100% !important; }
    .col > div {
      margin: 0 auto; }
  img.fullwidth {
    max-width: 100% !important; } }

    </style>
      <div class="nl-container" style="min-widht: 320px;Margin: 0 auto;background-color: #FFFFFF">

    <div style="background-color:transparent;">
      <div rel="col-num-container-box" style="Margin: 0 auto;min-width: 320px;max-width: 505px;width: 320px;width: calc(19500% - 101870px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid ">
        <div style="border-collapse: collapse;display: table;width: 100%;">
          <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td  style="background-color:transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 505px;"><tr class="layout-full-width" style="background-color:transparent;"><td class="corner__x">&nbsp;</td><![endif]-->

              <!--[if (mso)|(IE)]><td align="center"  width="505" style=" width:505px; border-top: 0px solid transparent; border-left: 0px solid transparent;  border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><![endif]-->
            <!--[if !mso]><!--><div rel="col-num-container-box" class="col num12" style="min-width: 320px;max-width: 505px;width: 320px;width: calc(18500% - 92920px);background-color: transparent;">
               <div style="background-color: transparent; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"><!--<![endif]-->
                
                  
<div align="center" style="Margin-right: 0px;Margin-left: 0px;">
  <a href="https://www.kippy.eu" target="_blank">
    <img class="center fullwidth" align="center" border="0" src="https://www.kippy.eu/dem/service/images/logo-kippy.png" alt="Image" title="Image" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block;border: none;height: auto;width: 100%;max-width: 505px" width="505">
  </a>

</div>
                  
                              <!--[if !mso]><!--></div>
            </div><!--<![endif]-->
          <!--[if (mso)|(IE)]></td><td class="corner__x">&nbsp;</td></tr></table></td></tr></table><![endif]-->
        </div>
      </div>
    </div>    <div style="background-color:transparent;">
      <div rel="col-num-container-box" style="Margin: 0 auto;min-width: 320px;max-width: 505px;width: 320px;width: calc(19500% - 101870px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid ">
        <div style="border-collapse: collapse;display: table;width: 100%;">
          <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td  style="background-color:transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 505px;"><tr class="layout-full-width" style="background-color:transparent;"><td class="corner__x">&nbsp;</td><![endif]-->

              <!--[if (mso)|(IE)]><td align="center"  width="505" style=" width:505px; border-top: 0px solid transparent; border-left: 0px solid transparent;  border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><![endif]-->
            <!--[if !mso]><!--><div rel="col-num-container-box" class="col num12" style="min-width: 320px;max-width: 505px;width: 320px;width: calc(18500% - 92920px);background-color: transparent;">
               <div style="background-color: transparent; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"><!--<![endif]-->
                
                  
<div style="Margin-right: 15px; Margin-left: 15px;">
  <div style="line-height: 15px; font-size: 1px">&nbsp;</div>
	<div style="font-size:12px;line-height:18px;font-family:&quot;Trebuchet MS&quot;, &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Sans&quot;, Tahoma, sans-serif;color:#58c5da;text-align:left;"><p style="margin: 0;font-size: 12px;line-height: 18px;text-align: center"><span style="font-size: 22px; line-height: 33px;">YOUR SERVICE PACKAGE</span></p><p style="margin: 0;font-size: 12px;line-height: 18px;text-align: center"><span style="font-size: 22px; line-height: 33px;"><?=$nome_pacchetto?></span></p><p style="margin: 0;font-size: 12px;line-height: 18px;text-align: center"><span style="font-size: 22px; line-height: 33px;">HAS BEEN RENEWED</span></p></div>

  <div style="line-height: 15px; font-size: 1px">&nbsp;</div>
</div>
                  
                              <!--[if !mso]><!--></div>
            </div><!--<![endif]-->
          <!--[if (mso)|(IE)]></td><td class="corner__x">&nbsp;</td></tr></table></td></tr></table><![endif]-->
        </div>
      </div>
    </div>    <div style="background-color:transparent;">
      <div rel="col-num-container-box" style="Margin: 0 auto;min-width: 320px;max-width: 505px;width: 320px;width: calc(19500% - 101870px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid ">
        <div style="border-collapse: collapse;display: table;width: 100%;">
          <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td  style="background-color:transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 505px;"><tr class="layout-full-width" style="background-color:transparent;"><td class="corner__x">&nbsp;</td><![endif]-->

              <!--[if (mso)|(IE)]><td align="center"  width="505" style=" width:505px; border-top: 0px solid transparent; border-left: 0px solid transparent;  border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><![endif]-->
            <!--[if !mso]><!--><div rel="col-num-container-box" class="col num12" style="min-width: 320px;max-width: 505px;width: 320px;width: calc(18500% - 92920px);background-color: transparent;">
               <div style="background-color: transparent; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"><!--<![endif]-->
                
                  
<div align="center" style="Margin-right: 0px;Margin-left: 0px;">

  <img class="center" align="center" border="0" src="https://www.kippy.eu/dem/service/images/rinnovo-pacchetto.png" alt="Image" title="Image" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block;border: 0;height: auto;width: 380px;max-width: 380px" width="380">
</div>
                  
                              <!--[if !mso]><!--></div>
            </div><!--<![endif]-->
          <!--[if (mso)|(IE)]></td><td class="corner__x">&nbsp;</td></tr></table></td></tr></table><![endif]-->
        </div>
      </div>
    </div>    <div style="background-color:transparent;">
      <div rel="col-num-container-box" style="Margin: 0 auto;min-width: 320px;max-width: 505px;width: 320px;width: calc(19500% - 101870px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid ">
        <div style="border-collapse: collapse;display: table;width: 100%;">
          <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td  style="background-color:transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 505px;"><tr class="layout-full-width" style="background-color:transparent;"><td class="corner__x">&nbsp;</td><![endif]-->

              <!--[if (mso)|(IE)]><td align="center"  width="505" style=" width:505px; border-top: 0px solid transparent; border-left: 0px solid transparent;  border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><![endif]-->
            <!--[if !mso]><!--><div rel="col-num-container-box" class="col num12" style="min-width: 320px;max-width: 505px;width: 320px;width: calc(18500% - 92920px);background-color: transparent;">
               <div style="background-color: transparent; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"><!--<![endif]-->
                
                  
<div style="Margin-right: 15px; Margin-left: 15px;">
  <div style="line-height: 20px; font-size: 1px">&nbsp;</div>
	<div style="font-size:12px;line-height:18px;color:#616161;font-family:'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;text-align:left;"><p style="margin: 0;font-size: 12px;line-height: 18px;text-align: center"><span style="font-size: 14px; line-height: 21px;">You can keep using the services until <?=$scadenza?>.</span></p><p style="margin: 0;font-size: 12px;line-height: 18px;text-align: center">&nbsp;<br></p><p style="margin: 0;font-size: 12px;line-height: 18px;text-align: center"><span style="font-size: 14px; line-height: 21px;">To change your service package,</span></p><p style="margin: 0;font-size: 12px;line-height: 18px;text-align: center"><span style="font-size: 14px; background-color: transparent; line-height: 21px;">you only have to access the App or the Web App through your account</span></p><p style="margin: 0;font-size: 12px;line-height: 18px;text-align: center"><span style="font-size: 14px; background-color: transparent; line-height: 21px;"> and select service management from the "Pet Profile" menu.</span><br></p></div>

  <div style="line-height: 20px; font-size: 1px">&nbsp;</div>
</div>
                  
                              <!--[if !mso]><!--></div>
            </div><!--<![endif]-->
          <!--[if (mso)|(IE)]></td><td class="corner__x">&nbsp;</td></tr></table></td></tr></table><![endif]-->
        </div>
      </div>
    </div>    <div style="background-color:transparent;">
      <div rel="col-num-container-box" style="Margin: 0 auto;min-width: 320px;max-width: 505px;width: 320px;width: calc(19500% - 101870px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid ">
        <div style="border-collapse: collapse;display: table;width: 100%;">
          <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td  style="background-color:transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 505px;"><tr class="layout-full-width" style="background-color:transparent;"><td class="corner__x">&nbsp;</td><![endif]-->

              <!--[if (mso)|(IE)]><td align="center"  width="505" style=" width:505px; border-top: 0px solid transparent; border-left: 0px solid transparent;  border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><![endif]-->
            <!--[if !mso]><!--><div rel="col-num-container-box" class="col num12" style="min-width: 320px;max-width: 505px;width: 320px;width: calc(18500% - 92920px);background-color: transparent;">
               <div style="background-color: transparent; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"><!--<![endif]-->
                  <div style="line-height: 5px; font-size:1px">&nbsp;</div>
                
                  
<div align="center" style="Margin-right: 5px;Margin-left: 5px;">

  <img class="center" align="center" border="0" src="https://www.kippy.eu/dem/service/images/zampette.png" alt="Image" title="Image" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block;border: 0;height: auto;width: 47px;max-width: 47px" width="47">
  <div style="line-height:25px;font-size:1px">&nbsp;</div>
</div>
                  
                  
<div align="center" style="Margin-right: 0px;Margin-left: 0px;">

  <img class="center fullwidth" align="center" border="0" src="https://www.kippy.eu/dem/service/images/ombra-footer.png" alt="Image" title="Image" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block;border: 0;height: auto;width: 100%;max-width: 505px" width="505">
</div>
                  
                  

<div align="center" style="Margin-right: 10px; Margin-left: 10px; Margin-bottom: 10px;">
  <div style="line-height:10px;font-size:1px">&nbsp;</div>
  <div style="display: table; max-width:183px;">
  <!--[if (mso)|(IE)]><table width="183" align="center" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; mso-table-lspace: 0pt;mso-table-rspace: 0pt; width:183px;"><tr><td width="42" style="width:42px;" valign="top"><![endif]-->
    <table align="left" border="0" cellspacing="0" cellpadding="0" width="32" height="32" style="border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 10px">
      <tbody><tr style="vertical-align: top"><td align="left" valign="middle" style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
        <a href="https://www.facebook.com/kippypet" title="Facebook" target="_blank">
          <img src="https://www.kippy.eu/dem/service/images/facebook@2x.png" alt="Facebook" title="Facebook" width="32" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block;border: none;height: auto;max-width: 32px !important">
        </a>
      <div style="line-height:5px;font-size:1px">&nbsp;</div>
      </td></tr>
    </tbody></table>
      <!--[if (mso)|(IE)]></td><td width="42" style="width:42px;" valign="top"><![endif]-->
    <table align="left" border="0" cellspacing="0" cellpadding="0" width="32" height="32" style="border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 10px">
      <tbody><tr style="vertical-align: top"><td align="left" valign="middle" style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
        <a href="http://www.twitter.com/KippyPet" title="Twitter" target="_blank">
          <img src="https://www.kippy.eu/dem/service/images/twitter@2x.png" alt="Twitter" title="Twitter" width="32" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block;border: none;height: auto;max-width: 32px !important">
        </a>
      <div style="line-height:5px;font-size:1px">&nbsp;</div>
      </td></tr>
    </tbody></table>
      <!--[if (mso)|(IE)]></td><td width="42" style="width:42px;" valign="top"><![endif]-->
    <table align="left" border="0" cellspacing="0" cellpadding="0" width="32" height="32" style="border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 10px">
      <tbody><tr style="vertical-align: top"><td align="left" valign="middle" style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
        <a href="http://instagram.com/kippypet" title="Instagram" target="_blank">
          <img src="https://www.kippy.eu/dem/service/images/instagram@2x.png" alt="Instagram" title="Instagram" width="32" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block;border: none;height: auto;max-width: 32px !important">
        </a>
      <div style="line-height:5px;font-size:1px">&nbsp;</div>
      </td></tr>
    </tbody></table>
      <!--[if (mso)|(IE)]></td><td width="42" style="width:42px;" valign="top"><![endif]-->
    <table align="left" border="0" cellspacing="0" cellpadding="0" width="32" height="32" style="border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 0">
      <tbody><tr style="vertical-align: top"><td align="left" valign="middle" style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
        <a href="https://www.youtube.com/user/kippypet" title="YouTube" target="_blank">
          <img src="https://www.kippy.eu/dem/service/images/youtube@2x.png" alt="YouTube" title="YouTube" width="32" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block;border: none;height: auto;max-width: 32px !important">
        </a>
      <div style="line-height:5px;font-size:1px">&nbsp;</div>
      </td></tr>
    </tbody></table>
    <!--[if (mso)|(IE)]></td></tr></table><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td>&nbsp;</td></tr></table><![endif]-->
  </div>
</div>
                  
                  
<div style="Margin-right: 10px; Margin-left: 10px;">
  <div style="line-height: 10px; font-size: 1px">&nbsp;</div>
	<div style="font-size:12px;line-height:14px;font-family:&quot;Trebuchet MS&quot;, &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Sans&quot;, Tahoma, sans-serif;color:#616161;text-align:left;"><p style="margin: 0;font-size: 12px;line-height: 14px;text-align: center"><span style="font-size: 14px; line-height: 16px;">Follow us!</span></p></div>

  <div style="line-height: 10px; font-size: 1px">&nbsp;</div>
</div>
                  
                  
<div align="center" style="Margin-right: 0px;Margin-left: 0px;">

  <img class="center" align="center" border="0" src="https://www.kippy.eu/dem/service/images/linea_footer.jpg" alt="Image" title="Image" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block;border: 0;height: auto;width: 156px;max-width: 156px" width="156">
</div>
                  
                  
<div style="Margin-right: 10px; Margin-left: 10px;">
  <div style="line-height: 10px; font-size: 1px">&nbsp;</div>
	<div style="font-size:12px;line-height:14px;font-family:&quot;Trebuchet MS&quot;, &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Sans&quot;, Tahoma, sans-serif;color:#555555;text-align:left;"><p style="margin: 0;font-size: 14px;line-height: 17px;text-align: center"><span style="font-size: 12px; line-height: 14px;">© KIPPY Srl - customerservice@kippy.eu</span></p></div>

  <div style="line-height: 10px; font-size: 1px">&nbsp;</div>
</div>
                  
                                  <div style="line-height: 5px; font-size: 1px">&nbsp;</div>
              <!--[if !mso]><!--></div>
            </div><!--<![endif]-->
          <!--[if (mso)|(IE)]></td><td class="corner__x">&nbsp;</td></tr></table></td></tr></table><![endif]-->
        </div>
      </div>
    </div>  </div>