<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\BaseMessage instance of newly created mail message */

?>
<h3>Service Kippy: User </h3>

<br>

    <p>User <b><?php echo $this->params['username'];?></b> with account email adress: <?php echo $this->params['user_acc_email'];?> cancel subscription.</p>


    <p>User message:</p>
    <p><?php echo $this->params['user_message'];?></p>

    <p>reasons why User cancel subscription:</p>

    <p><?php echo $this->params['reason'];?></p>

<!--<ul>-->
<!--    <li class="text-center">--><?php //echo $this->params['reason1'];?><!--</li>-->
<!--    <li class="text-center">--><?php //echo $this->params['reason2'];?><!--</li>-->
<!--    <li class="text-center">--><?php //echo $this->params['reason3'];?><!--</li>-->
<!--    <li class="text-center">--><?php //echo $this->params['reason4'];?><!--</li>-->
<!--    <li class="text-center">--><?php //echo $this->params['reason5'];?><!--</li>-->
<!--    <li class="text-center">--><?php //echo $this->params['reason6'];?><!--</li>-->
<!--</ul>-->


<?= Html::a('Go to home page', Url::home('http')) ?>