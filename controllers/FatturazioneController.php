<?php
namespace app\controllers;


use yii\rest\Controller;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\VerbFilter;
use app\models\Abbonamenti;
use app\models\Infofatturazione;
use app\helper\DecodeLang;

class FatturazioneController extends Controller
{
    public $modelClass = 'app\models\Infofatturazione';
    

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
            'auth' => [$this , 'auth']
        ];
        
        
        return $behaviors;
    }
    public function auth($username, $password)
    {
        return \app\models\Account::findOne(
            [
                'username' => $username,
                'access_token' => $password
            ]
        );

    }
    public function actionCreate(){
        $post=json_decode(file_get_contents('php://input'));
        
        $serial_number = (isset($post->serial_number))?$post->serial_number:null;
        $scadenza = (isset($post->scadenza))?$post->scadenza:null;
        $language = (isset($post->language))?$post->language:null;
        
        if($serial_number ==null || $scadenza ==null) 
        return ['success'=>false,'error'=>'Mancano uno o più paramentri obbligatori'];
        
        $exist = Abbonamenti::find()->where(['serial_number'=>$serial_number,'status'=>1])->exists();
        
        if(!$exist) 
        return ['success'=>true,'message'=>'Non esiste un pacchetto servizi attivo con questo serial number'];
        
        $abbonamento = Abbonamenti::find()->where(['serial_number'=>$serial_number,'status'=>1])->one();

        $scadenza_old = $abbonamento->scadenza;
        
        $scadenza_new = strtotime($scadenza);
        
        $lingua = DecodeLang::getCode($language);
        
        if($scadenza_new > $scadenza_old){
            //omaggio
            
            switch ($lingua){
            case 'it-it':
                $descrizione = 'Periodo extra in omaggio - Customer Service Kippy';
                break;
            case 'fr-fr':
                $descrizione = 'Période supplémentaire gratuite - Customer Service Kippy';
                break;
            case 'de-de':
                $descrizione = 'Freier Zeitraum - Customer Service Kippy';
                break;
            case 'es-es':
                $descrizione = 'Período extra gratuito - Customer Service Kippy';
                break;
            case 'en-gb':
                $descrizione = 'Free extra period - Customer Service Kippy';
                break;
            }
            
        }else{
            //downgrade
            
            switch ($lingua){
            case 'it-it':
                $descrizione = 'Modificato dal Customer Service Kippy'; 
                break;
            case 'fr-fr':
                $descrizione = 'Modifié par le Customer Service Kippy';
                break;
            case 'de-de':
                $descrizione = 'Geändert von Customer Service Kippy';
                break;
            case 'es-es':
                $descrizione = 'Modificado por el Customer Service Kippy';
                break;
            case 'en-gb':
                $descrizione = 'Modified by Customer Service Kippy';
                break;
            }
        }
        
        $abbonamento->scadenza = $scadenza_new;
        
        if($abbonamento->save()){
            //l'abbonamento è stato modificato scrivo la riga info fatturazioni
            $riga = new Infofatturazione();
            $riga->id_abbonamento = $abbonamento->id;
            $riga->id_ordine = null;
            $riga->note = $descrizione;
            $riga->start_period = $scadenza_old;
            $riga->end_period = $scadenza_new;
            $riga->save();
            
            $response = [
                'success'=>true,
                'messagge'=>'Pacchetto esistente ed aggiornato correttamente',
            ];
            
        }else{
            $response = [
                'success'=>false,
                'error'=>'Pacchetto esistente. Aggiornamento fallito. Riprova.',
            ];
        }
  
        
        return $response;
    }
    
    
}