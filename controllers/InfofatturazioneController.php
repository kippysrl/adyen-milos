<?php
namespace app\controllers;


use yii\rest\Controller;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\VerbFilter;
use app\models\Abbonamenti;
use app\models\Infofatturazione;


class InfofatturazioneController extends Controller
{
    public $modelClass = 'app\models\Infofatturazione';
    

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
            'auth' => [$this , 'auth']
        ];
        
        
        return $behaviors;
    }
    public function auth($username, $password)
    {
        return \app\models\Account::findOne(
            [
                'username' => $username,
                'access_token' => $password
            ]
        );

    }
    public function actionCreate(){
        $post=json_decode(file_get_contents('php://input'));
        
        $serial_number = (isset($post->serial_number))?$post->serial_number:null;
        
        if($serial_number ==null) 
        return ['success'=>false,'error'=>'Mancano uno o più paramentri obbligatori'];
        
        $exist = Abbonamenti::find()->where(['serial_number'=>$serial_number])->exists();
        
        if(!$exist) 
        return ['success'=>true,'message'=>'Non esiste un pacchetto servizi con questo serial number'];
        
        $abbonamenti = Abbonamenti::find()->where(['serial_number'=>$serial_number])->all();

        $response = [
            'success'=>true,
            'abbonamenti'=>[]
        ];
        
        foreach ($abbonamenti as $abbonamento):
            $infofatturazioni = Infofatturazione::find()->where(['id_abbonamento'=>$abbonamento->id])->all();
            $pagamenti = [];
            foreach($infofatturazioni as $infofatturazione):
                $pagamenti[]=[
                    'id_ordine'=>$infofatturazione->id_ordine,
                    'start_period'=>date("Y-m-d",$infofatturazione->start_period),
                    'end_period'=>date("Y-m-d",$infofatturazione->end_period),
                    'created_at' => date("Y-m-d",$infofatturazione->created_at),
                    'updated_at' => date("Y-m-d",$infofatturazione->updated_at),
                ];
            endforeach;
            $response['abbonamenti'][] = [
                                    'id'=>$abbonamento->id,
                                    'serial_number'=>$abbonamento->serial_number,
                                    'imei' => $abbonamento->kippy_imei,
                                    'nome_pacchetto'=>$abbonamento->idProdotto->codice,
                                    'durata_mesi' => $abbonamento->durata_mesi,
                                    'scadenza'=>date("Y-m-d",$abbonamento->scadenza),
                                    'tipo_pagamento' => $abbonamento->pagamento,
                                    'valuta' => $abbonamento->sigla_valuta,
                                    'importo' => $abbonamento->importo,
                                    'attivo' => $abbonamento->status,
                                    'disdetta' => $abbonamento->disdetta,
                                    'sospeso' => $abbonamento->sospeso,
                                    'rinnovi_falliti' => $abbonamento->rinnovi_falliti,
                                    'created_at' => date("Y-m-d",$abbonamento->created_at),
                                    'updated_at' => date("Y-m-d",$abbonamento->updated_at),
                                    'info_fatturazione'=>$pagamenti
                                ]; 
        endforeach;
        
        
        
        return $response;
    }
    
    
}