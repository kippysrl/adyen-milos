<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Abbonamenti;
use app\models\Account;
use app\models\Ordini;
use app\models\RigheOrdini;
use app\models\Prodotti;
use app\models\Listino;
use app\models\Infofatturazione;
use app\helper\DecodeError;
use app\models\Country;
use Httpful\Request;

class RecurringController extends Controller 
{
    protected $admin_user = 'p.giarda@jellyfishadv.com';
    protected $admin_password = 'kippyjelly';
    protected $basic_username =  'AdminKippy';
    protected $basic_password = 'zR1Mczi0yv';
    
    public function actionIndex()
    {
        
        return $this->render('index');
    }
    public function actionTestAdyenRecurring() {
       //Set up a client and set the credentials to the Adyen platform.
        $client = new \Adyen\Client();
        $client->setApplicationName("Adyen checkout Kippy");
        $client->setUsername(Yii::$app->params['Adyen_user']);
        $client->setPassword(Yii::$app->params['Adyen_psw']);
        $client->setEnvironment(\Adyen\Environment::TEST);
        
        // initialize service
        $service = new \Adyen\Service\Payment($client);
        
        $params['amount']['value']= 1000;
        $params['amount']['currency']= 'EUR';
        
        $params['reference']= 'kippy_service_'.time();
        $params['merchantAccount']= Yii::$app->params['Adyen_merchantAccount'];
        
        $params['shopperReference'] = '26088';
        $params['shopperEmail'] = 'p.giarda@jellyfishadv.com';
        $params['shopperIP'] = Yii::$app->request->getUserIP();
        
        $params['selectedRecurringDetailReference'] = "LATEST";
        
        $params['recurring']['contract'] = 'RECURRING';
        
        $params['shopperInteraction']= 'ContAuth'; 
        
        $result = $service->authorise($params);
        echo '<pre>';
        print_r($result);
    }

//    public function actionScadenza($imei="860854020826920"){
//        if($this->LoginAdmin()) echo'login ok!<br>';
//         $auth = Yii::$app->session->get('auth');
//        $response = $this->ApiGetKippySubscription($auth['app_code'],$auth['app_verification_code'], $imei);
//        print_r($response);
//    }
    public function actionCheckScadenzaSella(){
        $today = strtotime(date('Y/m/d',time()));
        
        //$today = strtotime('2017/09/24'); //timestamp di oggi fingo di essere al 07/09/17
        $finestra = $today + (1*86400);
        
        $inscadenza = Abbonamenti::find()->where(['status'=>1,'pagamento'=>'bancasella'])->andWhere(['<=', 'scadenza',$finestra] )->all();
        
        $count = count($inscadenza);
        
        echo 'Trovati ' . $count . ' pacchetti in scadenza<br>'; 
        
        if($count <1 ) exit();
        if($this->LoginAdmin()) echo'login ok!<br>';
        $auth = Yii::$app->session->get('auth');
        
        foreach ($inscadenza as $rinnovo):
            //devo verificare se la scadenza dell'abbonamento coincide con quella di akerue
            
            $response = $this->ApiGetKippySubscription($auth['app_code'],$auth['app_verification_code'], $rinnovo->kippy_imei);
            if($response->return ==0) $fine = strtotime($response->subscription_end);
            echo '<br>Scadenza pacchetto kippy '.$rinnovo->serial_number.': ' . date('Y-m-d' , $rinnovo->scadenza) . ' - Scadenza dispositivo : ' .date('Y-m-d' ,$fine ). '<br>';            

        endforeach;
        
    }
    public function actionInScadenzaSella(){
        $today = strtotime(date('Y/m/d',time()));
        
        //$today = strtotime('2017/09/24'); //timestamp di oggi fingo di essere al 07/09/17
        $finestra = $today + (1*86400);
        
        $inscadenza = Abbonamenti::find()->where(['status'=>1,'pagamento'=>'bancasella'])->andWhere(['<=', 'scadenza',$finestra] )->limit(20)->all();
        
        $count = count($inscadenza);
        
        echo 'Trovati ' . $count . ' pacchetti in scadenza<br>'; 
        
        if($count <1 ) exit();
        if($this->LoginAdmin()) echo'login ok!<br>';
        $auth = Yii::$app->session->get('auth');
        
        foreach ($inscadenza as $rinnovo):
            //devo verificare se la scadenza dell'abbonamento coincide con quella di akerue
            
            $response = $this->ApiGetKippySubscription($auth['app_code'],$auth['app_verification_code'], $rinnovo->kippy_imei);
            if($response->return ==0) $fine = strtotime($response->subscription_end);
             echo '<br>Scadenza pacchetto kippy '.$rinnovo->serial_number.': ' . date('Y-m-d',$rinnovo->scadenza) . ' - Scadenza dispositivo : ' .date('Y-m-d',$fine) . '<br>';      
            //qui se le due sono diverse (vuol dire che c'è stato un omaggio) devo uscire dal ciclo
            
            if($fine > $rinnovo->scadenza){
                $abbonamento = Abbonamenti::findOne($rinnovo->id);
                $abbonamento->scadenza = $fine;
                $abbonamento->save();
                echo 'Operazione interrotta perchè la scadenza del  dispositivo è superiore a scadenza pacchetto; ho aggiornato la data scadenza del pacchetto id ' . $rinnovo->id . ' con la data di scadenza reale del dispositivo.<br>';
                
                continue;
            }  
             
            echo '</br>';
            $order = $this->createOrder($rinnovo);
            if($order->errors):
            echo 'Ordine non creato, c’è stato un errore sul pacchetto id ' . $rinnovo->id . '<br>';
            print_r($order->errors);
            else:
                echo 'Creato ordine numero ' .$order->id . ' per pacchetto id ' . $rinnovo->id . '<br>';
                
                echo $this->CallBancasella($order,$rinnovo);
                
                
                
            endif;

        
        endforeach;
        
    }
    protected function createOrder($rinnovo){
        
        $model = new Ordini;  
        
        $model->ip=Yii::$app->request->getUserIP();
        $populate = $this->getAccount($rinnovo->id_account);
        $model->fattura_nome = $model->nome = $populate->nome;
        $model->fattura_cognome = $model->citofono = $model->cognome = $populate->cognome;
        $model->fattura_indirizzo = $model->indirizzo = $populate->indirizzo;
        $model->fattura_cap = $model->cap = $populate->cap;
        $model->fattura_citta = $model->citta = $populate->citta;
        $model->fattura_provincia = $model->provincia = $populate->provincia;
        $model->fattura_nazione = $model->nazione = $populate->nazione;
        $model->fattura_email = $model->email = $populate->email;
        $model->telefono = $populate->telefono;
        $model->ragione_sociale = $populate->ragione_sociale;
        $model->partita_iva = $populate->partita_iva;
        $model->status=1;//confermata fase uno
        if($model->save()){
            $riga = new RigheOrdini(); 
            
            $prodotto = $this->getProdotto($rinnovo->id_prodotto);
            //compilo i campi
            $riga->id_prodotto=$rinnovo->id_prodotto;
            $riga->id_ordine=$model->id;

            $riga->codice_prodotto=$prodotto->codice;
            $riga->descrizione_prodotto= ($model->nazione=='IT')?$prodotto->descrizione_it:$prodotto->descrizione_en;
            $riga->sigla_valuta= $rinnovo->sigla_valuta;
            $riga->prezzo_ivato= $rinnovo->importo;
            $riga->quantita=1;
            $riga->stato_riga='pending';


            if($riga->save())
            return $model;  
            else 
            return $riga;    
        }
        return $model;
        
    }
    protected function getProdotto($id)
    {  
        return Prodotti::findOne($id);
    }
    protected function getRiga($id)
    {  
        return RigheOrdini::findOne(['id_ordine'=>$id]);
    }
    protected function getAccount($user_id)
    {  
        return Account::findOne($user_id);
    }
    
    protected function CallBancasella($model,$rinnovo){
        //parametri necessari
        $totale = $rinnovo->importo;

        //mappo le lingue per banca sella
        switch ($model->nazione) {
                case 'IT':
                        $lingua_sella=1;
                        break;
                case 'FR':
                        $lingua_sella=4;
                        break;
                case 'DE':
                        $lingua_sella=5;
                        break;
                case 'ES':
                        $lingua_sella=3;
                        break;		
                default:
                        $lingua_sella=2;
                        break;
        }
        //mappo le valute per sella
        $valuta = Listino::find()->where(['fk_prodotto'=>$rinnovo->id_prodotto , 'fk_paese'=>$model->nazione])->one()->fk_valuta;
        switch ($valuta) {
                case 'CHF':
                        $currencySella='3';
                        break;
                case 'CZK':
                        $currencySella='223';
                        break;
                case 'DKK':
                        $currencySella='7';
                        break;
                case 'EUR':
                        $currencySella='242';
                        break;
                case 'GBP':
                        $currencySella='2';
                        break;
                case 'HUF':
                        $currencySella='153';
                        break;
                case 'NOK':
                        $currencySella='8';
                        break;
                case 'PLN':
                        $currencySella='237';
                        break;
                case 'SEK':
                        $currencySella='9';
                        break;								
                default:
                        $currencySella='242';
                        break;
        }

        //Setting up the basic parameter set to retrieve an encrypted string form GestPay
        $shopLogin = Yii::$app->params['shopLogin']; //YOUR SHOP LOGIN Eg. production code '9000001' , test code 'gespay65242'
        $currency = $currencySella; //switch var in top
        $amount = number_format($totale,2,'.',''); //payment amount
        $shopTransactionID = 'KIPPY_'.$model->id;//your payment order identifier
        $languageId = $lingua_sella; //switch var in top
        $BuyerEmail = ':NOSEND:'.$model->email;
        $BuyerName = $model->nome.' '.$model->cognome;
        $tokenValue = $rinnovo->token;
        //Custom fileds
        $customInfo = Yii::$app->params['customInfo'];//'custom_instance=000*P1*custom_redirect=000';
        
        //load the NuSoap toolkit
        require_once("../gestpay/nusoap.php");
        //setting up the WSLD url
        $wsdl = Yii::$app->params['gestpay'] . "gestpay/GestPayWS/WsS2S.asmx?wsdl";
        //NuSoap client
        $client = new \nusoap_client($wsdl,true); 
        //setting up the parameters array
        $param = array('shopLogin' => $shopLogin, 'shopTransactionId' => $shopTransactionID, 'uicCode' => $currency, 'amount' => $amount, 'buyerName' => $BuyerName, 'buyerEmail' => $BuyerEmail,  'languageId' => $languageId,  'tokenValue' => $tokenValue);//'customInfo' => $customInfo,
        //Call the callPagamS2S method
        $objectresult = $client->call('callPagamS2S', $param);
        //Check for call error 
        $err = $client->getError();
        if ($err) {
                // A call error has occurred 	    	    
                // Display the error
                $finalResult = 'chiamata a GestPay non riuscita';

        } else {
                
                $finalResult = $this->Responsella($objectresult,$model,$rinnovo);
        }
        return $finalResult;
    }
    
    protected function Responsella($objectresult,$order,$abbonamento){

            //leggo risultato
            $decript = $objectresult['callPagamS2SResult']['GestPayS2S'];
            $result=$decript['TransactionResult'];
            $error = $decript['ErrorDescription'];
           
            //recupero i campi per terashop e salvo nel db
           
            $order->CC_Autorizzazione = isset($decript['AuthorizationCode'])?$decript['AuthorizationCode']:'';
            $order->CC_Addebito = isset($decript['Amount'])?$decript['Amount']:'';
            $order->CC_Id_Errore = isset($decript['ErrorCode'])?$decript['ErrorCode']:'';
            $order->CC_Descrizione_Errore = isset($decript['ErrorDescription'])?$decript['ErrorDescription']:'';
            $order->CC_Allert_Code = isset($decript['AlertCode'])?$decript['AlertCode']:'';
            $order->CC_Verified_by_Visa = isset($decript['VbV']['VbVFlag'])?$decript['VbV']['VbVFlag']:'';
            $order->CC_Paese = isset($decript['Country'])?$decript['Country']:'';
            $order->CC_Transazione = isset($decript['BankTransactionID'])?$decript['BankTransactionID']:''; 
            $order->CC_Data_Addebito = date('d/m/Y H:i:s'); 
            $order->save();

            if($result=='KO'){
                // incremento i tentativi di rinnovo falliti. 
                $falliti = $abbonamento->rinnovi_falliti = $abbonamento->rinnovi_falliti + 1;
                if($falliti == 3){//se arrivo al terzo tentativo disattivo e sospendo l'abbonamento
                    $abbonamento->status=0;
                    $abbonamento->sospeso=1;
                }else{
                    //qui calcolo se sono  più vicino al 6 o al 16 del mese e porto allungo la scadenza del kippy a quel giorno
                    $scadenza_day = date('d', $abbonamento->scadenza);
                    $scadenza_month = date('m', $abbonamento->scadenza);
                    $scadenza_year = date('Y', $abbonamento->scadenza);
                    
                    if(intval($scadenza_day)<=5){
                        $new_day = 6;
                        $incrementa_month = 0;
                    }elseif(intval($scadenza_day)>=16){
                        $new_day = 6;
                        $incrementa_month = 1;
                    }else{//compreso fra 6 e 15 estremi inclusi
                        $new_day = 16;
                        $incrementa_month = 0;
                    }
                    
                    $scadenza_new =  mktime(0, 0, 0, $scadenza_month+$incrementa_month, $new_day,   $scadenza_year);
                    
                    //riscrivo la scadenza su akerue della sim su akerue 
                    $subscription_end = date('Y-m-d', $scadenza_new);
                    
                    if(($loggedin = $this->LoginAdmin())==true){
                        $auth = Yii::$app->session->get('auth');
                        $response = $this->ApiSetKippySubscription($auth['app_code'],$auth['app_verification_code'], $abbonamento->kippy_imei, $subscription_end);
                    }    
                    
                    //riscrivo la scadenza dell'abbonamento
                    $abbonamento->scadenza = $scadenza_new;
                }
                $abbonamento->save();
                // Display the error
                $esito = 'Transazione non eseguita: '. $error . '. ('.$falliti.' tentativo fallito.)';
                if($falliti == 3){
                    $esito .= ' Abbonamento Sospeso!';
                }
                
            }else{
                $pagamento='bancasella';
                $paese= $order->nazione;

                //scrivo ordine su csv e lo chiudo
                $this->writeCsv($order,$pagamento);
                
                //aggiorno abbonamento  
                 //calcolo la data scadenza faccio il max fra oggi e la data + i nuovi mesi
                $scadenza = $abbonamento->scadenza;
                $oggi = strtotime(date('Y-m-d'));
                $durata = $abbonamento->durata_mesi . " month";
                $scadenza_new = strtotime($durata ,max($scadenza, $oggi));
                
                $abbonamento->scadenza = $scadenza_new;
                $abbonamento->rinnovi_falliti =0;
                
                $abbonamento->save();
                //creo nuova riga su infofatturazione
                $Info = new Infofatturazione();
                $Info->id_abbonamento = $abbonamento->id;
                $Info->id_ordine = $order->id;
                $Info->start_period = max($scadenza, $oggi);
                $Info->end_period = $scadenza_new;   
                $Info->save();
                //aggiorno la scadenza della sim su akerue 
                $subscription_end = date('Y-m-d', $scadenza_new);
                if(($loggedin = $this->LoginAdmin())==false){
                    $esito ='Il pagamento è andato a buon fine, tuttavia, si è verificato un problema tecnico nell’aggiornamento del servizio, Avvertire il customer care';    
                    return $esito;
                }    
                $auth = Yii::$app->session->get('auth');
                $response = $this->ApiSetKippySubscription($auth['app_code'],$auth['app_verification_code'], $abbonamento->kippy_imei, $subscription_end);
                if($response->return !==0):
                    $esito ='Il pagamento è andato a buon fine, tuttavia, si è verificato un problema tecnico nell’aggiornamento del servizio, Avvertire il customer care';    
                    return $esito;
                endif;
                
                $esito = 'Transazione eseguita correttamente!';
                //@$this->sendRecurring($abbonamento->idProdotto->codice, date('d-m-Y',$scadenza_new), $order->email);
                
            }
            
            return $esito . '<br>';
    }
        //genero il file csv e scrivo l'ordine
    protected function writeCsv($ordine,$pagamento){

            $ordine->status=2; 
            $ordine->pagamento = $pagamento;
            if($pagamento=='bancasella') $ordine->ModPag='CCBS';
            if($pagamento=='paypal') $ordine->ModPag='PPLSTD';
            if($ordine->save(false)){
                $Righe= RigheOrdini::findAll(['id_ordine'=>$ordine->id]);  
                foreach ($Righe as $riga){
                    $riga->stato_riga='confirmed';
                    $riga->save(false);
                } 
            }
            //genero il csv per terashop
            $country=  Country::findOne(['sigla'=>$ordine->nazione]);
            $istanza=$country->istanza;
            $righeordine=array();
            if(!file_exists(Yii::$app->params['csvPath']."/ordini/ordini.csv"))
            $righeordine[] ='id ordine|Descrizione prodotto|Codice prodotto|Prezzo ivato|valuta|istanze|quantità|'
                . 'nome|cognome|Codice Fiscale|Indirizzo|citofono|comune/città|'
                . 'CAP|nazione|provincia/stato/Contea/Bund|email|telefono|pagamento|'
                . 'ragione_sociale|partita_iva|fattura_nome|fattura_cognome|fattura_indirizzo|'
                . 'fattura_citta|fattura_cap|fattura_nazione|fattura_provincia|fattura_email|'
                . 'ModPag|CC_Autorizzazione|CC_Addebito|CC_Id_Errore|CC_Descrizione_Errore|'
                . 'CC_Allert_Code|CC_Verified_by_Visa|CC_Paese|CC_Transazione|CC_Data_Addebito|id riga';
            foreach ($Righe as $riga){
                $righeordine[]=$ordine->id.'|'.$riga->descrizione_prodotto.'|'.$riga->codice_prodotto.'|'.$riga->prezzo_ivato.'|'.$riga->sigla_valuta.'|'.$istanza.'|'.$riga->quantita
                               .'|'.$ordine->nome.'|'.$ordine->cognome.'|'.$ordine->codice_fiscale.'|'.$ordine->indirizzo.'|'.$ordine->citofono.'|'.$ordine->citta
                               .'|'.$ordine->cap.'|'.$ordine->nazione.'|'.$ordine->provincia.'|'.$ordine->email.'|'.$ordine->telefono.'|'.$ordine->pagamento
                               .'|'.$ordine->ragione_sociale.'|'.$ordine->partita_iva.'|'.$ordine->fattura_nome.'|'.$ordine->fattura_cognome.'|'.$ordine->fattura_indirizzo
                               .'|'.$ordine->fattura_citta.'|'.$ordine->fattura_cap.'|'.$ordine->fattura_nazione.'|'.$ordine->fattura_provincia.'|'.$ordine->fattura_email
                               .'|'.$ordine->ModPag.'|'.$ordine->CC_Autorizzazione.'|'.$ordine->CC_Addebito.'|'.$ordine->CC_Id_Errore.'|'.$ordine->CC_Descrizione_Errore
                               .'|'.$ordine->CC_Allert_Code.'|'.$ordine->CC_Verified_by_Visa.'|'.$ordine->CC_Paese.'|'.$ordine->CC_Transazione.'|'.$ordine->CC_Data_Addebito. '|' . $riga->id;
            } 

            
            $file = fopen(Yii::$app->params['csvPath']."/ordini/ordini.csv","a+");

            foreach ($righeordine as $line)
              {
              $newline=  str_replace(";", ",", $line);//rimuovo eventuali punti e virgola se ci sono nei campi
              $newline=  str_replace("|", ";", $newline);//trasformo i pipe in punti e virgola per separare i campi
              fwrite($file, $newline. "\n");
              }

            fclose($file);
            
    }
    //    public function actionTestadem(){
//        $this->sendRecurring($nome_pacchetto = 'BASIC',$scadenza = '09/04/2017',$to = 's.chinchio@kippy.eu');
//    }
    //invio la dem di rinnovo pacchetto
    protected function sendRecurring($nome_pacchetto, $scadenza, $to)
    {
        switch (Yii::$app->language){
            case 'it-it':
                $sub = 'Il tuo pacchetto di servizi è stato rinnovato';
                break;
            case 'en-gb':
                $sub = 'Your Kippy service pack has been renewed';
                break;
            case 'fr-fr':
                $sub = 'Votre forfait de service a été renouvelé';
                break;
            case 'de-de':
                $sub = 'Ihr Service-paket Kippy wurde erfasst';
                break;
            case 'es-es':
                $sub = 'Tu paquete de servicios Kippy ha sido renovado';
                break;
            default:
                $sub = 'Your Kippy service pack has been renewed';// in inglese
                break;
        }
        $language = substr(Yii::$app->language, 0,2); 
        
        
        return Yii::$app->mailer->compose(['html'=>'ricorrenti_'.$language],[
                    'nome_pacchetto'=>$nome_pacchetto,
                    'scadenza'=>$scadenza,
                ]) 
	    ->setFrom('orders@kippy.eu')
	    ->setTo($to)
	    ->setSubject($sub)
	    ->send();
    }
    protected function LoginAdmin(){
                if(!Yii::$app->session->get('auth')):
                    //recupero e metto in session app_code e app_verification_code dell'admin
                    $admin = $this->ApiLogin($this->admin_user, $this->admin_password);
                    if($admin->return==0){
                        
                        $auth = [
                            'app_code'=>$admin->app_code,
                            'app_verification_code'=>$admin->app_verification_code,
                        ];
                        Yii::$app->session->set('auth', $auth);
                        return true;
                    }else{
                        return false;
                    }
                endif;  
                return true;
    }
    /************************************************************************
     *                                                                      *
     *                         API AKERUE / KIPPY.                          *    
     *                                                                      *
     ************************************************************************/
    protected function ApiLogin($login_email,$login_password,$app_identity="",$token_device="",$platform_device="",$app_version="",$device_name=""){
        $uri = Yii::$app->params['apiKippy'] . "/login.php";

        $response = Request::post($uri)
                    ->expectsJson()
                    ->sendsJson()                                      
                    ->body('{  "login_email" : "'.$login_email.'", '
                            . '"login_password" : "'.$login_password.'",'
                            . '"app_identity" : "'.$app_identity.'",'
                            . '"token_device" : "'.$token_device.'",'
                            . '"platform_device" : "'.$platform_device.'",'
                            . '"app_version" : "'.$app_version.'",'
                            . '"device_name" : "'.$device_name.'"'
                            . ' }')     
                    ->send();  
        
            return $response->body;
        
    }
    protected function ApiGetKippySubscription($app_code, $app_verification_code, $imei){
        $uri = Yii::$app->params['apiKippy'] . "/admin/admin_get_kippy_subscription.php";

        $response = Request::post($uri)
                    ->expectsJson() 
                    ->authenticateWith($this->basic_username, $this->basic_password)
                    ->body('{  "app_code" : "'.$app_code.'", '
                            . '"app_verification_code" : "'.$app_verification_code.'",'
                            . '"kippy_imei" : "'.$imei.'"'
                            . ' }')  
                    ->send();     
            
        return $response->body;  
    }
    protected function ApiSetKippySubscription($app_code, $app_verification_code, $imei, $scadenza){
        $uri = Yii::$app->params['apiKippy'] . "/admin/admin_set_kippy_subscription.php";

        $response = Request::post($uri)
                    ->expectsJson() 
                    ->authenticateWith($this->basic_username, $this->basic_password)
                    ->body('{  "app_code" : "'.$app_code.'", '
                            . '"app_verification_code" : "'.$app_verification_code.'",'
                            . '"kippy_imei" : "'.$imei.'",'
                            . '"subscription_end" : "'.$scadenza.'"'
                            . ' }')  
                    ->send();     
            
        return $response->body;  
    }
}
