<?php
namespace app\controllers;


use yii\rest\Controller;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\VerbFilter;



class AbbonamentiController extends Controller
{
    public $modelClass = 'app\models\Abbonamenti';
    

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
            'auth' => [$this , 'auth']
        ];
        
        
        return $behaviors;
    }
    public function auth($username, $password)
    {
        return \app\models\Account::findOne(
            [
                'username' => $username,
                'access_token' => $password
            ]
        );

    }

    public function actionCreate(){
        
        $post=json_decode(file_get_contents('php://input'));
        
        $old_serial_number = (isset($post->old_serial_number))?$post->old_serial_number:null;
        
        $serial_number = (isset($post->serial_number))?$post->serial_number:null;
        $imei = (isset($post->imei))?$post->imei:null;
        
        if($old_serial_number == null || $serial_number ==null || $imei == null) 
        return ['success'=>false,'error'=>'Mancano uno o più paramentri obbligatori'];
        
        $exist = \app\models\Abbonamenti::find()->where(['serial_number'=>$old_serial_number,'status'=>1])->exists();
        
        if(!$exist) 
        return ['success'=>true,'message'=>'Non esiste un pacchetto da aggiornare con questo serial number'];
        
        $giàusato = \app\models\Abbonamenti::find()->where(['serial_number'=>$serial_number,'status'=>1])->exists();
        
        if($giàusato) 
        return ['success'=>false,'error'=>'Esiste un già un pacchetto attivo con questo nuovo serial number'];
                
        $abbonamento = \app\models\Abbonamenti::findOne(['serial_number'=>$old_serial_number,'status'=>1]);

        
        $abbonamento->serial_number = trim($serial_number);
        $abbonamento->kippy_imei = trim($imei);
        
        if($abbonamento->save())
        return ['success'=>true,'abbonamento'=>['serial_number'=>$abbonamento->serial_number,'imei'=>$abbonamento->kippy_imei,'scadenza'=>date("Y-m-d",$abbonamento->scadenza)]];
        else
        return ['success'=>false,'error'=>'Riprova, non è stato possibile completare l’aggiornamento'];
        
        
    }
    
}