<?php

namespace app\controllers;

use app\models\SubscriptionCancel;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Ordini;
use app\models\RigheOrdini;
use Httpful\Request;
use app\helper\DecodeError;
use app\helper\DecodeLang;
use app\models\Account;
use app\helper\SortMultiArray;
use app\models\Prodotti;
use app\models\Listino;
use app\models\Abbonamenti;
use app\models\Country;
use app\models\Token;
use yii\base\Security;
use app\models\Infofatturazione;
use DateTime;
use app\helper\AdyenFunction;
use yii\base\DynamicModel;

use app\models\SepaCountryList;


//test
use Adyen\Service\Payment;
use Adyen\Service\Recurring;


//https://kippysrl@bitbucket.org/kippysrl/adyen-milos.git

class SiteController extends Controller
{
    protected $app_code;
    protected $app_verification_code;
    
    //credenziali admin dashboard
    protected $admin_user = 'p.giarda@jellyfishadv.com';
    protected $admin_password = 'kippyjelly';
    protected $basic_username =  'AdminKippy';
    protected $basic_password = 'zR1Mczi0yv';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout','index','add-to-cart','user-account','checkout-billing','checkout-payment','responsella','success','paypal','paypal-return','adyen-return','device','unsubscribe','adyen-card','get-merchant-sig'],
                'rules' => [
                    [
                        'actions' => ['logout','index','add-to-cart','user-account','checkout-billing','checkout-payment','responsella','success','paypal','paypal-return','adyen-return','device','unsubscribe','adyen-card','get-merchant-sig'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'logout' => ['post'],
                    'paypal' => ['post'],
                    'adyen-card' => ['post'],
                    'get-merchant-sig' => ['post'],
                ],
            ],
        ];
    }
    public static $menuPets=[];
    
    public function beforeAction($action)
    {
        if (in_array($action->id, ['get-kippy-token','checkout-payment'])) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        
        //ip Detection (eseguo solo se non trovo il paese in session o se lo trovo uguale a NULL per non sprecare chiamate) 
        if(!Yii::$app->session->get('paese')){
       
            $apiUrl ='https://api.db-ip.com/v2/';
            $apiKey = 'ed5dc4bb2f0bb311031d055b47e94a33f5ed7aed';


           $ipAddress = Yii::$app->request->getUserIP();///produzione

            if($ipAddress=='::1')$ipAddress= '37.182.223.20';
            
            $url =  $apiUrl . $apiKey . '/' .$ipAddress;
            $curl=@file_get_contents($url);
            if($curl===false){
                //se la chiamata in api non va a buon fine (es: il servizio è in down)
                // setto il paese a NULL
                $paese= NULL;
            }else{
                $response = json_decode($curl);
                //QUI DEVO GESTIRE ERRORI SU IP
                if(isset($response->error)) $paese= NULL;
                else $paese = strtoupper($response->countryCode);
            }
            //mappo le eccezioni
           
            if($paese=='SM') $paese ='IT';
            if($paese=='UA') $paese ='IT';
            if($paese=='IN') $paese ='FR';
            Yii::$app->session->set('paese', $paese);
            
        }
        if(stripos(Yii::$app->request->referrer,"my.kippy.eu")!==FALSE) Yii::$app->session->set('backToWebApp','http://my.kippy.eu');
        if(stripos(Yii::$app->request->referrer,"vita.kippy.eu")!==FALSE) Yii::$app->session->set('backToWebApp','http://vita.kippy.eu');
        
        if($lang = Yii::$app->session->get('lang')) Yii::$app->language=$lang; 
        
        //se sono loggato costruisco il menu Pets 
        if(!Yii::$app->user->isGuest){
            
            //chiamata in api e recupero l'array dei pet
            $auth= Yii::$app->session->get('auth');           
            $this->app_code= $auth['app_code'];
            $this->app_verification_code= $auth['app_verification_code'];
            
            $pets =$this->ApiGetKippyList($this->app_code,  $this->app_verification_code,Yii::$app->user->identity->username);
            
            if($pets->return !==0):
                Yii::$app->session->setFlash('error', DecodeError::getMessage($pets->return));
                
            else:
                //se non ci sono animali messaggio a video
                if(count($pets->kippypet_list)>0):
                    //memorizzo il risultato e lo passo al main layout per costruire il menu
                    $sort= new SortMultiArray();//riordino l'array
                    $sort->sort_order='desc';
                    $sort->sort_key='serial_number';

                    self::$menuPets=$sort->sortalo($pets->kippypet_list);

                else:
                    Yii::$app->session->setFlash('error', Yii::t('app','Non c’è associato nessun Kippy a questo account.'));

                endif;
            endif;
        }
         return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    public function actionDevice()
    {
        $pets = self::$menuPets;
        return $this->render('device',[
            'pets'=>$pets,   
        ]);  
    }

    /**
     * Displays homepage.
     *
     *
     * @return string
     */
    public function actionIndex($kippyId='')
    {

        //se non ci sono animali abbinati redirigo a pagina errore
        if(count(self::$menuPets)==0) return $this->render('error_1');
        //se non ci sono più animali abbinati redirigo a device list
        if(count(self::$menuPets)>1 && $kippyId=='') return $this->redirect(Yii::$app->homeUrl.'site/device');
        
        //rimuovo eventuali carrelli in pending
        if($cart = Yii::$app->session->get('cart')) Yii::$app->session->remove('cart');
        if($IdOrdine = Yii::$app->session->get('IdOrdine')) Yii::$app->session->remove('IdOrdine');
                    
        //mostro di default il primo kippy del menu
        if($kippyId==''):
            $serial_number= self::$menuPets[0]->serial_number;
        else:
            $serial_number = $kippyId;
        endif;
         
        //recupero il nome giusto
        foreach (self::$menuPets as $pet):
            if($pet->serial_number == $serial_number):
                $petName = $pet->kippy_name;
                $kippy = $pet;
            endif;
        endforeach;

        //verifico se è vita o finder e richiamo i pacchetti corretti
        $packs = [];
        $isVita = ($kippy->kippy_type == 'vita')? true : false;
         
        //$isVita=true;    //per testare forzo il tipo                           
        //$kippy->listino = 'LICENZA_ZERO'; //per testare forzo il listino
        
        switch ($kippy->listino){
            case 'FINDER_OLD':
                $categoria = 'PacchettiOld';
                break;    
            case 'VITA_OLD':
                $categoria = 'PacchettiOld';
                break;       
            case 'AMZ_ZERO':
                $categoria = 'PacchettiOld';
                break; 
            case 'PROMO_RETENTION':
                $categoria = 'PacchettiOld';
                break; 
            case 'LICENZA_ZERO':
                $categoria = 'PacchettiNew';
                break;    
            case 'VITA_NEW_ZERO':
                $categoria = 'PacchettiNew';
                break;           
            case 'AMZ_NEW_ZERO':
                $categoria = 'PacchettiNew';
                break;    
            case 'VODA_1_YEAR':
                $categoria = 'PacchettiNew';
                break;    
            case 'RUBATI':
                $categoria = 'PacchettiNew';
                break;       
            default:
                $categoria = 'PacchettiNew';
                break;    
        }
        

        $paese = Yii::$app->session->get('paese');


        $items  = $this->getPacchetti($categoria);
        foreach ($items as $item):

            $listino = $this->findListino($item->id,$paese);

//            echo '<pre>'.print_r($listino['durata_mesi'], true).'</pre>';
//
//            die();


            $packs[]=[
                'id'=>$item->id,
                'code'=>$item->codice,
                'name'=>  explode('-', $item->codice)[0],
                'description'=> ($paese=='IT')?$item->descrizione_it:$item->descrizione_en,

                'durata_mesi'=>$listino->durata_mesi,
                'currency_code'=>$listino->fk_valuta,
                'currency'=>$listino->fkValuta->simbolo,
                'price'=>$listino->prezzo_ivato


            ];
        endforeach;



        //definisco lo stato del dispositivo in base alla data di scadenza della sim (attivo|non attivo|scaduto)
        switch ($kippy->subscription_status){
            case 0:
                $status = 'ACTIVE';
                break;
            case 1: 
                $status = 'EXPIRING';
                break;
            case 2:
               $status = 'SUSPENDEND';
                break;
            case 3: 
                $now = strtotime(date('Y-m-d'));
                $end = strtotime($kippy->subscription_end);
                $status = (($now - $end) >= (60*60*24*365*20))? 'INACTIVE' : 'EXPIRED';//AGGIUNTO STATO INATTIVO PER I - 20 ANNI
                break;
                
        }
       //$status='INACTIVE';/////per testare forzo stato
        //se esiste un abbonamento lo richiamo
        $abbonamento = Abbonamenti::find()->where(['id_account'=>Yii::$app->user->id,'serial_number'=>$serial_number,'status'=>1])->one();

        //metto in sessione il kippy visualizzato
        $datiKippy= [
            'serial'=>$kippy->serial_number,
            'imei'=>$kippy->imei,
            'stato'=>$status,
            'scadenza'=>$kippy->subscription_end,
            'petName'=>$kippy->kippy_name,
            'tipo'=>$kippy->kippy_type,
            'listino'=>$kippy->listino,
        ];
        Yii::$app->session->set('thisKippy', $datiKippy);

        return $this->render('index',[
            'petName'=>$petName,
            'kippy'=>$kippy ,
            'isVita'=>$isVita,
            'status'=>$status,
            'packs'=>$packs,
            'abbonamento'=>$abbonamento
        ]);  
    }
    

    public function actionUserAccount()
    {
        //se non ci sono animali abbinati redirigo a pagina errore
        if(count(self::$menuPets)==0) return $this->render('error_1');
        
        //recupero gli abbonamenti attivi
        $abbonamenti = Abbonamenti::find()->where(['id_account'=>Yii::$app->user->id])->all();
        $pacchetti=[];
        $id_array= []; //mi serve per recuperare id abbonamenti per info fatturazione
        foreach ($abbonamenti as $abbonamento):
            $id_array[]=$abbonamento->id;
            //recupero il nome giusto
            foreach (self::$menuPets as $pet):
                if($pet->serial_number == $abbonamento->serial_number) :
                    $petName = $pet->kippy_name;
                    $kippy= $pet;
                endif;
            endforeach;
            //ribalto la data
            $data = explode('-', $kippy->subscription_end);
            $scadenza = $data[2] . '/' . $data[1] . '/' . $data [0];
            if($abbonamento->status):
            $pacchetti[]=[
                'id'=>$abbonamento->id,
                'name'=>explode('-',$abbonamento->idProdotto->codice)[0],
                'petName'=>$petName,
                'serial'=>$kippy->serial_number,
                'scadenza'=>$scadenza,  
                'pagamento'=>$abbonamento->pagamento,  
                'token'=>$abbonamento->token,  
            ];
            endif;
        endforeach;
        //recupero le info fatturazione 
        $fatture = Infofatturazione::find()->where(['id_abbonamento' =>$id_array])->orderBy(['created_at' => SORT_DESC])->limit(10)->all();
        
        
        $model = $this->getAccount(Yii::$app->user->identity->username);
        $model->scenario = 'aggiorna';
        if ($model->load(Yii::$app->request->post())&& $model->save()) {
           Yii::$app->session->setFlash('success', Yii::t('app','Dati di fatturazione aggiornati correttamente!'));

            return $this->redirect('user-account');
            
        }

        //test
        $client = new \Adyen\Client();

        $client->setUsername(Yii::$app->params['Adyen_user']);
        $client->setPassword(Yii::$app->params['Adyen_psw']);
        $client->setEnvironment(\Adyen\Environment::TEST);
        $client->setApplicationName("Adyen checkout Kippy");

        // initialize service
        $service = new \Adyen\Service\Recurring($client);
        $recurring = array('contract' => \Adyen\Contract::RECURRING);


        $request = array(
            "merchantAccount" => Yii::$app->params['Adyen_merchantAccount'],
            "shopperReference" => Yii::$app->user->identity->username,
            "recurring" => $recurring
        );

        $result = $service->listRecurringDetails($request);

        //test
        
        return $this->render('fatturazione', [
            'model'=>$model,
            'pacchetti'=>$pacchetti,
            'fatture'=>$fatture,
            'result'=>$result
        ]);
    }


    //test

    public function actionCancelServicePackage(){

        if(Yii::$app->user->isGuest){

            return $this->goHome();
        }

        else {

        Yii::$app->session->set('id_recurring', $_GET['id']);


      return $this->render('/recurring/cancel-service-package');
        }


    }

    public function actionAdyenNotification(){


        return $this->render('adyen-notification');
    }


    public function actionUnsubscribe($idKippy)
    {
        $idAccount = Yii::$app->user->id;
        if(Abbonamenti::find()->where(['serial_number'=>$idKippy,'status'=>1,'id_account'=>$idAccount])->exists()){
            $abbonamento = Abbonamenti::find()->where(['serial_number'=>$idKippy,'status'=>1,'id_account'=>$idAccount])->one();
            //check se sono entro i due giorni
            $today= strtotime(date("Y-m-d",time()));
            $scadenza = $abbonamento->scadenza;
            $diff =($scadenza-$today)/86400;
            if($diff >=2){
                $abbonamento->status=0;
                $abbonamento->disdetta = 1;
                $abbonamento->save();
                Yii::$app->session->setFlash('success', Yii::t('app','Il tuo pacchetto servizi è stato cancellato. Potrai continuare ad usufruire dei servizi fino alla loro scadenza')); 
                $nomePacchetto = explode('-', $abbonamento->idProdotto->codice)[0];
                $this->sendDisdetta($nomePacchetto);
                
            }else{
                Yii::$app->session->setFlash('error', Yii::t('app','Hai superato il termine per la disdetta del tuo pacchetto. Puoi disdire il tuo pacchetto entro 2 giorni dalla sua scadenza. Per maggiori info consulta "Termini e condizioni"'));
            }
            return $this->redirect('user-account');
        }
        Yii::$app->session->setFlash('error', Yii::t('app','Errore associazione ID/utente'));
        $this->goHome();

    }
    
    public function actionCheckoutBilling()
    {
        if($cart = Yii::$app->session->get('cart')){
            if($id_ordine = Yii::$app->session->get('IdOrdine')){
                $model = $this->findOrder($id_ordine);
                $populate = $this->getAccount(Yii::$app->user->identity->username);
            }else{
                $model = new Ordini;  
                $model->scenario = 'create';  
                $model->ip=Yii::$app->request->getUserIP();
                $populate = $this->getAccount(Yii::$app->user->identity->username);
                $model->fattura_nome = $model->nome = $populate->nome;
                $model->fattura_cognome = $model->citofono = $model->cognome = $populate->cognome;
                $model->fattura_indirizzo = $model->indirizzo = $populate->indirizzo;
                $model->fattura_cap = $model->cap = $populate->cap;
                $model->fattura_citta = $model->citta = $populate->citta;
                $model->fattura_provincia = $model->provincia = $populate->provincia;
                $model->fattura_email = $model->email = $populate->email;
                $model->telefono = $populate->telefono;
                $model->ragione_sociale = $populate->ragione_sociale;
                $model->partita_iva = $populate->partita_iva;
            }
            if ($model->load(Yii::$app->request->post())) {
                
                $model->status=1;//confermata fase uno
                $model->fattura_nome = $model->nome;
                $model->fattura_cognome = $model->citofono = $model->cognome;
                $model->fattura_indirizzo = $model->indirizzo;
                $model->fattura_cap = $model->cap;
                $model->fattura_citta = $model->citta;
                $model->fattura_provincia = $model->provincia;
                $model->fattura_email = $model->email;
                $model->fattura_nazione = $model->nazione = Yii::$app->session->get('paese');//aggiunto per ip detect
                if($model->save()){
                    //metto in session l'id ordine
                    Yii::$app->session->set('IdOrdine',$model->id);
                    
                    //aggiorno la sezione account
                    $populate->nome = $model->nome;
                    $populate->cognome = $model->cognome;
                    $populate->indirizzo = $model->indirizzo;
                    $populate->cap = $model->cap;
                    $populate->citta = $model->citta;
                    $populate->provincia = $model->provincia;
                    $populate->email = $model->email;
                    $populate->telefono = $model->telefono;
                    $populate->ragione_sociale = $model->ragione_sociale;
                    $populate->partita_iva = $model->partita_iva;
                    $populate->save(); 

                    //genero il modello riga e scrivo il pack in ordine
                    if($id_ordine){
                       $riga = RigheOrdini::findOne(['id_ordine'=>$id_ordine]); 
                    }else{
                       $riga = new RigheOrdini(); 
                    }
                    
                    //compilo i campi
                    $riga->id_prodotto=$cart['items']['pack']['id'];
                    $riga->id_ordine=$model->id;

                    $riga->codice_prodotto=$cart['items']['pack']['code'];
                    $riga->descrizione_prodotto= $cart['items']['pack']['description'];
                    $riga->sigla_valuta= $cart['items']['pack']['currency'];
                    $riga->prezzo_ivato= $cart['items']['pack']['price'];
                    $riga->quantita=1;
                    $riga->stato_riga='pending';

                    if($riga->save()){

                        return $this->redirect(['site/checkout-payment']);

                    }else{
                       Yii::$app->session->setFlash('error', Yii::t('app','Si è verificato un problema durante il salvataggio dei dati e l’ordine è stato annullato. Riprova di nuovo.'));
                       $this->goHome();
                    } 
                }else{
                    Yii::$app->session->setFlash('error', Yii::t('app','Si è verificato un problema durante il salvataggio dei dati e l’ordine è stato annullato. Riprova di nuovo.'));
                    $this->goHome(); 
                }
            }
            return $this->render('checkout-billing', [
                'model'=>$model,
                'cart'=>$cart,
                'thisKippy'=>Yii::$app->session->get('thisKippy'),
            ]);
        }else{
            $this->goHome();
        }
    }


//    Milos new CheckoutPayment
    public function  actionNEWOLDCheckoutPayment(){


        $id = Yii::$app->session->get('IdOrdine');
        $model = $this->findOrder($id);
        $totale = $this->getTotaleOrder($id);

        $cart = Yii::$app->session->get('cart');
        $currency = $cart['items']['pack']['currency_code'];
        $decimalPoints = AdyenFunction::getDecimalPoints($currency);

        $paymentAmount = $totale * pow(10, $decimalPoints);

        $sepaCountries = SepaCountryList::find()->all();

        if(!isset($model) && !isset($cart)) {

            return $this->goHome();

        }

        if(isset($_POST['adyen-encrypted-data'])) {


            //Set up a client and set the credentials to the Adyen platform.
            $client = new \Adyen\Client();
            $client->setApplicationName("Adyen checkout Kippy");
            $client->setUsername(Yii::$app->params['Adyen_user']);
            $client->setPassword(Yii::$app->params['Adyen_psw']);
            $client->setEnvironment(\Adyen\Environment::TEST);

            $service = new Payment($client);

            $json = '{
              "amount": {
                "value": "'.$paymentAmount.'",
                "currency": "'.$currency.'"
              },
              "reference": "kippy_service_'.$id.'",
              "shopperReference" : "'.Yii::$app->user->identity->username.'",
              "shopperEmail" : "'.$model->email.'",
              "shopperIP" : "'.Yii::$app->request->getUserIP().'",
              "shopperLocale": "'.AdyenFunction::getLocale(Yii::$app->language).'",
              "recurringDetailReference" : "'.Yii::$app->session->get('id_recurring').'",
              "merchantAccount": "'.Yii::$app->params['Adyen_merchantAccount'].'",
              "additionalData": {
             "card.encrypted.json": "'.$_POST["adyen-encrypted-data"].'"
              }
            }';

            $params = json_decode($json, true);


            //if user check recurring field we put recurring in json array
            if (isset($_POST['recurring']) and ($_POST['recurring']=='1')) {

                $params['recurring'] = ['contract'=>'RECURRING'];
            }

            $result = $service->authorise($params);



            if($result['resultCode']=='Refused') {

                Yii::$app->session->setFlash('error', Yii::t('app','Your card information are not correct. '));
                return $this->redirect(['site/checkout-payment']);

            }

            elseif($result['resultCode'] =='Authorised' or $result['resultCode']=='Received' ) {

                $id_ordine = Yii::$app->session->get('IdOrdine');


                //TEST
                $pagamento='credit-card';
                $paese= Yii::$app->session->get('paese');

                //scrivo ordine su csv e lo chiudo
                $this->writeCsv($id_ordine, $paese, $pagamento);

                //creo l'abbonamento (devo gestire anche update)
                //verifico se esiste un abbonamento attivo con questo imei
                $id_account = Yii::$app->user->id;
                $thisKippy = Yii::$app->session->get('thisKippy');
                $cart = Yii::$app->session->get('cart');
                $pack = $cart['items']['pack'];


                if(Abbonamenti::find()->where(['id_account'=>$id_account,'kippy_imei'=>$thisKippy['imei'],'status'=>1])->exists()):

                    //se esiste ed è attivo è un update / upgrade
                   $abbonamento = Abbonamenti::find()->where(['id_account'=>$id_account,'kippy_imei'=>$thisKippy['imei'],'status'=>1])->one();

                    //disattivo il vecchio abbonamento
                    $abbonamento->status = 0;
                   // $abbonamento->recurring  = '0';
                   // $abbonamento->recurringDetailReference='0';
                    $abbonamento->save();


                    //test
                    //SELECT * FROM `abbonamenti` WHERE serial_number='AZ2M47U'

                endif;

                //creo un nuovo abbonamento

                $new_abbonamento = new Abbonamenti();
                //calcolo la data scadenza faccio il max fra oggi e la data + i nuovi mesi
                $scadenza = strtotime($thisKippy['scadenza']);
                $oggi = strtotime(date('Y-m-d'));
                $durata = $pack['durata_mesi'] . " month";
                $scadenza_new = strtotime($durata ,max($scadenza, $oggi));

                $new_abbonamento->id_account = $id_account;
                $new_abbonamento->serial_number = $thisKippy['serial'];
                $new_abbonamento->kippy_imei = $thisKippy['imei'];
                $new_abbonamento->id_prodotto = $pack['id'];
                $new_abbonamento->durata_mesi = $pack['durata_mesi'];
                $new_abbonamento->scadenza = $scadenza_new;
                $new_abbonamento->pagamento = $pagamento;
                //recupero la riga ordine
                $Riga= RigheOrdini::findOne(['id_ordine'=>$id_ordine]);
                $new_abbonamento->sigla_valuta = $Riga->sigla_valuta;
                $new_abbonamento->importo = $Riga->prezzo_ivato;

                $new_abbonamento->token = 'no-token-needed';
                $new_abbonamento->token_scadenza_mese = 'no-token-needed';
                $new_abbonamento->token_scadenza_anno = 'no-token-needed';
                $new_abbonamento->recurring = isset($_POST['recurring']) ? $_POST['recurring'] : '0';

                $new_abbonamento->recurringDetailReference 	= 'test';

               // $new_abbonamento->recurringDetailReference 	= Yii::$app->session->get('id_recurring');

                $new_abbonamento->status = 1;

                $new_abbonamento->save();

                //creo nuova riga su infofatturazione
                $Info = new Infofatturazione();
                $Info->id_abbonamento = $new_abbonamento->id;
                $Info->id_ordine = $id_ordine;
                $Info->start_period = max($scadenza, $oggi);
                $Info->end_period = $scadenza_new;
                $Info->save();






                //aggiorno la scadenza della sim su akerue
                $subscription_end = date('Y-m-d', $scadenza_new);
                $response = $this->ApiSetKippySubscription($this->app_code, $this->app_verification_code, $thisKippy['imei'], $subscription_end);
                if($response->return !==0):
                    Yii::$app->session->setFlash('error',Yii::t('app','Il pagamento è andato a buon fine, tuttavia, si è verificato un problema tecnico nell’aggiornamento del servizio, contatta il customer care e fornisci il seguente numero di ordine come riferimento : ') .$id_ordine);
                    $this->goHome();
                endif;





                //test

                //resetto la sessione cart e IdOrdine
                Yii::$app->session->remove('cart');
                Yii::$app->session->remove('IdOrdine');
                Yii::$app->session->remove('ResultOrdine');

                //invio la mail di conferma ordine
                //$this->sendOrderConfirm($id_ordine);
                return $this->redirect(array('site/transaction-success', 'id_ordine'=>$id_ordine));

            }

        }

        if(isset($_POST['ownerName']) && $_POST['iban'] && $_POST['countryCode'] )  {

            //Set up a client and set the credentials to the Adyen platform.
            $client = new \Adyen\Client();
            $client->setApplicationName("Adyen checkout Kippy");
            $client->setUsername(Yii::$app->params['Adyen_user']);
            $client->setPassword(Yii::$app->params['Adyen_psw']);
            $client->setEnvironment(\Adyen\Environment::TEST);

            // initialize service
            $service = new Payment($client);

            $ownerName = $_POST['ownerName'];
            $iban = $_POST['iban'];
            $country = $_POST['countryCode'];

            $json = '{

                    "bankAccount" : {
                        "iban" : "'.$iban.'",
                        "ownerName" : "'.$ownerName.'",
                        "countryCode" : "'.$country.'"          
                    },
                     
                    "shopperReference" : "'.Yii::$app->user->identity->username.'",
                    "recurringDetailReference" : "'.Yii::$app->session->get('id_recurring').'",
                   
                    "amount" : {
                        "value": "'.$paymentAmount.'",
                        "currency": "'.$currency.'"
                    },
                    },
                     
                    "reference": "kippy_service_'.$id.'",
                    "merchantAccount": "'.Yii::$app->params['Adyen_merchantAccount'].'",
                    "shopperEmail" : "'.$model->email.'",
                    "shopperIP" : "'.Yii::$app->request->getUserIP().'",
                    "shopperStatement" : "YOUR ORDER 122345677889",
                    "selectedBrand" : "sepadirectdebit"
                    
                }';


            $params = json_decode($json, true);

//            echo '<pre>'.print_r($params, true).'</pre>';
//
//            die();

            //if user check recurring field we put recurring in json array
            if (isset($_POST['recurring']) and ($_POST['recurring']=='1')) {

                $params['recurring'] = ['contract'=>'RECURRING'];

            }

            $result = $service->authorise($params);

            if($result['resultCode'] =='Received' or $result['resultCode']=='Authorised' ) {


                $id_ordine = Yii::$app->session->get('IdOrdine');

                //TEST
                $pagamento='bancasella';
                $paese= Yii::$app->session->get('paese');

                //scrivo ordine su csv e lo chiudo
                $this->writeCsv($id_ordine, $paese, $pagamento);

                //creo l'abbonamento (devo gestire anche update)
                //verifico se esiste un abbonamento attivo con questo imei
                $id_account = Yii::$app->user->id;
                $thisKippy = Yii::$app->session->get('thisKippy');
                $cart = Yii::$app->session->get('cart');
                $pack = $cart['items']['pack'];


                if(Abbonamenti::find()->where(['id_account'=>$id_account,'kippy_imei'=>$thisKippy['imei'],'status'=>1])->exists()):

                    //se esiste ed è attivo è un update / upgrade
                    $abbonamento = Abbonamenti::find()->where(['id_account'=>$id_account,'kippy_imei'=>$thisKippy['imei'],'status'=>1])->one();

                    //disattivo il vecchio abbonamento
                    $abbonamento->status = 0;
//                    $abbonamento->recurring  = '0';
//                    $abbonamento->recurringDetailReference='0';
                    $abbonamento->save();

                endif;


                //creo un nuovo abbonamento

                $new_abbonamento = new Abbonamenti();
                //calcolo la data scadenza faccio il max fra oggi e la data + i nuovi mesi
                $scadenza = strtotime($thisKippy['scadenza']);
                $oggi = strtotime(date('Y-m-d'));
                $durata = $pack['durata_mesi'] . " month";
                $scadenza_new = strtotime($durata ,max($scadenza, $oggi));

                $new_abbonamento->id_account = $id_account;
                $new_abbonamento->serial_number = $thisKippy['serial'];
                $new_abbonamento->kippy_imei = $thisKippy['imei'];
                $new_abbonamento->id_prodotto = $pack['id'];
                $new_abbonamento->durata_mesi = $pack['durata_mesi'];
                $new_abbonamento->scadenza = $scadenza_new;
                $new_abbonamento->pagamento = $pagamento;
                //recupero la riga ordine
                $Riga= RigheOrdini::findOne(['id_ordine'=>$id_ordine]);
                $new_abbonamento->sigla_valuta = $Riga->sigla_valuta;
                $new_abbonamento->importo = $Riga->prezzo_ivato;

                $new_abbonamento->token = 'no-token-needed';
                $new_abbonamento->token_scadenza_mese = 'no-token-needed';
                $new_abbonamento->token_scadenza_anno = 'no-token-needed';
                $new_abbonamento->recurring = $new_abbonamento->recurring = isset($_POST['recurring']) ? $_POST['recurring'] : '0';
                $new_abbonamento->recurringDetailReference 	= Yii::$app->session->get('id_recurring');

                $new_abbonamento->status = 1;

//                $new_abbonamento->disdetta  = 0;
//                $new_abbonamento->sospeso   = 0;
//                $new_abbonamento->rinnovi_falliti    = 0;


                $new_abbonamento->save();



                //aggiorno la scadenza della sim su akerue
                $subscription_end = date('Y-m-d', $scadenza_new);
                $response = $this->ApiSetKippySubscription($this->app_code, $this->app_verification_code, $thisKippy['imei'], $subscription_end);
                if($response->return !==0):
                    Yii::$app->session->setFlash('error',Yii::t('app','Il pagamento è andato a buon fine, tuttavia, si è verificato un problema tecnico nell’aggiornamento del servizio, contatta il customer care e fornisci il seguente numero di ordine come riferimento : ') .$id_ordine);
                    $this->goHome();
                endif;


//                echo '<pre>'.print_r($new_abbonamento, true).'</pre>';
//
//                die();



                //test

                //resetto la sessione cart e IdOrdine
                Yii::$app->session->remove('cart');
                Yii::$app->session->remove('IdOrdine');
                Yii::$app->session->remove('ResultOrdine');

                //invio la mail di conferma ordine
                //$this->sendOrderConfirm($id_ordine);

                return $this->redirect(array('site/transaction-success', 'id_ordine'=>$id_ordine));

            }

        }

            return $this->render('checkout-payment-milos', [
                'model'=>$model,
                'cart'=>$cart,
                'thisKippy'=>Yii::$app->session->get('thisKippy'),
                'sepaCountries'=>$sepaCountries,
            ]);


    }


    public function actionDisableRecurring(){


        if(Yii::$app->user->isGuest){

            return $this->goHome();
        }


        if(isset($_POST['cancel-the-service'])) {

            $reason = isset($_POST['reason']) ? $_POST['reason'] : '';
            $user_message = isset($_POST['message']) ? $_POST['message'] : '';
            $nome = isset(Yii::$app->user->identity->nome) ? Yii::$app->user->identity->nome : '';
            $cognome = isset(Yii::$app->user->identity->cognome) ? Yii::$app->user->identity->cognome : '';
            $username = isset(Yii::$app->user->identity->username) ? Yii::$app->user->identity->username : '';
            $indirizzo = isset(Yii::$app->user->identity->indirizzo) ? Yii::$app->user->identity->indirizzo : '';
            $cap =isset(Yii::$app->user->identity->cap) ? Yii::$app->user->identity->cap : '';
            $provincia =isset(Yii::$app->user->identity->provincia) ? Yii::$app->user->identity->provincia : '';
            $nazione =isset(Yii::$app->user->identity->nazione) ? Yii::$app->user->identity->nazione : '';
            $telefono = isset(Yii::$app->user->identity->telefono) ? Yii::$app->user->identity->telefono : '';
            $account_created=isset(Yii::$app->user->identity->created_at)? Yii::$app->user->identity->created_at : '';


            Yii::$app->mailer->view->params['reason'] = $reason;
            Yii::$app->mailer->view->params['user_message'] = $user_message;
            Yii::$app->mailer->view->params['user_acc_email']=Yii::$app->user->identity->email;
            Yii::$app->mailer->view->params['username']=Yii::$app->user->identity->username;



            Yii::$app->mailer->compose([
                'html' => 'disable-recurring',

            ])
                ->setFrom('service@kippy.eu')
                ->setTo('miloslukicbk1@gmail.com')
                ->setSubject('User cancel the service package')
                ->send();


            switch (Yii::$app->language){
                case 'it-it':
                    $sub = 'Il tuo pacchetto di servizi è stato annullato';
                    break;
                case 'en-gb':
                    $sub = 'Your Kippy service pack has been canceled';
                    break;
                case 'fr-fr':
                    $sub = 'Votre forfait de service a été annulé';
                    break;
                case 'de-de':
                    $sub = 'Dein Service-paket Kippy wurde storniert';
                    break;
                case 'es-es':
                    $sub = 'Tu paquete de servicios Kippy ha sido cancelado';
                    break;
                default:
                    $sub = 'Your Kippy service pack has been canceled';// in inglese
                    break;
            }
            $language = substr(Yii::$app->language, 0,2);


     //       Yii::$app->mailer->view->params['image_tank'] =  'http://localhost:8080/service-kippy-3/web/images/mail/dark-youtube-48.png';


//            Yii::$app->mailer->view->params['user_message'] = $user_message;
//            Yii::$app->mailer->view->params['user_acc_email']=Yii::$app->user->identity->email;
//            Yii::$app->mailer->view->params['username']=Yii::$app->user->identity->username;


            Yii::$app->mailer->compose([
                'html'=>'disdetta_pacchetto_'.$language,
            ])


                ->setFrom('orders@kippy.eu')
                //production mail
                //    ->setTo(Yii::$app->user->identity->email)
                //test mail
                ->setTo('miloslukicbk1@gmail.com')
                ->setSubject($sub)
                ->send();

            //test 5.3

            $id_account = Yii::$app->user->id;
            $thisKippy = Yii::$app->session->get('thisKippy');


//            echo '<pre>'.print_r('TEST', true).'</pre>';
//
//            die();


            if(Abbonamenti::find()->where(['id_account'=>$id_account,'kippy_imei'=>$thisKippy['imei'], 'recurring'=>1])->exists()):

                //se esiste ed è attivo è un update / upgrade
                $abbonamento = Abbonamenti::find()->where(['id_account'=>$id_account,'kippy_imei'=>$thisKippy['imei'], 'recurring'=>1])->one();

                //disattivo il vecchio abbonamento
              //  $abbonamento->status = 1;
                $abbonamento->recurring = 0;
              //  $abbonamento->recurringDetailReference='0';
                $abbonamento->save();




            endif;

            //test 5.3


            $client = new \Adyen\Client();
            $client->setUsername(Yii::$app->params['Adyen_user']);
            $client->setPassword(Yii::$app->params['Adyen_psw']);
            $client->setEnvironment(\Adyen\Environment::TEST);
            $client->setApplicationName("Adyen checkout Kippy");


            $service = new \Adyen\Service\Recurring($client);


            $request = array(
                "merchantAccount" => Yii::$app->params['Adyen_merchantAccount'],
                "shopperReference" => $username,
                "recurringDetailReference" => Yii::$app->session->get('id_recurring'),

            );


            $result = $service->disable($request);


            //enter in database users that cancel subscription
            $new_subscription_cancel = new SubscriptionCancel();
            $new_subscription_cancel->username =  $username;
            $new_subscription_cancel->nome =  $nome;
            $new_subscription_cancel->cognome =  $cognome;
            $new_subscription_cancel->email =  Yii::$app->user->identity->email;
            $new_subscription_cancel->messaggio =  $user_message;
            $new_subscription_cancel->ragionare =  $reason;
            $new_subscription_cancel->indirizzo =  $indirizzo;
            $new_subscription_cancel->cap =  $cap;
            $new_subscription_cancel->provincia =  $provincia;
            $new_subscription_cancel->nazione =  $nazione;
            $new_subscription_cancel->telefono =  $telefono;
            $new_subscription_cancel->recurringAdyenReference =  Yii::$app->session->get('id_recurring');

            $new_subscription_cancel->account_creato =  $account_created;

            $new_subscription_cancel->save();


            Yii::$app->session->setFlash('success', Yii::t('app', 'You disabled recurring!'));

            return $this->redirect('user-account');


        }

        return $this->render('/recurring/disable-recurring');
    }



    public function actionTransactionSuccess(){

        //accedo alla thankyou solo se l'ordine ha avuto successo

        //recupero id ordine dalla sessione
        $id_ordine = Yii::$app->session->get('IdOrdine');
        //resetto la sessione cart e IdOrdine
        Yii::$app->session->remove('cart');
        Yii::$app->session->remove('IdOrdine');
        Yii::$app->session->remove('ResultOrdine');

        //invio la mail di conferma ordine
        //$this->sendOrderConfirm($id_ordine);

        return $this->render('success', [
            'id_ordine'=>$id_ordine,
        ]);

    }



//    TEST TEST TEST


    public function actionCheckRecurring($shopperReference = 26088){
        //Set up a client and set the credentials to the Adyen platform.
        $client = new \Adyen\Client();
        $client->setApplicationName("Adyen checkout Kippy");
        $client->setUsername(Yii::$app->params['Adyen_user']);
        $client->setPassword(Yii::$app->params['Adyen_psw']);
        $client->setEnvironment(\Adyen\Environment::TEST);

        // initialize service
        $service = new \Adyen\Service\Recurring($client);

        $recurring = array('contract' => \Adyen\Contract::RECURRING);
        $request = array(
            "merchantAccount" => "KippyCOM",
            "shopperReference" => $shopperReference,
            "recurring" => $recurring
        );
        $result = $service->listRecurringDetails($request);

        echo '<pre>';
        print_r($result);
    }


    public function actionCheckoutPayment(){


//         phpinfo();
//
//         die();

        //NOT WORKING

        //parametri necessari
        $id = Yii::$app->session->get('IdOrdine');

        $model = $this->findOrder($id);
        $totale = $this->getTotaleOrder($id);

        $lang_suffix=substr(Yii::$app->language, 0,2);

        $cart = Yii::$app->session->get('cart');

        $currency = $cart['items']['pack']['currency_code'];

        //array credit card abilitate (brandCode) mi serve per fare comparire le icone delle carte localizzate
        $array_credit_card_local_payment= ['diners','discover','amex','mc','visa','maestro'];

        //array altri metodi abilitati (brandCode) mi serve per ciclare i pagamenti che non sono credit card sempre localizzati
        $array_other_local_payment= ['sepadirectdebit','paypal'];
        //calcolo il totale in minim units
        $decimalPoints = AdyenFunction::getDecimalPoints($currency);

        $paymentAmount = $totale * pow(10, $decimalPoints);

        $paese= Yii::$app->session->get('paese');

        //Set up a client and set the credentials to the Adyen platform.
        $client = new \Adyen\Client();
        $client->setApplicationName("Adyen checkout Kippy");
        $client->setUsername(Yii::$app->params['Adyen_user']);
        $client->setPassword(Yii::$app->params['Adyen_psw']);
        $client->setEnvironment(\Adyen\Environment::TEST);

        // initialize service
        $service = new \Adyen\Service\DirectoryLookup($client);

//        echo '<pre>'.print_r($service, true).'</pre>';
//
//        die();

        $params['paymentAmount']= $paymentAmount;
        $params['currencyCode']= $currency;
        $params['merchantReference']= 'kippy_service_'.$id;
        $params['skinCode']= Yii::$app->params['Adyen_skinCode'];
        $params['merchantAccount']= Yii::$app->params['Adyen_merchantAccount'];
        $params['sessionValidity']= date('c', time()+60*60);//scade entro un ora;
        $params['countryCode']= $paese;
        $params['recurringContract'] = 'RECURRING';
        $params['shopperReference'] = Yii::$app->user->identity->username;
        $params['shopperEmail'] = $model->email;
        $params['shopperIP'] = Yii::$app->request->getUserIP();
        $params['shopperLocale']= AdyenFunction::getLocale(Yii::$app->language);
        $params['resURL']= Yii::$app->params['Adyen_local_return_url'];
        $params_form_locale = $params;

        // add signature in request
        $hmacKey = Yii::$app->params['Adyen_HMAC_KEY'];
        $params["merchantSig"] = \Adyen\Util\Util::calculateSha256Signature($hmacKey, $params);


       // $params = json_encode($params, true);

        $result = $service->directoryLookup($params);


//        echo '<pre>'.print_r($params, true).'</pre>';
//
//        die();



        $paymentMethods = $result['paymentMethods'];
        //echo '<pre>'; print_r($paymentMethods); exit;

        return $this->render('checkout-payment', [
            'model'=>$model,
            'cart'=>Yii::$app->session->get('cart'),
            'thisKippy'=>Yii::$app->session->get('thisKippy'),
            'paymentMethods'=>$paymentMethods,
            'params_form_locale'=>$params_form_locale,
            'credit_card_local_payment'=>$array_credit_card_local_payment,
            'other_local_payment'=>$array_other_local_payment,
        ]);



    }


    public function actionGetMerchantSig(){
        $querystring = $_POST['params'];
        parse_str($querystring, $params);
        unset($params['merchantSig']);//lo rimuovo non va messo nel calcolo firma
        // calcolo signature per il form
        $hmacKey = Yii::$app->params['Adyen_HMAC_KEY'];
        $merchantSig = \Adyen\Util\Util::calculateSha256Signature($hmacKey, $params);
        echo $merchantSig;
    }
    public function actionAdyenReturn(){
        echo '<pre>' ;print_r($_GET);
        $hmacKey = Yii::$app->params['Adyen_HMAC_KEY'];
        $merchantSig = $_GET['merchantSig'];
        unset($_GET['merchantSig']);
        $merchantSigCheck = \Adyen\Util\Util::calculateSha256Signature($hmacKey, $_GET);
        echo $merchantSig.'<br>';
        //valido la firma
        if($merchantSig===$merchantSigCheck):
            echo 'Firma Valida';
        else:
            echo 'Firma Corrotta';
        endif;

        /*
         * (
                [authResult] => AUTHORISED
                [merchantReference] => kippy_service_6972
                [merchantSig] => a6GNI7GKWiyIMSp9qkaWgx2E4pPyLr8JR/ZKRC1HZG8=
                [paymentMethod] => discover
                [pspReference] => 8825035885101405
                [shopperLocale] => it
                [skinCode] => gBkAo1Xt
            )
         * [authResult] => AUTHORISED , REFUSED , PENDING , CANCELLED(quando torna al sito da paypal)
         */

        exit();

    }


    public function actionPaypalPay(){

//https://developer.paypal.com/docs/classic/express-checkout/ec_set_up_reference_transactions/?mark=reference%20transaction
        $cart = Yii::$app->session->get('cart');
        $paymentAmount = $cart['items']['pack']['price'];
        $billingDescription = 'Order number ' . Yii::$app->session->get('IdOrdine') . ' - ' .  $cart['items']['pack']['description'] . ' ' . $cart['items']['pack']['currency'] . ' ' . $cart['items']['pack']['price'];
        $currencyCodeType = $cart['items']['pack']['currency_code'];

        $uri = Yii::$app->params['PayPal_API_Endpoint'];

        if(!isset($cart)) {

            return $this->goHome();

        }

        $response = Request::post($uri)
            ->body(
                "USER=" . Yii::$app->params['PayPal_API_UserName']
                . "&PWD=" . Yii::$app->params['PayPal_API_Password']
                . "&SIGNATURE=" . Yii::$app->params['PayPal_API_Signature']
                . "&METHOD=SetExpressCheckout"
                . "&VERSION=86"
                . "&PAYMENTREQUEST_0_PAYMENTACTION=AUTHORIZATION"
                . "&PAYMENTREQUEST_0_AMT=$paymentAmount"
                . "&PAYMENTREQUEST_0_CURRENCYCODE=$currencyCodeType"
                . "&L_BILLINGTYPE0=MerchantInitiatedBilling"
                . "&L_BILLINGAGREEMENTDESCRIPTION0=$billingDescription"
                . "&cancelUrl=" . Yii::$app->params['PayPal_cancelURL']
                . "&returnUrl=" . Yii::$app->params['PayPal_returnURL']
            )
            ->send();
        echo '<pre>';
        $resArray = ($this->deformatNVP($response->body));
        $ack = strtoupper($resArray['ACK']);
        if($ack=='SUCCESS' || $ack=='SUCCESSWITHWARNING'){
            $token = $resArray['TOKEN'];
            // Redirect to paypal.com here
            $payPalURL = Yii::$app->params['PAYPAL_URL'] . $token;
            header("Location: ".$payPalURL);
            exit;
        }else{
            //Display a user friendly Error on the page using any of the following error information returned by PayPal
//            $ErrorCode = urldecode($resArray["L_ERRORCODE0"]);
//            $ErrorShortMsg = urldecode($resArray["L_SHORTMESSAGE0"]);
//            $ErrorLongMsg = urldecode($resArray["L_LONGMESSAGE0"]);
//            $ErrorSeverityCode = urldecode($resArray["L_SEVERITYCODE0"]);
//
//            echo "SetExpressCheckout API call failed. ";
//            echo "Detailed Error Message: " . $ErrorLongMsg;
//            echo "Short Error Message: " . $ErrorShortMsg;
//            echo "Error Code: " . $ErrorCode;
//            echo "Error Severity Code: " . $ErrorSeverityCode;
            Yii::$app->session->setFlash('error',Yii::t('shop','Si è verificato un problema di connessione a PayPal, si prega di riprovare.'));
            return $this->redirect(['site/checkout-payment']);
        }


        return $this->render('paypal-payment-milos');

    }


    public function actionPaypal(){
        //https://developer.paypal.com/docs/classic/express-checkout/ec_set_up_reference_transactions/?mark=reference%20transaction
        $cart = Yii::$app->session->get('cart');
        $paymentAmount = $cart['items']['pack']['price'];
        $billingDescription = 'Order number ' . Yii::$app->session->get('IdOrdine') . ' - ' .  $cart['items']['pack']['description'] . ' ' . $cart['items']['pack']['currency'] . ' ' . $cart['items']['pack']['price'];
        $currencyCodeType = $cart['items']['pack']['currency_code'];

        $uri = Yii::$app->params['PayPal_API_Endpoint'];


        $response = Request::post($uri)
                    ->body(
                          "USER=" . Yii::$app->params['PayPal_API_UserName']
                        . "&PWD=" . Yii::$app->params['PayPal_API_Password']
                        . "&SIGNATURE=" . Yii::$app->params['PayPal_API_Signature']
                        . "&METHOD=SetExpressCheckout"
                        . "&VERSION=86"
                        . "&PAYMENTREQUEST_0_PAYMENTACTION=AUTHORIZATION"
                        . "&PAYMENTREQUEST_0_AMT=$paymentAmount"
                        . "&PAYMENTREQUEST_0_CURRENCYCODE=$currencyCodeType"
                        . "&L_BILLINGTYPE0=MerchantInitiatedBilling"
                        . "&L_BILLINGAGREEMENTDESCRIPTION0=$billingDescription"
                        . "&cancelUrl=" . Yii::$app->params['PayPal_cancelURL']
                        . "&returnUrl=" . Yii::$app->params['PayPal_returnURL']
                        )
                        ->send();
        echo '<pre>';
        $resArray = ($this->deformatNVP($response->body));
        $ack = strtoupper($resArray['ACK']);
        if($ack=='SUCCESS' || $ack=='SUCCESSWITHWARNING'){
            $token = $resArray['TOKEN'];
            // Redirect to paypal.com here
            $payPalURL = Yii::$app->params['PAYPAL_URL'] . $token;
            header("Location: ".$payPalURL);
            exit;
        }else{
            //Display a user friendly Error on the page using any of the following error information returned by PayPal
//            $ErrorCode = urldecode($resArray["L_ERRORCODE0"]);
//            $ErrorShortMsg = urldecode($resArray["L_SHORTMESSAGE0"]);
//            $ErrorLongMsg = urldecode($resArray["L_LONGMESSAGE0"]);
//            $ErrorSeverityCode = urldecode($resArray["L_SEVERITYCODE0"]);
//
//            echo "SetExpressCheckout API call failed. ";
//            echo "Detailed Error Message: " . $ErrorLongMsg;
//            echo "Short Error Message: " . $ErrorShortMsg;
//            echo "Error Code: " . $ErrorCode;
//            echo "Error Severity Code: " . $ErrorSeverityCode;
            Yii::$app->session->setFlash('error',Yii::t('shop','Si è verificato un problema di connessione a PayPal, si prega di riprovare.'));
            return $this->redirect(['site/checkout-payment']);
        }

    }
    public function actionPaypalReturn($token,$PayerID){

       $token = urldecode($token);

       $uri = Yii::$app->params['PayPal_API_Endpoint'];


        $response = Request::post($uri)
                    ->body(
                          "USER=" . Yii::$app->params['PayPal_API_UserName']
                        . "&PWD=" . Yii::$app->params['PayPal_API_Password']
                        . "&SIGNATURE=" . Yii::$app->params['PayPal_API_Signature']
                        . "&METHOD=CreateBillingAgreement"
                        . "&VERSION=86"
                        . "&TOKEN=$token"
                        )
                        ->send();



//        echo '<pre>'.print_r('TEST', true).'</pre>';
//
//        die();

        $resArray = ($this->deformatNVP($response->body));
        $ack = strtoupper($resArray['ACK']);
        if($ack=='SUCCESS' || $ack=='SUCCESSWITHWARNING'){
//            echo '<pre>';
//            print_r($resArray);



            //catturo il token per i ricorrenti
            $recurringToken = urldecode($resArray['BILLINGAGREEMENTID']);


            //concludo il pagamento
            $cart = Yii::$app->session->get('cart');


            $paymentAmount = $cart['items']['pack']['price'];

            $currencyCodeType = $cart['items']['pack']['currency_code'];
            $paynow = Request::post($uri)
                    ->body(
                          "USER=" . Yii::$app->params['PayPal_API_UserName']
                        . "&PWD=" . Yii::$app->params['PayPal_API_Password']
                        . "&SIGNATURE=" . Yii::$app->params['PayPal_API_Signature']
                        . "&METHOD=DoExpressCheckoutPayment"
                        . "&VERSION=86"
                        . "&TOKEN=$token"
                        . "&PAYMENTREQUEST_0_PAYMENTACTION=SALE"
                        . "&PAYERID=$PayerID"
                        . "&PAYMENTREQUEST_0_AMT=$paymentAmount"
                        . "&PAYMENTREQUEST_0_CURRENCYCODE=$currencyCodeType"
                        )
                        ->send();




            $payArray = ($this->deformatNVP($paynow->body));


            $ackPay = strtoupper($payArray['ACK']);

            //se va a buon fine la chiamata leggo il risultato ed eventuali errori
            if($ackPay=='SUCCESS' || $ackPay=='SUCCESSWITHWARNING'){

                //recupero i campi per terashop
        $transactionId		= $payArray["PAYMENTINFO_0_TRANSACTIONID"]; // ' Unique transaction ID of the payment. Note:  If the PaymentAction of the request was Authorization or Order, this value is your AuthorizationID for use with the Authorization & Capture APIs.
		$transactionType 	= $payArray["PAYMENTINFO_0_TRANSACTIONTYPE"]; //' The type of transaction Possible values: l  cart l  express-checkout
		$paymentType		= $payArray["PAYMENTINFO_0_PAYMENTTYPE"];  //' Indicates whether the payment is instant or delayed. Possible values: l  none l  echeck l  instant
		$orderTime 		= $payArray["PAYMENTINFO_0_ORDERTIME"];  //' Time/date stamp of payment
		$amt			= $payArray["PAYMENTINFO_0_AMT"];  //' The final amount charged, including any shipping and taxes from your Merchant Profile.
		$currencyCode		= $payArray["PAYMENTINFO_0_CURRENCYCODE"];  //' A three-character currency code for one of the currencies listed in PayPay-Supported Transactional Currencies. Default: USD.
                /*
		' Status of the payment:
				'Completed: The payment has been completed, and the funds have been added successfully to your account balance.
				'Pending: The payment is pending. See the PendingReason element for more information.
		*/

                $paymentStatus	= strtoupper($payArray["PAYMENTINFO_0_PAYMENTSTATUS"]);

                $errorCode      = $payArray["PAYMENTINFO_0_ERRORCODE"];

                /*
		'The reason the payment is pending:
		'  none: No pending reason
		'  address: The payment is pending because your customer did not include a confirmed shipping address and your Payment Receiving Preferences is set such that you want to manually accept or deny each of these payments. To change your preference, go to the Preferences section of your Profile.
		'  echeck: The payment is pending because it was made by an eCheck that has not yet cleared.
		'  intl: The payment is pending because you hold a non-U.S. account and do not have a withdrawal mechanism. You must manually accept or deny this payment from your Account Overview.
		'  multi-currency: You do not have a balance in the currency sent, and you do not have your Payment Receiving Preferences set to automatically convert and accept this payment. You must manually accept or deny this payment.
		'  verify: The payment is pending because you are not yet verified. You must verify your account before you can accept this payment.
		'  other: The payment is pending for a reason other than those listed above. For more information, contact PayPal customer service.
		*/

		$pendingReason	= $payArray["PAYMENTINFO_0_PENDINGREASON"];

                //salvo nel db i campi per Terashop
                $id_ordine= Yii::$app->session->get('IdOrdine');
                $order= $this->findOrder($id_ordine);
                $order->CC_Autorizzazione = $transactionId;
                $order->CC_Addebito = $amt;;
                $order->CC_Transazione = $transactionId;
                $order->CC_Data_Addebito = $orderTime;
                $order->CC_Id_Errore = $errorCode;
                $order->CC_Descrizione_Errore = $pendingReason;
                $order->save();

                //verifico se il pagamento è andato a buon fine
                if($paymentStatus=='COMPLETED'){
                    $pagamento='paypal';
                    $paese= Yii::$app->session->get('paese');

                    //scrivo ordine su csv e lo chiudo
                    $this->writeCsv($id_ordine, $paese, $pagamento);

                    //creo l'abbonamento (devo gestire anche update)
                    //verifico se esiste un abbonamento attivo con questo imei
                    $id_account = Yii::$app->user->id;
                    $thisKippy = Yii::$app->session->get('thisKippy');
                    $cart = Yii::$app->session->get('cart');
                    $pack = $cart['items']['pack'];
                    $token = $recurringToken;
                    $token_scadenza_mese = '';
                    $token_scadenza_anno = '';
                    if(Abbonamenti::find()->where(['id_account'=>$id_account,'kippy_imei'=>$thisKippy['imei'],'status'=>1])->exists()):
                        //se esiste ed è attivo è un update / upgrade
                        $abbonamento = Abbonamenti::find()->where(['id_account'=>$id_account,'kippy_imei'=>$thisKippy['imei'],'status'=>1])->one();
                        //disattivo il vecchio abbonamento
                        $abbonamento->status = 0;
                        $abbonamento->save();
                    endif;
                    //creo un nuovo abbonamento
                    $new_abbonamento = new Abbonamenti();
                     //calcolo la data scadenza faccio il max fra oggi e la data + i nuovi mesi
                    $scadenza = strtotime($thisKippy['scadenza']);
                    $oggi = strtotime(date('Y-m-d'));
                    $durata = $pack['durata_mesi'] . " month";
                    $scadenza_new = strtotime($durata ,max($scadenza, $oggi));

                    $new_abbonamento->id_account = $id_account;
                    $new_abbonamento->serial_number = $thisKippy['serial'];
                    $new_abbonamento->kippy_imei = $thisKippy['imei'];
                    $new_abbonamento->id_prodotto = $pack['id'];
                    $new_abbonamento->durata_mesi = $pack['durata_mesi'];
                    $new_abbonamento->scadenza = $scadenza_new;
                    $new_abbonamento->pagamento = $pagamento;
                    //recupero la riga ordine
                    $Riga= RigheOrdini::findOne(['id_ordine'=>$id_ordine]);
                    $new_abbonamento->sigla_valuta = $Riga->sigla_valuta;
                    $new_abbonamento->importo = $Riga->prezzo_ivato;
                    $new_abbonamento->token = $token;
                    $new_abbonamento->token_scadenza_mese = $token_scadenza_mese;
                    $new_abbonamento->token_scadenza_anno = $token_scadenza_anno;
                    $new_abbonamento->status = 1;
                    $new_abbonamento->save();
                    //creo nuova riga su infofatturazione
                    $Info = new Infofatturazione();
                    $Info->id_abbonamento = $new_abbonamento->id;
                    $Info->id_ordine = $id_ordine;
                    $Info->start_period = max($scadenza, $oggi);
                    $Info->end_period = $scadenza_new;
                    $Info->save();
                    //aggiorno la scadenza della sim su akerue
                    $subscription_end = date('Y-m-d', $scadenza_new);

                    $response = $this->ApiSetKippySubscription($this->app_code, $this->app_verification_code, $thisKippy['imei'], $subscription_end);
                    if($response->return !==0):
                        Yii::$app->session->setFlash('error',Yii::t('app','Il pagamento è andato a buon fine, tuttavia, si è verificato un problema tecnico nell’aggiornamento del servizio, contatta il customer care e fornisci il seguente numero di ordine come riferimento : ') .$id_ordine);
                        $this->goHome();
                    endif;

                    //setto ok ordine in session
                    Yii::$app->session->set('ResultOrdine', 'OK');

                    //ritorno la thankyou page e il numero d'ordine
                    return $this->redirect(array('site/transaction-success', 'id_ordine'=>$id_ordine));
                }

            }
        }
        //Display a user friendly Error on the page using any of the following error information returned by PayPal
      $ErrorCode = urldecode($resArray["L_ERRORCODE0"]);
      $ErrorShortMsg = urldecode($resArray["L_SHORTMESSAGE0"]);
      $ErrorLongMsg = urldecode($resArray["L_LONGMESSAGE0"]);
      $ErrorSeverityCode = urldecode($resArray["L_SEVERITYCODE0"]);

      echo "SetExpressCheckout API call failed. ";
      echo "Detailed Error Message: " . $ErrorLongMsg;
      echo "Short Error Message: " . $ErrorShortMsg;
      echo "Error Code: " . $ErrorCode;
      echo "Error Severity Code: " . $ErrorSeverityCode;
        Yii::$app->session->setFlash('error',Yii::t('shop','Si è verificato un problema durante il pagamento con Paypal, si prega di riprovare.'));
        return $this->redirect(['site/select-payment']);

    }


    public function actionResponsella($a,$b){
        //load the NuSoap toolkit
        require_once("../gestpay/nusoap.php");
        $wsdl= Yii::$app->params['gestpay'] . "gestpay/gestpayws/WSCryptDecrypt.asmx?WSDL";
        $client = new \nusoap_client($wsdl,true);
        $shopLogin = $a;
        $CryptedString = $b;

        $params = array('shopLogin' => $shopLogin, 'CryptedString' => $CryptedString);

        $objectresult = $client->call('Decrypt',$params);


        $err = $client->getError();

        if($err){
            // Display the error
            Yii::$app->session->setFlash('error',Yii::t('shop','Transazione non eseguita: '). $err);
            return $this->redirect(['site/checkout-payment']);
        }else{
            //se va a buonfine leggo risultato ed eventuale errore
            $decript = $objectresult['DecryptResult']['GestPayCryptDecrypt'];
            $result=$decript['TransactionResult'];
            $error = $decript['ErrorDescription'];

            //recupero i campi per terashop
            $sa = isset($decript['AuthorizationCode'])?$decript['AuthorizationCode']:'';
            $sb = isset($decript['Amount'])?$decript['Amount']:'';
            $sc = isset($decript['ErrorCode'])?$decript['ErrorCode']:'';
            $sd = isset($decript['ErrorDescription'])?$decript['ErrorDescription']:'';
            $se = isset($decript['AlertCode'])?$decript['AlertCode']:'';
            $sf = isset($decript['VbVFlag'])?$decript['VbVFlag']:'';
            $sg = isset($decript['Country'])?$decript['Country']:'';
            $sh = isset($decript['BankTransactionID'])?$decript['BankTransactionID']:'';
            //salvo nel db i campi per Terashop
            $id_ordine= Yii::$app->session->get('IdOrdine');
            $order= $this->findOrder($id_ordine);
            $order->CC_Autorizzazione = $sa;
            $order->CC_Addebito = $sb;
            $order->CC_Id_Errore = $sc;
            $order->CC_Descrizione_Errore = $sd;
            $order->CC_Allert_Code = $se;
            $order->CC_Verified_by_Visa = $sf;
            $order->CC_Paese = $sg;
            $order->CC_Transazione = $sh;
            $order->CC_Data_Addebito = date('d/m/Y H:i:s');
            $order->save();
            }
            if($result=='KO'){
                // Display the error
                Yii::$app->session->setFlash('error',Yii::t('shop','Transazione non eseguita: '). $error);
                return $this->redirect(['site/checkout-payment']);
            }else{
                $pagamento='bancasella';
                $paese= Yii::$app->session->get('paese');

                //scrivo ordine su csv e lo chiudo
                $this->writeCsv($id_ordine, $paese, $pagamento);

                //creo l'abbonamento (devo gestire anche update)
                //verifico se esiste un abbonamento attivo con questo imei
                $id_account = Yii::$app->user->id;
                $thisKippy = Yii::$app->session->get('thisKippy');
                $cart = Yii::$app->session->get('cart');
                $pack = $cart['items']['pack'];
                $token = isset($decript['TOKEN'])? $decript['TOKEN']:'';
                $token_scadenza_mese = isset($decript['TokenExpiryMonth'])?$decript['TokenExpiryMonth']:'';
                $token_scadenza_anno = isset($decript['TokenExpiryYear'])?$decript['TokenExpiryYear']:'';
                if(Abbonamenti::find()->where(['id_account'=>$id_account,'kippy_imei'=>$thisKippy['imei'],'status'=>1])->exists()):
                    //se esiste ed è attivo è un update / upgrade
                    $abbonamento = Abbonamenti::find()->where(['id_account'=>$id_account,'kippy_imei'=>$thisKippy['imei'],'status'=>1])->one();
                    //disattivo il vecchio abbonamento
                    $abbonamento->status = 0;
                    $abbonamento->save();
                endif;
                //creo un nuovo abbonamento
                $new_abbonamento = new Abbonamenti();
                 //calcolo la data scadenza faccio il max fra oggi e la data + i nuovi mesi
                $scadenza = strtotime($thisKippy['scadenza']);
                $oggi = strtotime(date('Y-m-d'));
                $durata = $pack['durata_mesi'] . " month";
                $scadenza_new = strtotime($durata ,max($scadenza, $oggi));

                $new_abbonamento->id_account = $id_account;
                $new_abbonamento->serial_number = $thisKippy['serial'];
                $new_abbonamento->kippy_imei = $thisKippy['imei'];
                $new_abbonamento->id_prodotto = $pack['id'];
                $new_abbonamento->durata_mesi = $pack['durata_mesi'];
                $new_abbonamento->scadenza = $scadenza_new;
                $new_abbonamento->pagamento = $pagamento;
                //recupero la riga ordine
                $Riga= RigheOrdini::findOne(['id_ordine'=>$id_ordine]);
                $new_abbonamento->sigla_valuta = $Riga->sigla_valuta;
                $new_abbonamento->importo = $Riga->prezzo_ivato;
                $new_abbonamento->token = $token;
                $new_abbonamento->token_scadenza_mese = $token_scadenza_mese;
                $new_abbonamento->token_scadenza_anno = $token_scadenza_anno;
                $new_abbonamento->status = 1;
                $new_abbonamento->save();
                //creo nuova riga su infofatturazione
                $Info = new Infofatturazione();
                $Info->id_abbonamento = $new_abbonamento->id;
                $Info->id_ordine = $id_ordine;
                $Info->start_period = max($scadenza, $oggi);
                $Info->end_period = $scadenza_new;
                $Info->save();
                //aggiorno la scadenza della sim su akerue
                $subscription_end = date('Y-m-d', $scadenza_new);
                $response = $this->ApiSetKippySubscription($this->app_code, $this->app_verification_code, $thisKippy['imei'], $subscription_end);
                if($response->return !==0):
                    Yii::$app->session->setFlash('error',Yii::t('app','Il pagamento è andato a buon fine, tuttavia, si è verificato un problema tecnico nell’aggiornamento del servizio, contatta il customer care e fornisci il seguente numero di ordine come riferimento : ') .$id_ordine);
                    $this->goHome();
                endif;

                //setto ok ordine in session
                Yii::$app->session->set('ResultOrdine', 'OK');

                //ritorno la thankyou page e il numero d'ordine
                return $this->redirect(['site/success']);


            }

    }
    public function actionSuccess(){
        //accedo alla thankyou solo se l'ordine ha avuto successo
        if(Yii::$app->session->get('ResultOrdine')=='OK'){
            //recupero id ordine dalla sessione
            $id_ordine = Yii::$app->session->get('IdOrdine');
            //resetto la sessione cart e IdOrdine
            Yii::$app->session->remove('cart');
            Yii::$app->session->remove('IdOrdine');
            Yii::$app->session->remove('ResultOrdine');

            //invio la mail di conferma ordine
            //$this->sendOrderConfirm($id_ordine);

            return $this->render('success', [
                'id_ordine'=>$id_ordine,
            ]);

        }

        else {

            $this->goHome();
        }



    }



    public function actionScadenza(){
        //aggiorno la scadenza della sim su akerue
                $subscription_end = '2017-03-12';
                $imei = '860854020907027'; // imei '86085402097027';
                $response = $this->ApiSetKippySubscription($this->app_code, $this->app_verification_code, $imei, $subscription_end);
                echo '<pre>'; print_r($response);
                if($response->return !==0):
                    echo 'no set';
                else:
                    echo 'ok';
                endif;
    }
    public function actionAddToCart(){
        if(Yii::$app->request->isAjax){
            $id= $_POST['idPacchetto'];
            $item = $this->getPacchetto($id);
            $paese = Yii::$app->session->get('paese');
            $listino = $this->findListino($id, $paese);

            $cart=Yii::$app->session->get('cart');

            if(isset($cart['items']['pack'])){//item exist
                //rimuovo l'elemento vecchio
                unset($cart['items']['pack']);
            }

            $cart['items']['pack']= [
                'id'=>$item->id,
                'code'=>$item->codice,
                'name'=>  explode('-', $item->codice)[0],
                'description'=> ($paese=='IT')?$item->descrizione_it:$item->descrizione_en,
                'durata_mesi'=>$listino->durata_mesi,
                'currency_code'=>$listino->fk_valuta,
                'currency'=>$listino->fkValuta->simbolo,
                'price'=>$listino->prezzo_ivato
            ];

            Yii::$app->session->set('cart',$cart);

          return true;
        }else{
            $this->goHome();
        }
    }
    protected function checkToken($token)
    {
       //controllo il token
       if(Token::find()->where(['token'=>$token, 'status'=>1])->exists()){
           $tok = Token::find()->where(['token'=>$token, 'status'=>1])->one();
           if((time() - $tok->created_at) > 86400){//86400
               $tok->delete();
               return false;
           }else{
               Yii::$app->session->set('LoginByApp', true);
               return true;
           }
       }
       return false;
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        //if ($model->load(Yii::$app->request->post())) {
        if((Yii::$app->request->isGet && isset($_GET['a']) && isset($_GET['b']) && isset($_GET['c']))|| Yii::$app->request->isPost){
            if(Yii::$app->request->isGet){

                $token = isset($_GET['c'])?$_GET['c']:null;
                $isTokenValid = $this->checkToken($token);

                $model->email = (isset($_GET['a']) && $isTokenValid)?base64_decode($_GET['a']):null;
                $model->password = (isset($_GET['b']) && $isTokenValid)?base64_decode($_GET['b']):null;

            }else{
                $model->load(Yii::$app->request->post());
            }

            $identity = $this->ApiLogin($model->email, $model->password);

            if($identity->return==0){
                $loggedin_params = [
                            'app_code'=>$identity->app_code,
                            'app_verification_code'=>$identity->app_verification_code,
                        ];
                Yii::$app->session->set('loggedIn_params', $loggedin_params);
                $user = $this->ApiGetUser($identity->app_code, $identity->app_verification_code);

                if($user->return==0){

                    //se l'utente non esiste lo registro a db
                    if(!Account::find()->where(['username'=>$user->user_id])->exists()){
                        $account = new Account;
                        $account->username = $user->user_id;
                        $account->nome = $user->name;
                        $account->cognome = $user->surname;
                        $account->indirizzo = $user->address;
                        $account->citta = $user->city;
                        $account->nazione = Yii::$app->session->get('paese');
                        $account->email = $user->email;
                        $account->telefono = $user->phone;
                        if(!$account->save()){
                            Yii::$app->session->setFlash('error', DecodeError::getMessage (108));
                            return $this->goHome();
                        }

                    }
                    //faccio la login dell'utente
                    Yii::$app->user->login($this->getAccount($user->user_id),0);

                    //leggo la lingua e la metto in sessione
                    $language = DecodeLang::getCode($user->language_id);
                    Yii::$app->session->set('lang', $language);
                    //Yii::$app->session->set('lang', 'es-es');//serve per test.. commentare in produzione

                    //recupero e metto in session app_code e app_verification_code dell'admin
                    $admin = $this->ApiLogin($this->admin_user, $this->admin_password);
                    if($admin->return==0){

                        $auth = [
                            'app_code'=>$admin->app_code,
                            'app_verification_code'=>$admin->app_verification_code,
                        ];
                        Yii::$app->session->set('auth', $auth);
                    }else{
                        Yii::$app->session->setFlash('error', DecodeError::getMessage ($admin->return));
                    }

                } else {
                    Yii::$app->session->setFlash('error', DecodeError::getMessage ($user->return));
                }

            }else{
                Yii::$app->session->setFlash('error', DecodeError::getMessage ($identity->return));
            }
            return $this->goHome();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }
     /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
    /** Password recovery **/
    public function actionPasswordRecovery()
    {
        $model = new DynamicModel(['email']);
         $model->addRule(['email'], 'required');
        $model->addRule(['email'], 'email');

        return $this->render('password-recovery',[
            'model' => $model,
        ]);
    }
    public function actionGetPassword()
    {
        if(!Yii::$app->request->isAjax)
        return $this->goHome ();

        $registration_email=  $_POST['DynamicModel']['email'];
        $response = $this->ApiForgotPassword($registration_email);
        return $response->return;

    }
     /** Password recovery **/
    //invio la dem di conferma ordine
    protected function sendOrderConfirm($id_ordine)
    {

        switch (Yii::$app->language){
            case 'it-it':
                $sub = 'Il tuo pacchetto di servizi Kippy è confermato';
                break;
            case 'en-gb':
                $sub = 'Your Kippy service pack is confirmed';
                break;
            case 'fr-fr':
                $sub = 'Forfait des services Kippy  confirmé';
                break;
            case 'de-de':
                $sub = 'Dein Service-paket Kippywurde korrekt aktiviert';
                break;
            case 'es-es':
                $sub = 'Tu paquete de servicios Kippy ha sido activado correctamente';
                break;
            default:
                $sub = 'Your Kippy service pack is confirmed';// in inglese
                break;
        }
        $language = substr(Yii::$app->language, 0,2);

        $Infofatturazione = Infofatturazione::findOne(['id_ordine'=>$id_ordine]);

        $nome_pacchetto = explode('-', $Infofatturazione->idAbbonamento->idProdotto->codice)[0];
        $prezzo = $Infofatturazione->idAbbonamento->sigla_valuta . ' ' .$Infofatturazione->idAbbonamento->importo;
        $durata =  ($Infofatturazione->idAbbonamento->durata_mesi ==1)? Yii::t('app', '4 settimane') : $Infofatturazione->idAbbonamento->durata_mesi . ' ' . Yii::t('app', 'mesi');
        $isVita = false;
        $localizzazioni = ($nome_pacchetto == 'BASIC')? '10000' : Yii::t('app', 'Illimitate');
        $checkPetme = ($nome_pacchetto == 'BASIC')? false : true;
        $checkCustomercare = ($nome_pacchetto == 'ULTIMATE')? true : false;
        $primipassi = false;

        //controllo se il kippy è vita o finder
        $pet = Yii::$app->session->get('thisKippy');
        $isVita = ($pet['tipo']=='vita')? true:false;


        //controllo se il kippy è un licenza 0 e se è il primo abbonamento sottoscritto
        if(substr($Infofatturazione->idAbbonamento->idProdotto->codice, -3)=='NEW' && count(Abbonamenti::findAll(['serial_number'=>$Infofatturazione->idAbbonamento->serial_number]))==1)
           $primipassi = true;


	return Yii::$app->mailer->compose(['html'=>'pacchetti_'.$language],[
                    'nome_pacchetto'=>$nome_pacchetto,
                    'prezzo'=>$prezzo,
                    'durata'=>$durata,
                    'isVita'=>$isVita,
                    'localizzazioni'=>$localizzazioni,
                    'checkPetme'=>$checkPetme,
                    'checkCustomercare'=>$checkCustomercare,
                    'primipassi'=>$primipassi,
                    'paese'=>$Infofatturazione->idOrdine->nazione,
                ])
	    ->setFrom('orders@kippy.eu')
	    ->setTo($Infofatturazione->idOrdine->email)
	    ->setSubject($sub)
	    ->send();

    }
    //invio la dem di disdetta pacchetto
    protected function sendDisdetta($nome_pacchetto)
    {
        switch (Yii::$app->language){
            case 'it-it':
                $sub = 'Il tuo pacchetto di servizi è stato annullato';
                break;
            case 'en-gb':
                $sub = 'Your Kippy service pack has been canceled';
                break;
            case 'fr-fr':
                $sub = 'Votre forfait de service a été annulé';
                break;
            case 'de-de':
                $sub = 'Dein Service-paket Kippy wurde storniert';
                break;
            case 'es-es':
                $sub = 'Tu paquete de servicios Kippy ha sido cancelado';
                break;
            default:
                $sub = 'Your Kippy service pack has been canceled';// in inglese
                break;
        }
        $language = substr(Yii::$app->language, 0,2);


        return Yii::$app->mailer->compose(['html'=>'disdetta_'.$language],[
                    'nome_pacchetto'=>$nome_pacchetto,
                ])
	    ->setFrom('orders@kippy.eu')
	//    ->setTo(Yii::$app->user->identity->email)
    ->setTo('miloslukicbk1@gmail.com')
	    ->setSubject($sub)
	    ->send();
    }

    //genero il file csv e scrivo l'ordine
    protected function writeCsv($id,$paese,$pagamento){
        //confermo lo stato righe e ordine
            $ordine= $this->findOrder($id);
            $ordine->status=2;
            $ordine->pagamento= $pagamento;
            if($ordine->pagamento=='bancasella') $ordine->ModPag='CCBS';
            if($ordine->pagamento=='paypal') $ordine->ModPag='PPLSTD';
            if($ordine->save(false)){
                $Righe= RigheOrdini::findAll(['id_ordine'=>$id]);
                foreach ($Righe as $riga){
                    $riga->stato_riga='confirmed';
                    $riga->save(false);
                }
            }
            //genero il csv per terashop
            $country=  Country::findOne(['sigla'=>$paese]);
            $istanza=$country->istanza;
            $righeordine=array();
            if(!file_exists(Yii::$app->params['csvPath']."/ordini/ordini.csv"))
            $righeordine[] ='id ordine|Descrizione prodotto|Codice prodotto|Prezzo ivato|valuta|istanze|quantità|'
                . 'nome|cognome|Codice Fiscale|Indirizzo|citofono|comune/città|'
                . 'CAP|nazione|provincia/stato/Contea/Bund|email|telefono|pagamento|'
                . 'ragione_sociale|partita_iva|fattura_nome|fattura_cognome|fattura_indirizzo|'
                . 'fattura_citta|fattura_cap|fattura_nazione|fattura_provincia|fattura_email|'
                . 'ModPag|CC_Autorizzazione|CC_Addebito|CC_Id_Errore|CC_Descrizione_Errore|'
                . 'CC_Allert_Code|CC_Verified_by_Visa|CC_Paese|CC_Transazione|CC_Data_Addebito|id riga';
            foreach ($Righe as $riga){
                $righeordine[]=$id.'|'.$riga->descrizione_prodotto.'|'.$riga->codice_prodotto.'|'.$riga->prezzo_ivato.'|'.$riga->sigla_valuta.'|'.$istanza.'|'.$riga->quantita
                               .'|'.$ordine->nome.'|'.$ordine->cognome.'|'.$ordine->codice_fiscale.'|'.$ordine->indirizzo.'|'.$ordine->citofono.'|'.$ordine->citta
                               .'|'.$ordine->cap.'|'.$ordine->nazione.'|'.$ordine->provincia.'|'.$ordine->email.'|'.$ordine->telefono.'|'.$ordine->pagamento
                               .'|'.$ordine->ragione_sociale.'|'.$ordine->partita_iva.'|'.$ordine->fattura_nome.'|'.$ordine->fattura_cognome.'|'.$ordine->fattura_indirizzo
                               .'|'.$ordine->fattura_citta.'|'.$ordine->fattura_cap.'|'.$ordine->fattura_nazione.'|'.$ordine->fattura_provincia.'|'.$ordine->fattura_email
                               .'|'.$ordine->ModPag.'|'.$ordine->CC_Autorizzazione.'|'.$ordine->CC_Addebito.'|'.$ordine->CC_Id_Errore.'|'.$ordine->CC_Descrizione_Errore
                               .'|'.$ordine->CC_Allert_Code.'|'.$ordine->CC_Verified_by_Visa.'|'.$ordine->CC_Paese.'|'.$ordine->CC_Transazione.'|'.$ordine->CC_Data_Addebito. '|' . $riga->id;
            }


            $file = fopen(Yii::$app->params['csvPath']."/ordini/ordini.csv","a+");

            foreach ($righeordine as $line)
              {
              $newline=  str_replace(";", ",", $line);//rimuovo eventuali punti e virgola se ci sono nei campi
              $newline=  str_replace("|", ";", $newline);//trasformo i pipe in punti e virgola per separare i campi
              fwrite($file, $newline. "\n");
              }

            fclose($file);

    }
    /**
     * Finds Account by [[user_id]]
     *
     * @return User|null
     */
    protected function getAccount($user_id)
    {
        return Account::findOne(['username'=>$user_id]);
    }
    protected function getPacchetto($id){
        return Prodotti::findOne($id);
    }
    protected function getPacchetti($categoria){
        return Prodotti::find()->where(['categoria'=>$categoria,'status'=>1])->all();
    }
    protected function findListino($fk_prodotto,$fk_paese){

      //  $listino_due = Listino::find()->where(['fk_prodotto'=>$fk_prodotto,'status'=>1])->one();
      //  $listino_due = Listino::find()->where(['fk_prodotto'=>$fk_prodotto])->andWhere(['status'=>1])->andWhere(['fk_paese'=>$fk_paese])->one();

//
//        echo '<pre>'.print_r($listino_due, true).'</pre>';
//        die();

    //    return $listino_due;

       // original

//        echo '<pre>'.Listino::find()->where(['fk_prodotto'=>$fk_prodotto,'fk_paese'=>$fk_paese,'status'=>1])->one().'</pre>';
//        die();

       return Listino::find()->where(['fk_prodotto'=>$fk_prodotto,'fk_paese'=>$fk_paese,'status'=>1])->one();






    }
    protected function findOrder($id){
        return Ordini::findOne($id);
    }
    protected function getTotaleOrder($id_order){
        return RigheOrdini::find()->where(['id_ordine'=>$id_order])->sum('prezzo_ivato');
    }
    /*'----------------------------------------------------------------------------------
	 * This function will take NVPString and convert it to an Associative Array and it will decode the response.
	  * It is usefull to search for a particular key and displaying arrays.
	  * @nvpstr is NVPString.
	  * @nvpArray is Associative Array.
	   ----------------------------------------------------------------------------------
	  */
    private function deformatNVP($nvpstr)
    {
            $intial=0;
            $nvpArray = array();

            while(strlen($nvpstr))
            {
                    //postion of Key
                    $keypos= strpos($nvpstr,'=');
                    //position of value
                    $valuepos = strpos($nvpstr,'&') ? strpos($nvpstr,'&'): strlen($nvpstr);

                    /*getting the Key and Value values and storing in a Associative Array*/
                    $keyval=substr($nvpstr,$intial,$keypos);
                    $valval=substr($nvpstr,$keypos+1,$valuepos-$keypos-1);
                    //decoding the respose
                    $nvpArray[urldecode($keyval)] =urldecode( $valval);
                    $nvpstr=substr($nvpstr,$valuepos+1,strlen($nvpstr));
         }
            return $nvpArray;
    }
    /************************************************************************
     *                                                                      *
     *                         API AKERUE / KIPPY.                          *
     *                                                                      *
     ************************************************************************/
    protected function ApiForgotPassword($registration_email){
        $uri = Yii::$app->params['apiKippy'] . "/request_forgot_password.php";

        $response = Request::post($uri)
                    ->expectsJson()
                    ->body('{  "registration_email" : "'.$registration_email.'", '
                            . '"app_identity" : "vita"'
                            . ' }')
                    ->send();

        return $response->body;
    }
    protected function ApiLogin($login_email,$login_password,$app_identity="",$token_device="",$platform_device="",$app_version="",$device_name=""){
        $uri = Yii::$app->params['apiKippy'] . "/login.php";

        $response = Request::post($uri)
                    ->expectsJson()
                    ->sendsJson()
                    ->body('{  "login_email" : "'.$login_email.'", '
                            . '"login_password" : "'.$login_password.'",'
                            . '"app_identity" : "'.$app_identity.'",'
                            . '"token_device" : "'.$token_device.'",'
                            . '"platform_device" : "'.$platform_device.'",'
                            . '"app_version" : "'.$app_version.'",'
                            . '"device_name" : "'.$device_name.'"'
                            . ' }')
                    ->send();

            return $response->body;

    }
    protected function ApiGetKippyList($app_code, $app_verification_code, $user_id){
        $uri = Yii::$app->params['apiKippy'] . "/admin/admin_get_kippypet_info.php";

        $response = Request::post($uri)
                    ->expectsJson()
                    ->authenticateWith($this->basic_username, $this->basic_password)
                    ->body('{  "app_code" : "'.$app_code.'", '
                            . '"app_verification_code" : "'.$app_verification_code.'",'
                            . '"user_id" : "'.$user_id.'"'
                            . ' }')
                    ->send();

        return $response->body;
    }
    protected function ApiGetKippySubscription($app_code, $app_verification_code, $imei){
        $uri = Yii::$app->params['apiKippy'] . "/admin/admin_get_kippy_subscription.php";

        $response = Request::post($uri)
                    ->expectsJson()
                    ->authenticateWith($this->basic_username, $this->basic_password)
                    ->body('{  "app_code" : "'.$app_code.'", '
                            . '"app_verification_code" : "'.$app_verification_code.'",'
                            . '"kippy_imei" : "'.$imei.'"'
                            . ' }')
                    ->send();

        return $response->body;
    }
    protected function ApiSetKippySubscription($app_code, $app_verification_code, $imei, $scadenza){
        $uri = Yii::$app->params['apiKippy'] . "/admin/admin_set_kippy_subscription.php";

        $response = Request::post($uri)
                    ->expectsJson()
                    ->authenticateWith($this->basic_username, $this->basic_password)
                    ->body('{  "app_code" : "'.$app_code.'", '
                            . '"app_verification_code" : "'.$app_verification_code.'",'
                            . '"kippy_imei" : "'.$imei.'",'
                            . '"subscription_end" : "'.$scadenza.'"'
                            . ' }')
                    ->send();

        return $response->body;
    }
    protected function ApiGetUser($app_code, $app_verification_code, $app_identity=""){
        $uri = Yii::$app->params['apiKippy'] . "/kippymap_getUser.php";

        $response = Request::post($uri)
                    ->expectsJson()
                    ->body('{  "app_code" : "'.$app_code.'", '
                            . '"app_verification_code" : "'.$app_verification_code.'",'
                            . '"app_identity" : "'.$app_identity.'"'
                            . ' }')
                    ->send();

        return $response->body;
    }
//    public function actionTestApi(){
//        $uri = 'localhost/~paologiarda/kippydashboard/generateToken';
//
//        $response = Request::post($uri)
//                    ->expectsJson()
//                    ->authenticateWith('admin_kippy', '3!77gtEOlkU)bb2sktUU18FFpp1')
//
//                    ->send();
//
//        echo '<pre>'; print_r($response);
//    }
}
