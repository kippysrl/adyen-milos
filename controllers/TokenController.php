<?php
namespace app\controllers;


use yii\rest\Controller;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\VerbFilter;
use yii\base\Security;
use app\models\Token;

class TokenController extends Controller
{
    public $modelClass = 'app\models\Token';
    

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
            'auth' => [$this , 'auth']
        ];
        
        
        return $behaviors;
    }
    public function auth($username, $password)
    {
        return \app\models\Account::findOne(
            [
                'username' => $username,
                'access_token' => $password
            ]
        );

    }
    public function actionCreate(){
        $token= new Token();
        $security =new Security();
        $token->token = $security->generateRandomString();
        $token->status = 1;
        $token->save();
        
        return [$token];
        
    }
    
    
}