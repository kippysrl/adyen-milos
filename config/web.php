<?php

$params = require(__DIR__ . '/params.php');
use \yii\web\Request;
$baseUrl = str_replace('/web', '', (new Request)->getBaseUrl());
$config = [
    'id' => 'basic',
    'name'=>'Kippy | Renewal Dashboard',
    'timezone' => 'Europe/Rome',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'gridview' =>  [
        'class' => '\kartik\grid\Module'
        ],
        
    ],
    'components' => [
        'session' => [
            'class' => '\yii\web\Session',
            'name' => 'kippydasboard',
        ],
        'request' => [
            'baseUrl' => $baseUrl,
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'adAFUbNGncTLO0w_I1oo5hHW4g_luaFW',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\Account',
            //'enableSession' => false,
            //'loginUrl'=>null
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@app/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
                'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.inet.it', 
                'username' => 'orders@kippy.eu',
                'password' => 'Orders2016!',
                'port' => '25',   
                'encryption' => '',
            ],
        ], 
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'it-it',
                    'basePath' => '@app/messages',
                ]
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            
            'class' => 'yii\web\UrlManager',
            
            'showScriptName' => false,
            //'enableStrictParsing' => true,
            'enablePrettyUrl' => true,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule', 
                    'controller' => ['updateAbbonamenti'=> 'abbonamenti'],
                    'pluralize' =>false//disabilito la s finale
                ],
                [
                    'class' => 'yii\rest\UrlRule', 
                    'controller' => ['infoAbbonamenti'=> 'infofatturazione'],
                    'pluralize' =>false//disabilito la s finale
                ],
                [
                    'class' => 'yii\rest\UrlRule', 
                    'controller' => ['updateFatturazione'=> 'fatturazione'],
                    'pluralize' =>false//disabilito la s finale
                ],
                [
                    'class' => 'yii\rest\UrlRule', 
                    'controller' => ['generateToken'=> 'token'],
                    'pluralize' =>false//disabilito la s finale
                ],
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<page:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
	],
    ],
    'language'=>'it-it',
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
//    $config['bootstrap'][] = 'debug';
//    $config['modules']['debug'] = [
//        'class' => 'yii\debug\Module',
//    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
