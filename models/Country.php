<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "country".
 *
 * @property string $sigla
 * @property string $paese
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Listino[] $listinos
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sigla', 'paese', 'status'], 'required'],
            [['sigla', 'paese'], 'filter', 'filter' => 'trim'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['sigla'], 'string', 'max' => 2, 'min' => 2],
            [['paese'], 'string', 'max' => 250],
            [['istanza'], 'string', 'max' => 50],
            [['sigla'], 'unique'],
            [['paese'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sigla' => 'Sigla',
            'paese' => 'Paese',
            'istanza' => 'Istanza',
            'status' => 'Attivo',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getListinos()
    {
        return $this->hasMany(Listino::className(), ['fk_paese' => 'sigla']);
    }
}
