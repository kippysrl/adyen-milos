<?php

namespace app\models;

use Yii;



/**
 * This is the model class for table "subscription_cancel".
 *
 * @property int $id
 * @property string $username
 * @property string $nome
 * @property string $cognome
 * @property string $email
 * @property string $telefono
 * @property string $messaggio
 * @property string $ragionare
 * @property string $indirizzo
 * @property string $cap
 * @property string $provincia
 * @property int $nazione
 * @property string $recurringAdyenReference
 * @property string $created_at
 */
class SubscriptionCancel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscription_cancel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'nome', 'cognome', 'email', 'telefono', 'messaggio', 'ragionare', 'indirizzo', 'cap', 'provincia', 'nazione', 'recurringAdyenReference','account_creato'], 'required'],
            [['messaggio'], 'string'],
            [['nazione'], 'string', 'max'=>50],
            [['account_creato'], 'integer'],
            [['created_at'], 'safe'],
            [['username', 'nome', 'telefono', 'recurringAdyenReference'], 'string', 'max' => 25],
            [['cognome'], 'string', 'max' => 30],
            [['email'], 'string', 'max' => 150],
            [['ragionare', 'indirizzo', 'provincia'], 'string', 'max' => 100],
            [['cap'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'nome' => 'Nome',
            'cognome' => 'Cognome',
            'email' => 'Email',
            'telefono' => 'Telefono',
            'messaggio' => 'Messaggio',
            'ragionare' => 'Ragionare',
            'indirizzo' => 'Indirizzo',
            'cap' => 'Cap',
            'provincia' => 'Provincia',
            'nazione' => 'Nazione',
            'recurringAdyenReference' => 'Recurring Adyen Reference',
            'created_at' => 'Created At',
            'account_creato'=>'Account Creato'
        ];
    }
}
