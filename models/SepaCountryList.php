<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sepa_country_list".
 *
 * @property int $id
 * @property string $country_name
 * @property string $iban
 * @property string $currency
 */
class SepaCountryList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sepa_country_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_name', 'iban', 'currency'], 'required'],
            [['country_name'], 'string', 'max' => 20],
            [['iban', 'currency'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country_name' => 'Country Name',
            'iban' => 'Iban',
            'currency' => 'Currency',
        ];
    }
}
