<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "infofatturazione".
 *
 * @property integer $id
 * @property integer $id_abbonamento
 * @property integer $id_ordine
 * @property integer $start_period
 * @property integer $end_period
 * @property string  $note
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Abbonamenti $idAbbonamento
 * @property Ordini $idOrdine
 */
class Infofatturazione extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'infofatturazione';
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_abbonamento', 'start_period', 'end_period'], 'required'],
            [['note'], 'string', 'max' => 250],
            [['id_abbonamento', 'id_ordine', 'start_period', 'end_period', 'created_at', 'updated_at'], 'integer'],
            [['id_abbonamento'], 'exist', 'skipOnError' => true, 'targetClass' => Abbonamenti::className(), 'targetAttribute' => ['id_abbonamento' => 'id']],
            [['id_ordine'], 'exist', 'skipOnError' => true, 'targetClass' => Ordini::className(), 'targetAttribute' => ['id_ordine' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_abbonamento' => 'Id Abbonamento',
            'id_ordine' => 'Id Ordine',
            'start_period' => 'Start Period',
            'end_period' => 'End Period',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAbbonamento()
    {
        return $this->hasOne(Abbonamenti::className(), ['id' => 'id_abbonamento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdOrdine()
    {
        return $this->hasOne(Ordini::className(), ['id' => 'id_ordine']);
    }
}
