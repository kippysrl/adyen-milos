<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "listino".
 *
 * @property integer $id
 * @property integer $fk_prodotto
 * @property string $fk_paese
 * @property string $fk_valuta
 * @property double $prezzo_ivato
 * @property double $opzione_12
 * @property double $opzione_24
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Country $fkPaese
 * @property Prodotti $fkProdotto
 * @property Valuta $fkValuta
 */
class Listino extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'listino';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_prodotto', 'status', 'created_at', 'updated_at'], 'integer'],
            [['prezzo_ivato', 'opzione_12', 'opzione_24'], 'number'],
            [['opzione_12', 'opzione_24', 'status', 'created_at', 'updated_at'], 'required'],
            [['fk_paese'], 'string', 'max' => 2],
            [['fk_valuta'], 'string', 'max' => 3],
            [['fk_paese'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['fk_paese' => 'sigla']],
            [['fk_prodotto'], 'exist', 'skipOnError' => true, 'targetClass' => Prodotti::className(), 'targetAttribute' => ['fk_prodotto' => 'id']],
            [['fk_valuta'], 'exist', 'skipOnError' => true, 'targetClass' => Valuta::className(), 'targetAttribute' => ['fk_valuta' => 'sigla']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fk_prodotto' => 'Fk Prodotto',
            'fk_paese' => 'Fk Paese',
            'fk_valuta' => 'Fk Valuta',
            'prezzo_ivato' => 'Prezzo Ivato',
            'opzione_12' => 'Opzione 12',
            'opzione_24' => 'Opzione 24',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkPaese()
    {
        return $this->hasOne(Country::className(), ['sigla' => 'fk_paese']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkProdotto()
    {
        return $this->hasOne(Prodotti::className(), ['id' => 'fk_prodotto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkValuta()
    {
        return $this->hasOne(Valuta::className(), ['sigla' => 'fk_valuta']);
    }
}
