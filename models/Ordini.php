<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "ordini".
 *
 * @property integer $id
 * @property string $ip
 * @property string $nome
 * @property string $cognome
 * @property string $codice_fiscale
 * @property string $indirizzo
 * @property string $citofono
 * @property string $citta
 * @property string $cap
 * @property string $nazione
 * @property string $provincia
 * @property string $email
 * @property string $telefono
 * @property string $ragione_sociale
 * @property string $partita_iva
 * @property string $fattura_nome
 * @property string $fattura_cognome
 * @property string $fattura_indirizzo
 * @property string $fattura_citta
 * @property string $fattura_cap
 * @property string $fattura_nazione
 * @property string $fattura_provincia
 * @property string $fattura_email
 * @property string $pagamento
 * @property integer $incassato
 * @property string $ModPag
 * @property string $CC_Autorizzazione
 * @property string $CC_Addebito
 * @property string $CC_Id_Errore
 * @property string $CC_Descrizione_Errore
 * @property string $CC_Allert_Code
 * @property string $CC_Verified_by_Visa
 * @property string $CC_Paese
 * @property string $CC_Transazione
 * @property string $CC_Data_Addebito
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Pending[] $pendings
 * @property PetmeOrdini[] $petmeOrdinis
 * @property RigheOrdini[] $righeOrdinis
 */
class Ordini extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ordini';
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    public $privacy;
    public $condizioni;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nome', 'cognome', 'indirizzo', 'citofono', 'citta', 'cap', 'nazione', 'provincia', 'email', 'telefono','fattura_nome', 'fattura_cognome', 'fattura_indirizzo','fattura_cap', 'fattura_citta', 'fattura_nazione', 'fattura_provincia', 'fattura_email'], 'required'],
            [['incassato', 'status', 'created_at', 'updated_at'], 'integer'],
            [['ip'], 'string', 'max' => 15],
            [['nome', 'cognome', 'indirizzo', 'citofono', 'citta', 'nazione', 'provincia', 'email', 'ragione_sociale', 'fattura_nome', 'fattura_cognome', 'fattura_indirizzo', 'fattura_citta', 'fattura_nazione', 'fattura_provincia', 'fattura_email'], 'string', 'max' => 250],
            [['partita_iva','cap', 'fattura_cap','codice_fiscale', 'telefono','nome', 'cognome', 'indirizzo', 'citofono', 'citta', 'nazione', 'provincia', 'email', 'ragione_sociale', 'fattura_nome', 'fattura_cognome', 'fattura_indirizzo', 'fattura_citta', 'fattura_nazione', 'fattura_provincia', 'fattura_email'],'filter', 'filter'=> 'trim'],
            [['ModPag','codice_fiscale', 'telefono', 'pagamento'], 'string', 'max' => 50],
            [['CC_Autorizzazione','CC_Addebito','CC_Id_Errore','CC_Descrizione_Errore','CC_Allert_Code','CC_Verified_by_Visa','CC_Paese','CC_Transazione','CC_Data_Addebito'], 'string', 'max' => 250],
            [['cap', 'fattura_cap'], 'string', 'max' => 10],
            [['partita_iva'], 'string', 'max' => 20],
            [['email','fattura_email'],'email'],
            
            
            // checkbox deve essere spuntato =1
            //['privacy', 'required', 'requiredValue' => 1, 'message' =>Yii::t('shop','Devi accettare la privacy per proseguire'),'on'=>'create'],
            // checkbox deve essere spuntato =1
            ['condizioni', 'required', 'requiredValue' => 1, 'message' =>Yii::t('shop','Devi accettare termini e condizioni per proseguire'),'on'=>'create'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ip' => 'Ip',
            'nome' => Yii::t('shop','Nome'),
            'cognome' => Yii::t('shop', 'Cognome'),
            'codice_fiscale' => Yii::t('shop','Codice Fiscale'),
            'indirizzo' => Yii::t('shop','Indirizzo'),     
            'citofono' => Yii::t('shop','Citofono'),
            'citta' => Yii::t('shop','Citta'),
            'cap' => Yii::t('shop','Cap'),
            'nazione' => Yii::t('shop','Paese'),
            'provincia' => Yii::t('shop','Stato/provincia'),
            'email' => Yii::t('shop', 'E-mail'),
            'telefono' => Yii::t('shop','Telefono'),
            'ragione_sociale' => Yii::t('shop','Ragione Sociale'),
            'partita_iva' => Yii::t('shop','Partita Iva'),
            'fattura_nome' => Yii::t('shop','Nome'),
            'fattura_cognome' => Yii::t('shop','Cognome'),
            'fattura_indirizzo' => Yii::t('shop','Indirizzo'),
            'fattura_civico' => Yii::t('shop','Numero civico'),
            'fattura_citta' => Yii::t('shop','Città' ),
            'fattura_cap' => Yii::t('shop','Cap' ),
            'fattura_nazione' => Yii::t('shop','Paese' ),
            'fattura_provincia' => Yii::t('shop','Stato/provincia'),
            'fattura_email' => Yii::t('shop','E-mail'),
            'pagamento' => 'Pagamento',
            'incassato' => 'Incassato',
            'status' => 'Stato',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPendings()
    {
        return $this->hasMany(Pending::className(), ['id_ordine' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPetmeOrdinis()
    {
        return $this->hasMany(PetmeOrdini::className(), ['id_ordine' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRigheOrdinis()
    {
        return $this->hasMany(RigheOrdini::className(), ['id_ordine' => 'id']);
    }
}
