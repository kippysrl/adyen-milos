<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "province".
 *
 * @property string $cod_iso
 * @property string $sigla_country
 * @property string $provincia
 * @property string $sigla
 */
class Province extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'province';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cod_iso', 'sigla_country', 'provincia', 'sigla'], 'required'],
            [['cod_iso'], 'string', 'max' => 50],
            [['sigla_country'], 'string', 'max' => 2],
            [['provincia'], 'string', 'max' => 250],
            [['sigla'], 'string', 'max' => 5],
            [['cod_iso'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cod_iso' => 'Cod Iso',
            'sigla_country' => 'Sigla Country',
            'provincia' => 'Provincia',
            'sigla' => 'Sigla',
        ];
    }
}
