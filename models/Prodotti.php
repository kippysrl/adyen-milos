<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prodotti".
 *
 * @property integer $id
 * @property string $categoria
 * @property string $codice
 * @property string $descrizione_it
 * @property string $descrizione_en
 * @property string $dettagli_it
 * @property string $dettagli_en
 * @property string $foto
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Listino[] $listinos
 */
class Prodotti extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prodotti';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categoria', 'codice', 'descrizione_it', 'descrizione_en', 'status', 'created_at', 'updated_at'], 'required'],
            [['dettagli_it', 'dettagli_en'], 'string'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['categoria', 'codice'], 'string', 'max' => 50],
            [['descrizione_it', 'descrizione_en', 'foto'], 'string', 'max' => 250],
            [['codice'], 'unique'],
            [['descrizione_it'], 'unique'],
            [['descrizione_en'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'categoria' => 'Categoria',
            'codice' => 'Codice',
            'descrizione_it' => 'Descrizione It',
            'descrizione_en' => 'Descrizione En',
            'dettagli_it' => 'Dettagli It',
            'dettagli_en' => 'Dettagli En',
            'foto' => 'Foto',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getListinos()
    {
        return $this->hasMany(Listino::className(), ['fk_prodotto' => 'id']);
    }
}
