<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "abbonamenti".
 *
 * @property integer $id
 * @property integer $id_account
 * @property string $serial_number
 * @property string $kippy_imei
 * @property integer $id_prodotto
 * @property integer $durata_mesi
 * @property integer $scadenza
 * @property string $pagamento
 * @property string $sigla_valuta
 * @property double $importo
 * @property string $token
 * @property string $token_scadenza_mese
 * @property string $token_scadenza_anno
 * @property integer $status
 * @property integer $disdetta
 * @property integer $sospeso
 * @property integer $rinnovi_falliti
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Account $idAccount
 * @property Prodotti $idProdotto
 */
class Abbonamenti extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'abbonamenti';
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_account', 'serial_number', 'kippy_imei', 'id_prodotto', 'durata_mesi', 'scadenza', 'pagamento','sigla_valuta','importo', 'token','status'], 'required'],
            [['id_account', 'id_prodotto', 'durata_mesi', 'scadenza', 'status','disdetta','sospeso','rinnovi_falliti', 'recurring', 'created_at', 'updated_at'], 'integer'],
            [['serial_number', 'pagamento', 'token_scadenza_mese', 'token_scadenza_anno'], 'string', 'max' => 50],
            [['kippy_imei'], 'string', 'max' => 150],
            [['recurringDetailReference'], 'string', 'max' => 20],
            [['token'], 'string', 'max' => 250],
            [['importo'], 'number'],
            [['sigla_valuta'], 'string', 'max' => 3],
            [['id_account'], 'exist', 'skipOnError' => true, 'targetClass' => Account::className(), 'targetAttribute' => ['id_account' => 'id']],
            [['id_prodotto'], 'exist', 'skipOnError' => true, 'targetClass' => Prodotti::className(), 'targetAttribute' => ['id_prodotto' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_account' => 'Id Account',
            'serial_number' => 'Serial Number',
            'kippy_imei' => 'Kippy Imei',
            'id_prodotto' => 'Id Prodotto',
            'durata_mesi' => 'Durata Mesi',
            'scadenza' => 'Scadenza',
            'pagamento' => 'Pagamento',
            'token' => 'Token',
            'token_scadenza_mese' => 'Token Scadenza Mese',
            'token_scadenza_anno' => 'Token Scadenza Anno',
            'status' => 'Status',
            'recurring'=>'Subscription',
            'recurringDetailReference'=>'Adyen reference code',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAccount()
    {
        return $this->hasOne(Account::className(), ['id' => 'id_account']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProdotto()
    {
        return $this->hasOne(Prodotti::className(), ['id' => 'id_prodotto']);
    }
}
