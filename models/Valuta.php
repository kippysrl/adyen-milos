<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "valuta".
 *
 * @property string $sigla
 * @property string $moneta
 * @property string $simbolo
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Cupon[] $cupons
 * @property Listino[] $listinos
 */
class Valuta extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'valuta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sigla', 'moneta', 'simbolo', 'status', 'created_at', 'updated_at'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['sigla'], 'string', 'max' => 3],
            [['moneta'], 'string', 'max' => 250],
            [['simbolo'], 'string', 'max' => 5],
            [['sigla'], 'unique'],
            [['moneta'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sigla' => 'Sigla',
            'moneta' => 'Moneta',
            'simbolo' => 'Simbolo',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCupons()
    {
        return $this->hasMany(Cupon::className(), ['fk_valuta' => 'sigla']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getListinos()
    {
        return $this->hasMany(Listino::className(), ['fk_valuta' => 'sigla']);
    }
}
