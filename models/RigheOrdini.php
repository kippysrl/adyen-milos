<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "righe_ordini".
 *
 * @property integer $id
 * @property integer $id_ordine
 * @property integer $id_prodotto
 * @property string $codice_prodotto
 * @property string $descrizione_prodotto
 * @property string $sigla_valuta
 * @property double $prezzo_ivato
 * @property integer $quantita
 * @property string $tracking_number
 * @property string $corriere
 * @property string $link
 * @property string $stato_riga
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Ordini $idOrdine
 */
class RigheOrdini extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'righe_ordini';
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    public $da_data;
    public $a_data;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_ordine','id_prodotto', 'quantita', 'created_at', 'updated_at'], 'integer'],
            [['codice_prodotto', 'descrizione_prodotto', 'sigla_valuta', 'stato_riga'], 'required'],
            [['prezzo_ivato'], 'number'],
            [['codice_prodotto', 'stato_riga'], 'string', 'max' => 50],
            [['kippy_imei'], 'string', 'max' => 150],
            [['descrizione_prodotto', 'tracking_number', 'corriere', 'link'], 'string', 'max' => 250],
            [['sigla_valuta'], 'string', 'max' => 3]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_ordine' => 'Id Ordine',
            'codice_prodotto' => 'Codice Prodotto',
            'descrizione_prodotto' => 'Descrizione Prodotto',
            'sigla_valuta' => 'Sigla Valuta',
            'prezzo_ivato' => 'Prezzo Ivato',
            'quantita' => 'Quantita',
            'tracking_number' => 'Tracking Number',
            'corriere' => 'Corriere',
            'link' => 'Link',
            'stato_riga' => 'Stato Riga',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdOrdine()
    {
        return $this->hasOne(Ordini::className(), ['id' => 'id_ordine']);
    }
}